package com.totalbp.mmrequest.activity;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.totalbp.mmrequest.MainActivity;
import com.totalbp.mmrequest.R;
import com.totalbp.mmrequest.config.SessionManager;
import com.totalbp.mmrequest.controller.MMController;
import com.totalbp.mmrequest.interfaces.VolleyCallback;
import com.totalbp.mmrequest.model.CommonAreaFormEnt;
import com.totalbp.mmrequest.model.CommonEnt;
import com.totalbp.mmrequest.model.CommonFloorFormEnt;
import com.totalbp.mmrequest.model.CommonMaterialFormEnt;
import com.totalbp.mmrequest.model.CommonTowerFormEnt;
import com.totalbp.mmrequest.model.CommonWorkFormEnt;
import com.totalbp.mmrequest.receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Ezra.R on 09/10/2017.
 */

public class RequestMaterialFilterActivity extends AppCompatActivity implements  ConnectivityReceiver.ConnectivityReceiverListener{

    private String fromrequestmaterial;

    private SearchableSpinner spinnerTower,spinnerFloor, spinnerZone, spinnerMaterial;
    private FloatingActionButton floatActButton;
    private String tower="-", floor="-", floortype="-", area="-", material="-", tdsprid="", unit="";

    private ArrayList<CommonTowerFormEnt> ddlArrayListTower2 = new ArrayList<>();
    private ArrayList<CommonFloorFormEnt> ddlArrayListFloor = new ArrayList<>();
    private ArrayList<CommonAreaFormEnt> ddlArrayListArea = new ArrayList<>();
    private ArrayList<CommonWorkFormEnt> ddlArrayListWork = new ArrayList<>();
    private ArrayList<CommonMaterialFormEnt> ddlArrayListMaterial = new ArrayList<>();

    private SessionManager session;
    MMController controller;
    public ProgressDialog pDialog;

    private static final int request_data_from_2  = 21;
    public RadioGroup rgStatus;
    private RadioButton radioStatusButton;
    private EditText etDateFrom, etDateTo;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener dateStart,dateEnd;
    DatePickerDialog datedialog;
    private boolean chooseRadioButton = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requestmaterialoutfilter);

        myCalendar = Calendar.getInstance();

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if(bd != null)
        {
            fromrequestmaterial = (String) bd.get("fromrequestmaterial");
            Log.d("fromrequestmaterial", fromrequestmaterial);
        }

        etDateFrom = (EditText) findViewById(R.id.etDateFrom);
        etDateTo = (EditText) findViewById(R.id.etDateTo);
        spinnerTower = (SearchableSpinner) findViewById(R.id.spinnerTower);
        spinnerFloor = (SearchableSpinner) findViewById(R.id.spinnerFloor);
        spinnerZone = (SearchableSpinner) findViewById(R.id.spinnerZone);
        spinnerMaterial = (SearchableSpinner) findViewById(R.id.spinnerMaterial);
        rgStatus = (RadioGroup) findViewById(R.id.rgStatus);

        floatActButton = (FloatingActionButton) findViewById(R.id.action_header_search_btn);

        getSupportActionBar().setTitle("Filter Request Material");
        session = new SessionManager(getApplicationContext());

        controller = new MMController();
        pDialog = new ProgressDialog(this);
        initControls();

        rgStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked)
                {
                    chooseRadioButton = true;
                    // Changes the textview's text to "Checked: example radiobutton text"
                    Log.d("LogRadio", "Checked:" + checkedRadioButton.getText()+ chooseRadioButton);
                    //tv.setText();
                }
            }
        });

        floatActButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d("LogTower", tower);
//                Log.d("LogFloor", floortype+floor);
//                Log.d("LogArea", area);
//                Log.d("LogMaterial", material);

                if( !etDateFrom.getText().toString().equals("") && !etDateTo.getText().toString().equals("") ) {

                    if(chooseRadioButton)
                    {
                        int selectedId = rgStatus.getCheckedRadioButtonId();
                        radioStatusButton = (RadioButton) findViewById(selectedId);
                        String status = "";
                        status = radioStatusButton.getText().toString();
                        Log.d("tesstatus", status);

                        List<String> dates = getDates(etDateFrom.getText().toString(), etDateTo.getText().toString());

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.putExtra("towerid", tower);
                        intent.putExtra("floorid", floortype + floor);
                        intent.putExtra("areaid", area);
                        intent.putExtra("materialid", material);
                        intent.putExtra("status", status);
                        intent.putExtra("dateRange", dates.toString());
                        setResult(request_data_from_2, intent);
                        finish();
                    }
                    else
                    {
                        Toast toast = Toast.makeText(getApplicationContext(), "Status Required!", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
                else
                {
                    Toast toast = Toast.makeText(getApplicationContext(), "Date From and Date To Required!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    etDateFrom.requestFocus();
                }
            }
        });

    }

    public static ArrayList<String> getDates(String dateString1, String dateString2)
    {
        Log.d("LogFilterUpcoming",dateString1);
        ArrayList<String> dates = new ArrayList<String>();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = dateFormat.parse(dateString1);
            date2 = dateFormat.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while(!cal1.after(cal2))
        {
            dates.add(dateFormat.format(cal1.getTime()));
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    public void initControls(){

        getDataForDDL2(spinnerTower, "SPM_GetTower_Param", "Kode_Tower", "Nama_Tower", ddlArrayListTower2, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "100", "@sortby", "", "@wherecond", " AND Kode_Proyek ='" + session.getKodeProyek() + "' ", "@nik", String.valueOf(session.isLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
        getDataForDDL3(spinnerFloor, "SPM_GetFloor_Param", "Kode_Lantai", "Nama_Lantai", ddlArrayListFloor, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "100", "@sortby", " Nama_Lantai ASC ", "@wherecond", " AND M_Zona.Kode_Proyek ='" + session.getKodeProyek() + "' ", "@nik", String.valueOf(session.isLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
        getDataForDDL4(spinnerZone, "Get_GetAreaFilter", "Kode_Lokasi", "Nama_Area", ddlArrayListArea, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "200", "@sortby", "", "@wherecond", " AND M_Zona.Kode_Proyek ='" + session.getKodeProyek() + "' ", "@nik", String.valueOf(session.isLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
        getDataForDDL6(spinnerMaterial, "SPM_GetMaterial_Param", "ID_M_Katalog", "Spec", ddlArrayListMaterial, "@TotalRecords", "", "@currentpage", "1", "@pagesize", "300", "@sortby", "", "@wherecond", " AND b.Kode_Proyek ='" + session.getKodeProyek() + "' ", "@nik", String.valueOf(session.isLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
        spinnerTower.setTitle("Select Tower");
        spinnerTower.setPositiveButton("OK");
        spinnerTower.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonTowerFormEnt commonEnt = (CommonTowerFormEnt) parent.getSelectedItem();
                tower = commonEnt.getValue();
                //if(!tower.equals("-")) {
                    //Log.d("towerChoose", tower);
                //}
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerFloor.setTitle("Select Floor");
        spinnerFloor.setPositiveButton("OK");
        spinnerFloor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonFloorFormEnt commonEnt = (CommonFloorFormEnt) parent.getSelectedItem();
                floor = commonEnt.getValue();
                floortype = commonEnt.getValue2();
                //work = commonEnt.getValue();
                //if(!floor.equals("-")) {

                    //getDataForDDL5(spinnerWork, "SPM_GetWorkBasedSprAndTowerAndFloorMobile_Param", "Kode_Work", "Nama_Work", ddlArrayListWork, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", "", "@wherecond", " AND a.Kode_Proyek ='" + session.getKodeProyek() + "' AND d.Kode_Zona ='" + tower + "' AND c.Kode_Lokasi ='" + floor + "' ");
                    //Log.d("floorChoose", floor);
                    // Log.d("workChoose", work);
                //}
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerZone.setTitle("Select Area");
        spinnerZone.setPositiveButton("OK");
        spinnerZone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonAreaFormEnt commonEnt = (CommonAreaFormEnt) parent.getSelectedItem();
                area = commonEnt.getParameter();
                //if(!area.equals("-")) {

                    //Log.d("areaChoose", area);
                //}
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerMaterial.setTitle("Select Material");
        spinnerMaterial.setPositiveButton("OK");
        spinnerMaterial.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonMaterialFormEnt commonEnt = (CommonMaterialFormEnt) parent.getSelectedItem();
                material = commonEnt.getValue();
                tdsprid = commonEnt.getValue4();
                unit = commonEnt.getValue3();
                Log.d("materialChoose", material);
                Log.d("materialChoose", tdsprid);
                Log.d("materialChoose", unit);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etDateFrom.setOnFocusChangeListener(new View.OnFocusChangeListener() { //supaya calendar sekali click
            @Override
            @TargetApi(Build.VERSION_CODES.M)
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    datedialog =  new DatePickerDialog(v.getContext(), dateStart, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH));
                    datedialog.show();
                    etDateFrom.setShowSoftInputOnFocus(false);
                } else {
                    datedialog.hide();
                }
            }
        });

        etDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(v.getContext(), dateStart, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });



        dateStart = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(etDateFrom);
            }
        };

        etDateTo.setOnFocusChangeListener(new View.OnFocusChangeListener() { //supaya calendar sekali click
            @Override
            @TargetApi(Build.VERSION_CODES.M)
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    datedialog =  new DatePickerDialog(v.getContext(), dateEnd, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH));
                    datedialog.show();
                    etDateTo.setShowSoftInputOnFocus(false);
                } else {
                    datedialog.hide();
                }
            }
        });

        etDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(v.getContext(), dateEnd, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        dateEnd = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(etDateTo);
            }
        };
    }

    private void updateLabel(EditText text) {
        String myFormat = "dd-MM-yyyy"; //In which you need put here "EEE, d MMM yyyy HH:mm:ss Z"
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        text.setText(sdf.format(myCalendar.getTime()));
        //validateField();
    }

    public void showDialog() {
        pDialog.setMessage("Loading ...");
        if (!pDialog.isShowing())
            pDialog.show();
    }

    public void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    public void getDataForDDL(final SearchableSpinner spinner, final String uniqueSPName,
                              final String paramName, final String paramVal,
                              final ArrayList<CommonEnt> ddlArrayList,
                              final String paramName1, final String paramVal1,
                              final String paramName2, final String paramVal2,
                              final String paramName3, final String paramVal3,
                              final String paramName4, final String paramVal4,
                              final String paramName5, final String paramVal5,
                              final String paramName6, final String paramVal6,
                              final String paramName7, final String paramVal7,
                              final String paramName8, final String paramVal8){
        ddlArrayList.clear();
        showDialog();
        controller.InqGeneral2(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,paramName6, paramVal6,paramName7, paramVal7,paramName8, paramVal8,
                new VolleyCallback() {
            @Override
            public void onSuccess(JSONArray result) {
                try {
                    if (result.length() > 0) {

                        CommonEnt itemEntsinit = new CommonEnt(
                                "-","-",
                                "-","-",
                                "-","-",
                                "-","-",
                                "-","-"
                        );
                        ddlArrayList.add(itemEntsinit);

                        for (int i = 0; i < result.length(); i++) {
                            JSONObject item = result.getJSONObject(i);

                            CommonEnt itemEnts = new CommonEnt(item.getString(paramVal), item.getString(paramName),
                                    item.getString(paramName), item.getString(paramName),
                                    item.getString(paramName), item.getString(paramName),
                                    item.getString(paramName), item.getString(paramName),
                                    item.getString(paramName), item.getString(paramName)
                            );
                            ddlArrayList.add(itemEnts);
                        }
                        hideDialog();
                        ArrayAdapter<CommonEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                        spinner.setAdapter(adapterDDLGlobal);
                    }
                    else{
                        CommonEnt itemEnts = new CommonEnt(
                                "No Item Found","-",
                                "No Item Found","-",
                                "No Item Found","-",
                                "No Item Found","-",
                                "No Item Found","-"
                        );
                        ddlArrayList.add(itemEnts);
                        ArrayAdapter<CommonEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                        spinner.setAdapter(adapterDDLGlobal);
                    }
                    hideDialog();
                }catch (JSONException e){
                    Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                }
            }


            @Override
            public void onSave(String output) {

            }
        });
    }

    public void getDataForDDL2(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonTowerFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();
        showDialog();
        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {

                                CommonTowerFormEnt itemEntsinit = new CommonTowerFormEnt(
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );
                                ddlArrayList.add(itemEntsinit);

                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonTowerFormEnt itemEnts = new CommonTowerFormEnt(item.getString(paramVal), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                }
                                hideDialog();
                                ArrayAdapter<CommonTowerFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonTowerFormEnt itemEnts = new CommonTowerFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonTowerFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            hideDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void getDataForDDL3(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonFloorFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();
        showDialog();
        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {

                                CommonFloorFormEnt itemEntsinit = new CommonFloorFormEnt(
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );
                                ddlArrayList.add(itemEntsinit);

                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonFloorFormEnt itemEnts = new CommonFloorFormEnt(item.getString(paramVal), item.getString(paramName),
                                            item.getString("Tipe_Lokasi"), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                }
                                hideDialog();
                                ArrayAdapter<CommonFloorFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonFloorFormEnt itemEnts = new CommonFloorFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonFloorFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            hideDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void getDataForDDL4(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonAreaFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();
        showDialog();
        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                CommonAreaFormEnt itemEntsinit = new CommonAreaFormEnt(
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );

                                ddlArrayList.add(itemEntsinit);
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonAreaFormEnt itemEnts = new CommonAreaFormEnt(item.getString(paramVal), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                }
                                hideDialog();
                                ArrayAdapter<CommonAreaFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonAreaFormEnt itemEnts = new CommonAreaFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonAreaFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            hideDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void getDataForDDL6(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonMaterialFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();
        showDialog();
        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                CommonMaterialFormEnt itemEntsinit = new CommonMaterialFormEnt(
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );

                                ddlArrayList.add(itemEntsinit);
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonMaterialFormEnt itemEnts = new CommonMaterialFormEnt(item.getString(paramVal), item.getString(paramName),
                                            item.getString("Unit"), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                    //sprqty = item.getString("Volume");
                                    //etSprQty.setText(sprqty);
                                    //Log.d("sprqty", sprqty);
                                }
                                hideDialog();
                                ArrayAdapter<CommonMaterialFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonMaterialFormEnt itemEnts = new CommonMaterialFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonMaterialFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            hideDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MMController.getInstance().setConnectivityListener(this);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;


        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.spinnerTower), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);

        CoordinatorLayout.LayoutParams params =(CoordinatorLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.TOP;
        sbView.setLayoutParams(params);
        snackbar.show();

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
}
