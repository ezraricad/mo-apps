package com.totalbp.mmrequest.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.picasso.Picasso;
import com.totalbp.mmrequest.R;
import com.totalbp.mmrequest.config.AppConfig;
import com.totalbp.mmrequest.config.SessionManager;
import com.totalbp.mmrequest.controller.MMController;
import com.totalbp.mmrequest.interfaces.VolleyCallback;
import com.totalbp.mmrequest.model.ApprovalEnt;
import com.totalbp.mmrequest.model.ChangeMoStatusEnt;
import com.totalbp.mmrequest.model.CommonRequestMoFormEnt;
import com.totalbp.mmrequest.model.Image;
import com.totalbp.mmrequest.model.ListItemRequestOutFullTable;
import com.totalbp.mmrequest.model.checkApprovalEnt;
import com.totalbp.mmrequest.receiver.ConnectivityReceiver;
import com.totalbp.mmrequest.utils.MProgressDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import static com.totalbp.mmrequest.config.AppConfig.URL_IMAGE_PORTAL;
import static com.totalbp.mmrequest.config.AppConfig.URL_IMAGE_PREFIX;
import static com.totalbp.mmrequest.config.AppConfig.urlProfileFromTBP;
import static com.totalbp.mmrequest.utils.ImageDecoder.decodeSampledBitmapFromFile;
import static com.totalbp.mmrequest.utils.ImageDecoder.getStringImage;

/**
 * Created by Ezra.R on 09/10/2017.
 */

public class ApprovalMaterialActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener
{
    private SessionManager session;
    private MMController controller;
    public List<ListItemRequestOutFullTable> listItems = new ArrayList<>();

    private ActionMode actionMode;
    JSONObject item;

    private Boolean isFabOpen = false;
    private FloatingActionButton fab,fab1,fab2;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;
    private TextView tvLabelNewMo, tvLabelFiler, tvReadyStock, tvIdleStock, dInputTitle, dITittle, tvManagerName;
    private ImageView ivCourirPhoto, dContentImage, ivRequesterPhoto, ivSmPhoto, dContentImageQspv, dContentImageManager, ivPicPenerima, ivAddPicPenerimaPhoto, dContentImagePicPenerima;
    private EditText etRequestBy,etTower,etFloor,etArea,etMaterial,etPekerjaan,etVolume, dIComment, etComment;
    private static final int request_data_from  = 20;
    private static final int request_data_from_2  = 21;
    private static final int request_data_to_confirmation = 22;
    private String idmo, idmaterial, approvalnumber, statusrequest, kodemo, kodetower, kodelantai, kodearea,
    Comment, Nama_Pembuat ,Nama_Pengubah, Pembuat, Pengubah,Tanggal_Dibuat, Tanggal_Diubah,sitemanagername, sitemanagernik;
    private Button btnApprove, btnReject, btnPosting, btnConfirmation, btnRejectQspv, dBtnCancel, dIBtnCancel, dIBtnSubmit, btnRejectPickUp, btnConfirmationPickUp;
    private Dialog dialog, dialogCourier, dialogInputText;
    private LinearLayout llPosting, llApproval, llConfirmation, llComment, llConfirmationPickUp;
    String today;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
    JSONObject itemstrukturorganisasi;
    private String savePath = "";
    public String mCurrentPhotoPath;
    static final int REQUEST_TAKE_PHOTO = 1;
    Uri photoURI;
    Calendar myCalendar = new GregorianCalendar();
    public String id_daily_log_temp;
    private int year, month, day;

    public ApprovalMaterialActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approvalmaterial);

        controller = new MMController();
        session = new SessionManager(getApplicationContext());

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if(bd != null)
        {
            idmo = (String) bd.get("IdMo");
            idmaterial = (String) bd.get("IdMaterial");
            approvalnumber = (String) bd.get("ApprovalNumber");
            statusrequest = (String) bd.get("StatusRequest");
            kodemo = (String) bd.get("KodeMo");

            kodetower = (String) bd.get("KodeTower");
            kodelantai = (String) bd.get("KodeLantai");
            kodearea =  (String) bd.get("KodeArea");
            Log.d("EzzIdMo", idmo);

            Comment = (String) bd.get("Comment");
            Nama_Pembuat = (String) bd.get("Nama_Pembuat");
            Nama_Pengubah = (String) bd.get("Nama_Pengubah");
            Pembuat = (String) bd.get("Pembuat");
            Pengubah = (String) bd.get("Pengubah");
            Tanggal_Dibuat = (String) bd.get("Tanggal_Dibuat");
            Tanggal_Diubah = (String) bd.get("Tanggal_Diubah");

            Log.d("EzzComment", Comment);
            Log.d("EzzNama_Pembuat", Nama_Pembuat);
            Log.d("EzzNama_Pengubah", Nama_Pengubah);
            Log.d("EzzPembuat", Pembuat);
            Log.d("EzzPengubah", Pengubah);
            Log.d("EzzTanggal_Dibuat", Tanggal_Dibuat);
            Log.d("EzzTanggal_Diubah", Tanggal_Diubah);

            if(!session.getCanEdit().toString().equals("1"))
            {
                Toast.makeText(getApplicationContext(),"You dont have Privilege to Edit!", Toast.LENGTH_SHORT).show();
                this.finish();
            }
        }

        etRequestBy = (EditText) findViewById(R.id.etRequestBy);
        etTower= (EditText) findViewById(R.id.etTower);
        etFloor= (EditText) findViewById(R.id.etFloor);
        etArea= (EditText) findViewById(R.id.etArea);
        etMaterial= (EditText) findViewById(R.id.etMaterial);
        etPekerjaan= (EditText) findViewById(R.id.etPekerjaan);
        tvReadyStock= (TextView) findViewById(R.id.tvReadyStock);
        tvIdleStock = (TextView) findViewById(R.id.tvIdleStock);
        etVolume= (EditText) findViewById(R.id.etVolume);
        etComment = (EditText) findViewById(R.id.etComment);
        ivCourirPhoto = (ImageView) findViewById(R.id.ivCourirPhoto);
        ivRequesterPhoto = (ImageView) findViewById(R.id.ivRequesterPhoto);
        ivSmPhoto = (ImageView) findViewById(R.id.ivSmPhoto);
        tvManagerName = (TextView) findViewById(R.id.tvManagerName);
        btnApprove = (Button) findViewById(R.id.btnApprove);
        btnReject = (Button) findViewById(R.id.btnReject);
        btnPosting = (Button) findViewById(R.id.btnPosting);
        llPosting = (LinearLayout) findViewById(R.id.llPosting);
        llApproval = (LinearLayout) findViewById(R.id.llApproval);
        llConfirmation = (LinearLayout) findViewById(R.id.llConfirmation);
        llComment= (LinearLayout) findViewById(R.id.llComment);
        btnConfirmation = (Button) findViewById(R.id.btnConfirmation);
        btnRejectQspv = (Button) findViewById(R.id.btnRejectQspv);
        ivPicPenerima = (ImageView) findViewById(R.id.ivPicPenerima);
        ivAddPicPenerimaPhoto = (ImageView) findViewById(R.id.ivAddPicPenerimaPhoto);

        llConfirmationPickUp = (LinearLayout) findViewById(R.id.llConfirmationPickUp);
        btnRejectPickUp = (Button) findViewById(R.id.btnRejectPickUp);
        btnConfirmationPickUp = (Button) findViewById(R.id.btnConfirmationPickUp);

        dialog = new Dialog(ApprovalMaterialActivity.this);
        today = dateFormat.format(myCalendar.getTime());

        year = myCalendar.get(Calendar.YEAR);
        month = myCalendar.get(Calendar.MONTH);
        day = myCalendar.get(Calendar.DAY_OF_MONTH);

        //Dialog Courier Photo
        //Dialog untuk input
        dialogCourier = new Dialog(ApprovalMaterialActivity.this);
        dialogCourier.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogCourier.setContentView(R.layout.photoframe_dialog);
        dialogCourier.setCanceledOnTouchOutside(false);

        dInputTitle = (TextView) dialogCourier.findViewById(R.id.dTittle);
        dContentImage = (ImageView) dialogCourier.findViewById(R.id.dContentImageCourir);
        dContentImageManager = (ImageView) dialogCourier.findViewById(R.id.dContentImageManager);
        dContentImageQspv = (ImageView) dialogCourier.findViewById(R.id.dContentImageQspv);
        dContentImagePicPenerima = (ImageView) dialogCourier.findViewById(R.id.dContentImagePicPenerima);
        dBtnCancel = (Button) dialogCourier.findViewById(R.id.dBtnCancel);

        //Dialog untuk input text
        dialogInputText = new Dialog(ApprovalMaterialActivity.this);
        dialogInputText.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogInputText.setContentView(R.layout.general_dialog_inputtext);
        dialogInputText.setCanceledOnTouchOutside(false);
        dITittle = (TextView) dialogInputText.findViewById(R.id.dITittle);
        dIComment = (EditText) dialogInputText.findViewById(R.id.dIComment);
        dIBtnCancel = (Button) dialogInputText.findViewById(R.id.dIBtnCancel);
        dIBtnSubmit = (Button) dialogInputText.findViewById(R.id.dIBtnSubmit);


        getSupportActionBar().setSubtitle(kodemo);

        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onApprove(kodemo, approvalnumber, "0");
            }
        });

        btnApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onApprove(kodemo, approvalnumber, "1");
            }
        });

        btnPosting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPosting(kodemo, idmo);
            }
        });

        btnConfirmation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String status = "6";
                onConfirmation(status, approvalnumber, "");
            }
        });

        btnConfirmationPickUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String status = "4";

                //jika foto PIC Penerima belum diupload maka tidak bisa confirm
                if(savePath != "")
                {
                    onConfirmation(status, approvalnumber, "");
                }
                else
                {
                    Toast toast = Toast.makeText(ApprovalMaterialActivity.this,"Foto PIC Penerima diperlukan!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });

        btnRejectPickUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String status = "5";
//                onConfirmation(status, approvalnumber, "");

                dITittle.setText("Reject Reason");

                dIBtnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!dIComment.getText().toString().equals("")) {
                            String status = "5";
                            onConfirmation(status, approvalnumber, dIComment.getText().toString());
                        }
                        else
                        {
                            Toast toast = Toast.makeText(ApprovalMaterialActivity.this,"Reason Required!", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            dIComment.setText("");
                            dIComment.requestFocus();
                        }
                    }
                });

                dIBtnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogInputText.dismiss();
                    }
                });

                dialogInputText.show();
            }
        });

        btnRejectQspv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dITittle.setText("Reject Reason");

                dIBtnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!dIComment.getText().toString().equals("")) {
                            String status = "7";
                            onConfirmation(status, approvalnumber, dIComment.getText().toString());
                        }
                        else
                        {
                            Toast toast = Toast.makeText(ApprovalMaterialActivity.this,"Reason Required!", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            dIComment.setText("");
                            dIComment.requestFocus();
                        }
                    }
                });

                dIBtnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogInputText.dismiss();
                    }
                });

                dialogInputText.show();
            }
        });

        ivCourirPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dInputTitle.setText("Foto PIC Ditunjuk");
                dContentImageManager.setVisibility(View.GONE);
                dContentImageQspv.setVisibility(View.GONE);
                dContentImagePicPenerima.setVisibility(View.GONE);
                dContentImage.setVisibility(View.VISIBLE);

                dBtnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogCourier.dismiss();
                    }
                });

                dialogCourier.show();
            }
        });

        ivRequesterPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dInputTitle.setText("Foto Peminta");
                dContentImage.setVisibility(View.GONE);
                dContentImageManager.setVisibility(View.GONE);
                dContentImagePicPenerima.setVisibility(View.GONE);
                dContentImageQspv.setVisibility(View.VISIBLE);

                dBtnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogCourier.dismiss();
                    }
                });

                dialogCourier.show();
            }
        });

        ivSmPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dInputTitle.setText("Foto Penyetuju");
                dContentImage.setVisibility(View.GONE);
                dContentImageQspv.setVisibility(View.GONE);
                dContentImagePicPenerima.setVisibility(View.GONE);
                dContentImageManager.setVisibility(View.VISIBLE);

                dBtnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogCourier.dismiss();
                    }
                });

                dialogCourier.show();
            }
        });

        ivPicPenerima.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Jagaan saat status request 2 jika ingin lihat foto di disable karena posisi belum upload
                if(!statusrequest.equals("2")) {

                    dInputTitle.setText("Foto PIC Penerima");
                    dContentImage.setVisibility(View.GONE);
                    dContentImageManager.setVisibility(View.GONE);
                    dContentImageQspv.setVisibility(View.GONE);
                    dContentImagePicPenerima.setVisibility(View.VISIBLE);

                    dBtnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogCourier.dismiss();
                        }
                    });

                    dialogCourier.show();
                }
            }
        });

        ivAddPicPenerimaPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Jika status request 2 maka baru bisa upload gambar
                if(statusrequest.equals("2")) {
                    dispatchTakePictureIntent();
                }
                else
                {
                    Toast toast = Toast.makeText(ApprovalMaterialActivity.this,"Status Request tidak sesuai!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });

        SetListViewDetil(idmo);
        StvReadyStockMaterial(idmaterial, kodetower, kodelantai, kodearea);
        gtvIdleStockQty(idmaterial, kodetower);
        CheckAction(statusrequest);
        gtvByteImageFromNik(Pembuat, "requester");
        getSiteManagetByNik(Pembuat);

    }

    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //MainActivity activity = (MainActivity)getActivity();

        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                mCurrentPhotoPath = photoFile.getAbsolutePath();

                photoURI = FileProvider.getUriForFile(getApplicationContext(), "com.totalbp.mmrequest.fileprovider", photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,photoURI);
//                takePictureIntent.putExtra("id_daily_log",id_daily_log);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private  File createImageFile() throws IOException { //bener2 simpen filenya di storage directory
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()){
            if (!storageDir.mkdirs()){
                return null;
            }
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);


        if( requestCode == request_data_from ) {
            if (data != null){
                //projectSelected.setText(data.getExtras().getString("projectname"));
                session.setKodeProyek(data.getExtras().getString("kodeProyek"));
                session.setNamaProyek(data.getExtras().getString("projectname"));

                //Toast.makeText(getApplicationContext(), "Sampai sini Kode Proyek"+data.getExtras().getString("kodeProyek"), Toast.LENGTH_LONG).show();
                //Toast.makeText(getApplicationContext(), "Sampai sini Kode2 Proyek"+session.getKodeProyek(), Toast.LENGTH_LONG).show();

                //setupViewPager(viewPager);
            }
        }
        else if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            Toast.makeText(getBaseContext(), "Photo Taken",Toast.LENGTH_SHORT).show();

            UploadPicture(id_daily_log_temp);

            final Bitmap selectedImage = decodeSampledBitmapFromFile(mCurrentPhotoPath,500, 250);
            String encodedImage = getStringImage(selectedImage);

            byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            //Start
            //Code By Ezra
            //bitmapnya dideclare ulang terlebih dahulu untuk dibenarkan rotatenya kemudian baru diset ke imageview
            ExifInterface ei = null;
            try {
                ei = new ExifInterface(mCurrentPhotoPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            Bitmap rotatedBitmap = null;
            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(decodedByte, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(decodedByte, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(decodedByte, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = decodedByte;
            }
            //Close
            ivPicPenerima.setImageBitmap(rotatedBitmap);

        }

    }


    private void UploadPicture(String id_daily_log){
        final Bitmap selectedImage = decodeSampledBitmapFromFile(mCurrentPhotoPath,500, 500);
        String filename="DP" +"_"+ mCurrentPhotoPath.substring(mCurrentPhotoPath.lastIndexOf("/")+1);
        String encodedImage = getStringImage(selectedImage);

        //Start
        //Code by Ezra
        //Decode terlebih dahulu untuk dibenarkan rotatenya kemudian baru di upload
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        ExifInterface ei = null;
        try {
            ei = new ExifInterface(mCurrentPhotoPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap rotatedBitmap = null;
        switch(orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(decodedByte, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(decodedByte, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(decodedByte, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = decodedByte;
        }

        String encodedImage2 = getStringImage(rotatedBitmap);
        //end
        UploadFileToServer(encodedImage2,filename, id_daily_log);

    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public void CheckAction(String statusrequest)
    {
        getSupportActionBar().setTitle("Confirmation Material Out");
        if(statusrequest.equals("1") && approvalnumber.equals(""))
        {
            isCanPosting(kodemo, idmo);
        }
        else if(statusrequest.equals("2"))
        {
            getSupportActionBar().setTitle("Confirmation Material Pickup");
            llConfirmationPickUp.setVisibility(View.VISIBLE);
            //Jika status 2 maka tombol add PicPenerimaPhoto berwarna biru
            ivAddPicPenerimaPhoto.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        else if(statusrequest.equals("4"))
        {
            llConfirmation.setVisibility(View.VISIBLE);
        }
        else if(statusrequest.equals("5"))
        {
            llComment.setVisibility(View.VISIBLE);
            etComment.setText(Comment);
        }
        else if(statusrequest.equals("7"))
        {
            llComment.setVisibility(View.VISIBLE);
            etComment.setText(Comment);
        }
    }

    public void UploadFileToServer(String base64encode, String filename, final String id_hse_assessment){

        MProgressDialog.showProgressDialog(ApprovalMaterialActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        RequestQueue requestQueue;
        session = new SessionManager(getApplicationContext());
        JSONObject request = new JSONObject();

        try {
            request.put("Pengubah", session.getUserDetails().get("nik"));
            request.put("FileName", filename);
            request.put("TokenID", session.getUserToken());
            request.put("FormID", "AR.03.01");
            request.put("KodeProyek",session.getKodeProyek()); //Dummy
            request.put("Base64String",base64encode);
        } catch (Exception e) {
            e.printStackTrace();
        }

        requestQueue = Volley.newRequestQueue(getApplicationContext());
//        http://10.100.223.51:20173/API/TQA/Upload
        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, session.getUrlConfig()+ AppConfig.URL_UPLOAD_GAMBAR,request,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("LogUploadPicPenerimaGbr",response.toString());
                            if(response.getString("IsSucceed").equals("true")){
                                // Toast.makeText(getApplicationContext(), response.getString("Message"), Toast.LENGTH_LONG).show();
//                                String savePath = response.getString("VarOutput");
                                String pathDatabase = response.getString("VarOutput"); //hashcode
                                String yearstring = String.valueOf(year);
                                String monthstring = "0"+String.valueOf(month+1);
                                savePath = session.getUrlConfig()+URL_IMAGE_PREFIX + "/GeneralFile/"+yearstring.substring(yearstring.length()-2)+"/"+monthstring.substring(monthstring.length()-2)+"/" + response.getString("Message") + ".jpg";

                                Log.d("savePath",savePath);
                                MProgressDialog.dismissProgressDialog();

                            }
                            else{
                                Toast.makeText(getApplicationContext(), response.getString("Message "+"Please Re-Login"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Error Response " + error.toString(), Toast.LENGTH_LONG).show();
                    }
                });
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(strReq);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MMController.getInstance().setConnectivityListener(this);
    }

    private void isCanPosting(String kodematerialout, String IdMo) {

        MProgressDialog.showProgressDialog(ApprovalMaterialActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("is_can_posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
        approvalEnt.setTokenID(session.getUserToken());
        //approvalEnt.setApproval_Name("Approval Material Out Management Mobile");
        
        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

                if(jsonObject.get("IsCanPosting").getAsString().equals("true"))
                {
                    llPosting.setVisibility(View.VISIBLE);
                }
                else
                {
                    Toast toast = Toast.makeText(ApprovalMaterialActivity.this, jsonObject.get("Message").getAsString()+" !", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });
    }

    private void IsCanApprove(String kodematerialout, String ApprovalNumber, String IdMo) {

        MProgressDialog.showProgressDialog(ApprovalMaterialActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("");
        //approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number(ApprovalNumber);
        approvalEnt.setTokenID(session.getUserToken());
        //approvalEnt.setApproval_Name("Approval Material Out Management Mobile");



        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {
                Log.d("Saapp", output);
                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

                if(jsonObject.get("IsCanApprove").getAsString().equals("true"))
                {
                    llApproval.setVisibility(View.VISIBLE);
                }
                else
                {
                    Toast toast = Toast.makeText(ApprovalMaterialActivity.this,jsonObject.get("Message").getAsString()+"!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });
    }

    private void onApprove(String kodematerialout, String ApprovalNumber, String IsApprove) {

        MProgressDialog.showProgressDialog(ApprovalMaterialActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("");
        approvalEnt.setIs_approve(IsApprove);
        approvalEnt.setApproval_number(ApprovalNumber);
        approvalEnt.setTokenID(session.getUserToken());
        //approvalEnt.setApproval_Name("Approval Material Out Management Mobile");

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                Intent intent = new Intent();
                intent.putExtra("", "");
                setResult(request_data_to_confirmation, intent);
                finish();

            }
        });
    }

    private void onPosting(String kodematerialout, String IdMo) {

        MProgressDialog.showProgressDialog(ApprovalMaterialActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
        approvalEnt.setTokenID(session.getUserToken());
        //approvalEnt.setApproval_Name("Approval Material Out Management Mobile");

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {


                Log.d("OmOm2", output);
                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

                    Log.d("c", output);
                    updateRequestMO(jsonObject.get("Approval_Number").getAsString(), IdMo, kodematerialout);
            }
        });
    }

    private void onConfirmation(String status, String approvalnumber, String reason) {

        MProgressDialog.showProgressDialog(ApprovalMaterialActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ChangeMoStatusEnt changemostatus = new ChangeMoStatusEnt();
        changemostatus.setID(idmo);
        changemostatus.setKode_MO(idmaterial);
        changemostatus.setNama_Pembuat(Nama_Pembuat);
        changemostatus.setNama_Pengubah(Nama_Pengubah);
        changemostatus.setPembuat(Pembuat);
        changemostatus.setPengubah(Pengubah);
        changemostatus.setStatusAktif("true");
        changemostatus.setStatusRequest(status);
        changemostatus.setTanggal_Dibuat(Tanggal_Dibuat);
        changemostatus.setTanggal_Diubah(Tanggal_Diubah);
        changemostatus.setComment(reason);
        changemostatus.setApprovalNo(approvalnumber);
        changemostatus.setTokenID(session.getUserToken());

        //Jika material pick up maka tambahkan variable dot penerima
        if(status.equals("4"))
        {
            changemostatus.setPicPenerimaPhoto(savePath);
        }

        String jsonString = gsonHeader.toJson(changemostatus);

        controller.SaveGeneralObjectRequestMo(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                if(status.equals("4")) {
                    Toast toast = Toast.makeText(ApprovalMaterialActivity.this, "Approve Pickup Success!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                else if(status.equals("5")) {
                    Toast toast = Toast.makeText(ApprovalMaterialActivity.this, "Reject Pickup Success!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                else if(status.equals("6")) {
                    Toast toast = Toast.makeText(ApprovalMaterialActivity.this, "Confirmation Receive Success!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                else
                {
                    Toast toast = Toast.makeText(ApprovalMaterialActivity.this, "Confirmation Reject Success!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

                Intent intent = new Intent();
                intent.putExtra("", "");
                setResult(request_data_to_confirmation, intent);
                finish();
            }
        });
    }


    private void updateRequestMO(String ApprovalNumber, String IdMo, String KodeMaterialOut) {
        Log.d("OmOm4", ApprovalNumber);
        MProgressDialog.showProgressDialog(ApprovalMaterialActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();

        CommonRequestMoFormEnt commonRequestMoFormEnt = new CommonRequestMoFormEnt();
        commonRequestMoFormEnt.setID(IdMo);
        commonRequestMoFormEnt.setKode_MO(KodeMaterialOut);
        commonRequestMoFormEnt.setKode_Proyek(session.getKodeProyek());
        commonRequestMoFormEnt.setKode_Item("");
        commonRequestMoFormEnt.setVolume("1");
        commonRequestMoFormEnt.setUnit("");
        commonRequestMoFormEnt.setWork_Code("");
        commonRequestMoFormEnt.setKode_Tower("");
        commonRequestMoFormEnt.setKode_Lantai("");
        commonRequestMoFormEnt.setKode_Zona("0");
        commonRequestMoFormEnt.setCourierPhoto("");
        commonRequestMoFormEnt.setRefHeaderSPR("0");
        commonRequestMoFormEnt.setRefDetailSPR("0");
        commonRequestMoFormEnt.setStatusRequest("1");
        commonRequestMoFormEnt.setApprovalNo(ApprovalNumber);
        commonRequestMoFormEnt.setStatusAktif("True");
        commonRequestMoFormEnt.setPembuat(session.getKeyNik());
        commonRequestMoFormEnt.setNama_Pembuat(session.getKeyNik());
        commonRequestMoFormEnt.setWaktuBuat(today);
        commonRequestMoFormEnt.setPengubah(session.getKeyNik());
        commonRequestMoFormEnt.setNama_Pengubah(session.getKeyNik());
        commonRequestMoFormEnt.setWaktuUbah(today);
        commonRequestMoFormEnt.setComment("");
        commonRequestMoFormEnt.setTokenID(session.getUserToken());
        commonRequestMoFormEnt.setFormId("MAMAN.02");
        String jsonString = gsonHeader.toJson(commonRequestMoFormEnt);
        Log.d("OmOm5", ApprovalNumber);
        controller.SaveGeneralObjectRequestMo(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                Toast toast = Toast.makeText(ApprovalMaterialActivity.this,KodeMaterialOut+" Posting Success !", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                Intent intent = new Intent();
                intent.putExtra("", "");
                setResult(request_data_to_confirmation, intent);
                finish();

            }
        });
    }

    public void updateStatusRequest(String action)
    {
        Gson gsonHeader = new Gson();
        checkApprovalEnt checkapprovalEnt = new checkApprovalEnt();
        checkapprovalEnt.setUserid(session.isNikUserLoggedIn());
        checkapprovalEnt.setKodeProyek(session.getKodeProyek());
        checkapprovalEnt.setAppprovalNo(approvalnumber);

        String jsonStringCheck = gsonHeader.toJson(checkapprovalEnt);

        if(action.equals("Approve"))
        {
            controller.SaveGeneralObjectPostApproval(getApplicationContext(), jsonStringCheck, new VolleyCallback() {
                @Override
                public void onSuccess(JSONArray result) {
                    //Toast.makeText(getApplicationContext(),"CheckApproval result "+result,Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSave(String output) {

                    Bundle sendBundle = new Bundle();
                    sendBundle.putString("data", "");
                    Intent intent = new Intent(getApplicationContext(), RequestMaterialActivity.class );
                    intent.putExtras(sendBundle);
                    setResult(22,intent);
                    finish();

                    Toast.makeText(getApplicationContext(), "Submit Approval Success !" + output, Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            controller.SaveGeneralObjectPostReject(getApplicationContext(), jsonStringCheck, new VolleyCallback() {
                @Override
                public void onSuccess(JSONArray result) {
                    //Toast.makeText(getApplicationContext(),"CheckApproval result "+result,Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSave(String output) {

                    Bundle sendBundle = new Bundle();
                    sendBundle.putString("data", "");
                    Intent intent = new Intent(getApplicationContext(), RequestMaterialActivity.class );
                    intent.putExtras(sendBundle);
                    setResult(22,intent);
                    finish();
                    Toast.makeText(getApplicationContext(), "Submit Reject Success !" + output, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void SetListViewDetil(String wherecond){
        session = new SessionManager(getApplicationContext());
        //listItems.clear();

        controller.InqGeneral2(getApplicationContext(),"SPM_GetListRequestMaterialOut_Param",
                "@KodeProject",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond"," AND T_MO.ID = "+wherecond,
                "@nik",session.isNikUserLoggedIn(),
                "@formid","MAMAN.02",
                "@zonaid","",

                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("EZRA", result.toString());
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);
                                    ListItemRequestOutFullTable listProjectEnt = new ListItemRequestOutFullTable(
                                            item.getString("ID"), item.getString("Kode_MO"),
                                            item.getString("Kode_Proyek"), item.getString("Kode_Item"),
                                            item.getString("Volume"),item.getString("Unit"),
                                            item.getString("Work_Code"),item.getString("Kode_Tower"),
                                            item.getString("Kode_Lantai"),item.getString("Kode_Zona"),
                                            item.getString("CourierPhoto"),item.getString("RefHeaderSPR"),
                                            item.getString("RefDetailSPR"),item.getString("StatusRequest"),
                                            item.getString("ApprovalNo"),item.getString("StatusAktif"),
                                            item.getString("Nama_Pembuat"),item.getString("Tanggal_Dibuat"),
                                            item.getString("Nama_Pengubah"),item.getString("Tanggal_Diubah"),
                                            item.getString("Comment"),item.getString("Nama_Kurir"),
                                            item.getString("Deskripsi"),item.getString("Nama_Tower"),
                                            item.getString("Nama_Lantai"),item.getString("Nama_Zona"),
                                            item.getString("DeskripsiPekerjaan"),item.getString("PicPenerimaPhoto")
                                    );

                                    listItems.add(listProjectEnt);

                                    if(!item.getString("NamaPembuatReal").equals("null")) {
                                        etRequestBy.setText(item.getString("NamaPembuatReal"));
                                    }
                                    if(!item.getString("Nama_Tower").equals("null")) {
                                        etTower.setText(item.getString("Nama_Tower"));
                                    }
                                    if(!item.getString("Nama_Lantai").equals("null")) {
                                        etFloor.setText(item.getString("Nama_Lantai"));
                                    }
                                    if(!item.getString("Nama_Zona").equals("null")) {
                                        etArea.setText(item.getString("Nama_Zona"));
                                    }
                                    etMaterial.setText(item.getString("Deskripsi"));
                                    etPekerjaan.setText(item.getString("DeskripsiPekerjaan"));

                                    //if(!item.getString("Volume").equals("null")) {

                                        if(!item.getString("VolumeIdle").equals("null")) {
                                            float totalmo = (Float.parseFloat(item.getString("Volume")) + Float.parseFloat(item.getString("VolumeIdle")));

                                            //JIKA VOLUME IDLE TIDAK ADA MAKA TAMPILKAN VO
                                            if (item.getString("VolumeIdle").toString().equals("0.0")) {
                                                etVolume.setText(item.getString("Volume"));
                                            } else {
                                                etVolume.setText(item.getString("Volume") + " + " + item.getString("VolumeIdle") + " (Idle) = " + String.valueOf(totalmo));
                                            }
                                        }
                                        else
                                        {
                                            etVolume.setText(item.getString("Volume"));
                                        }
//                                    }
//                                    else
//                                    {
//                                        etVolume.setText("0");
//                                    }

                                    Picasso.with(getApplicationContext())
                                            .load(item.getString("CourierPhoto"))
                                            .placeholder(R.drawable.ic_insert_photo_black_48dp)
                                            .error(R.drawable.ic_insert_photo_black_48dp)
                                            .into(ivCourirPhoto);

                                    Picasso.with(getApplicationContext())
                                            .load(item.getString("CourierPhoto"))
                                            .placeholder(R.drawable.ic_insert_photo_black_48dp)
                                            .error(R.drawable.ic_insert_photo_black_48dp)
                                            .into(dContentImage);

                                    Picasso.with(getApplicationContext())
                                            .load(item.getString("PicPenerimaPhoto"))
                                            .placeholder(R.drawable.ic_insert_photo_black_48dp)
                                            .error(R.drawable.ic_insert_photo_black_48dp)
                                            .into(ivPicPenerima);

                                    Picasso.with(getApplicationContext())
                                            .load(item.getString("PicPenerimaPhoto"))
                                            .placeholder(R.drawable.ic_insert_photo_black_48dp)
                                            .error(R.drawable.ic_insert_photo_black_48dp)
                                            .into(dContentImagePicPenerima);


//                                    Glide.with(getApplicationContext()).load(URL_IMAGE_PORTAL+session.getKeyNik())
//                                            .asBitmap()
//                                            .centerCrop()
//                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
//                                            .into(new BitmapImageViewTarget(ivRequesterPhoto) {
//                                                @Override
//                                                protected void setResource(Bitmap resource) {
//                                                    RoundedBitmapDrawable circularBitmapDrawable =
//                                                            RoundedBitmapDrawableFactory.create(getResources(), resource);
//                                                    circularBitmapDrawable.setCircular(true);
//                                                    ivRequesterPhoto.setImageDrawable(circularBitmapDrawable);
//                                                }
//                                            });

//                                    Picasso.with(getApplicationContext())
//                                            .load(URL_IMAGE_PORTAL+session.getKeyNik())
//                                            .placeholder(R.drawable.ic_insert_photo_black_48dp)
//                                            .error(R.drawable.ic_insert_photo_black_48dp)
//                                            .into(ivRequesterPhoto);

//                                    Picasso.with(getApplicationContext())
//                                            .load(item.getString("CourierPhoto"))
//                                            .placeholder(R.drawable.ic_insert_photo_black_48dp)
//                                            .error(R.drawable.ic_insert_photo_black_48dp)
//                                            .into(ivSmPhoto);

                                    Log.d("LogMMApps", URL_IMAGE_PORTAL+session.getKeyNik());
                                    Log.d("LogMMApps", item.getString("CourierPhoto"));

                                    //Jika data foto kurir dan dialognya null
                                    if(item.getString("CourierPhoto").equals("null")) {
                                        ivCourirPhoto.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                                        dContentImage.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                                    }

                                    //Jika data PicPenerimaPhoto
                                    if(item.getString("PicPenerimaPhoto").equals("null")) {
//                                        ivPicPenerima.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
//                                        dContentImagePicPenerima.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                                    }
                                }
                                // Log.d("EZRA2", item.toString());
                               // adapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void StvReadyStockMaterial(String materialid, String kodetower, String kodelantai, String kodearea){
        session = new SessionManager(getApplicationContext());
        String where = "";

        //Jagaan saat parameter kode area kosong berarti wherecond diganti untuk menampilkan item migrasi
        if(kodearea.equals("null") || kodearea.equals("000"))
        {
            where = " WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+materialid+"' ";
        }
        else
        {
            where = " WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+materialid+"' AND Kode_Lokasi = '"+kodelantai+"' AND Kode_Area = '"+kodearea+"' ";
        }
        controller.InqGeneral3(getApplicationContext(),"SPM_GetQtyReadyStockFromViewParam",
                "@KodeProject",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond",where,
                //"@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+materialid+"' AND Kode_Tower = '"+kodetower+"' AND Kode_Lokasi = '"+kodelantai+"' AND Kode_Area = '"+kodearea+"' ",


                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("WOI2",result.toString());
                            if (result.length() > 0 || !result.toString().equals("[]")) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);
                                    tvReadyStock.setText(item.getString("LatestVol"));
                                }
                            }
                            else {
                                tvReadyStock.setText("0");
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void gtvIdleStockQty(String material, String tower){
        Log.d("material", material);
        session = new SessionManager(getApplicationContext());
        controller.InqGeneral3(getApplicationContext(),"SPM_GetQtyIdleStockFromView_Param",
                "@KodeProject",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond"," WHERE ttrans.Kode_Proyek = '"+session.getKodeProyek()+"' AND ttrans.Kode_Item = '"+material+"' AND ttrans.ToKode_Tower = '"+tower+"' AND ttrans.FlagReturnGudang = 1 ",
                //"@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' +"' AND Requester = '"+session.getUserName()+"' ",
                //"@nik",String.valueOf(session.isLoggedIn()),
                //"@formid","MAMAN.02",
                //"@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("EZRAgtvIdleStockQty", result.toString());
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);
                                    tvIdleStock.setText(item.getString("vol"));
                                }
                            }
                            else
                            {
                                tvIdleStock.setText("0");
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH IdleStockQty", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void gtvByteImageFromNik(String nik, String type){
        session = new SessionManager(getApplicationContext());
        controller.InqGeneralFoto(getApplicationContext(),"GetFotoKaryawanMobile",
                "@NIK",nik,
                "@NikPembuat",nik,
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result)
                    {
                            if (result.length() > 0) {
                                try {
                                        item = result.getJSONObject(0);
                                        String foto = item.getString("Foto");

                                        byte[] decodedString = Base64.decode(foto, Base64.DEFAULT);

                                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                        Log.d("MMLogFoto", foto);
                                        if (type.equals("requester")) {
                                            ivRequesterPhoto.setImageBitmap(decodedByte);
                                            dContentImageQspv.setImageBitmap(decodedByte);
                                        } else {
                                            ivSmPhoto.setImageBitmap(decodedByte);
                                            dContentImageManager.setImageBitmap(decodedByte);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                    }
                });
    }

    public void getSiteManagetByNik(String nik){

//        MProgressDialog.showProgressDialog(TransferMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                MProgressDialog.dismissProgressDialog();
//            }
//        });

        session = new SessionManager(getApplicationContext());
        controller.InqGeneral3(getApplicationContext(),"GetHirarkiUserMM",
                "@KodeProject"," '"+session.getKodeProyek()+"' ",
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond"," '"+nik+"' ",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("MMLogManager", result.toString());
                            MProgressDialog.dismissProgressDialog();
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    itemstrukturorganisasi = result.getJSONObject(i);
                                    sitemanagername = itemstrukturorganisasi.getString("Nama");
                                    sitemanagernik = itemstrukturorganisasi.getString("NoNik");
                                    tvManagerName.setText(sitemanagername);
                                    gtvByteImageFromNik(sitemanagernik, "sitemanager");
                                }
                            }
                        }catch (JSONException e){
                            //Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            // Network is present and connected
            isAvailable = true;
            //btnConnect.setVisibility(View.GONE);
            //h2.removeCallbacks(run);
        }
        else
        {
            isAvailable = false;
            ///h2.postDelayed(run, 0);
            //btnConnect.setVisibility(View.VISIBLE);
        }
        return isAvailable;
    }



    public void animateFAB(){

        if(isFabOpen){

            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            tvLabelNewMo.startAnimation(fab_close);
            tvLabelFiler.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            isFabOpen = false;
            Log.d("Raj", "close");

        } else {

            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            tvLabelNewMo.startAnimation(fab_open);
            tvLabelFiler.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            isFabOpen = true;
            Log.d("Raj","open");

        }
    }


    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;


        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.etRequestBy), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);

        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.TOP;
        sbView.setLayoutParams(params);
        snackbar.show();

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
}
