package com.totalbp.mmrequest.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.totalbp.mmrequest.MainActivity;
import com.totalbp.mmrequest.R;
import com.totalbp.mmrequest.adapter.ListLokasiAdapter;
import com.totalbp.mmrequest.adapter.ListRequestItemAdapter;
import com.totalbp.mmrequest.config.AppConfig;
import com.totalbp.mmrequest.config.SessionManager;
import com.totalbp.mmrequest.controller.MMController;
import com.totalbp.mmrequest.interfaces.VolleyCallback;
import com.totalbp.mmrequest.model.ApprovalEnt;
import com.totalbp.mmrequest.model.CommonAreaFormEnt;
import com.totalbp.mmrequest.model.CommonEnt;
import com.totalbp.mmrequest.model.CommonFloorFormEnt;
import com.totalbp.mmrequest.model.CommonListRecipientEnt;
import com.totalbp.mmrequest.model.CommonMaterialFormEnt;
import com.totalbp.mmrequest.model.CommonRequestMoFormEnt;
import com.totalbp.mmrequest.model.CommonTowerFormEnt;
import com.totalbp.mmrequest.model.CommonWorkFormEnt;
import com.totalbp.mmrequest.model.ListIdleStockEnt;
import com.totalbp.mmrequest.model.ListItemRequestOut;
import com.totalbp.mmrequest.model.ListLokasiEnt;
import com.totalbp.mmrequest.model.ListReadyStockEnt;
import com.totalbp.mmrequest.model.ListRecipientEnt;
import com.totalbp.mmrequest.receiver.ConnectivityReceiver;
import com.totalbp.mmrequest.utils.MProgressDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.totalbp.mmrequest.config.AppConfig.URL_IMAGE_PREFIX;
import static com.totalbp.mmrequest.utils.ImageDecoder.decodeSampledBitmapFromFile;
import static com.totalbp.mmrequest.utils.ImageDecoder.getStringImage;

/**
 * Created by Ezra.R on 09/10/2017.
 */

public class RequestMaterialOutFinal2Activity extends AppCompatActivity implements ListLokasiAdapter.MessageAdapterListener, ConnectivityReceiver.ConnectivityReceiverListener {

    private String fromrequestmaterial;

    private SearchableSpinner spinnerSpr, spinnerTower, spinnerFloor, spinnerArea, spinnerWork, spinnerMaterial, spinnerMaterial1;
    private FloatingActionButton floatActButton;
    private Button btnAddMoreUnit, btnSave;
    private String spr = "", tower = "",tower2 = "", floor = "", floortype = "", floorfull = "", floorfull2 = "", area = "", area2 = "", work = "",work2 = "", material = "", sprqty = "", thsprid = "", today = "", unit = "", tdsprid = "", typeitem = "";
    private String savePath = "";
    private int spnvol, tmovol, ttransvol;
    private String latestvol, idlveol, latestvolupdate;
    private ArrayList<CommonEnt> ddlArrayListTower = new ArrayList<>();
    private ArrayList<CommonTowerFormEnt> ddlArrayListTower2 = new ArrayList<>();
    private ArrayList<CommonFloorFormEnt> ddlArrayListFloor = new ArrayList<>();
    private ArrayList<CommonAreaFormEnt> ddlArrayListArea = new ArrayList<>();
    private ArrayList<CommonWorkFormEnt> ddlArrayListWork = new ArrayList<>();
    private ArrayList<CommonMaterialFormEnt> ddlArrayListMaterial = new ArrayList<>();
    private ArrayList<CommonListRecipientEnt> recipientData = new ArrayList<>();

    private ArrayList<CommonTowerFormEnt> ListTowerTemp = new ArrayList<>();
    private ArrayList<CommonFloorFormEnt> ListFloorTemp = new ArrayList<>();
    private ArrayList<CommonAreaFormEnt> ListAreaTemp = new ArrayList<>();
    private ArrayList<CommonWorkFormEnt> ListWorkTemp = new ArrayList<>();

    private SessionManager session;
    MMController controller;
    public ProgressDialog pDialog;
    private EditText etVolumeRequest, etTower,etFloor,etArea,etWork, etLokasi1, etUnit1, etRequestVolume1;
    private TextView tvUnggahFoto, tvRequestBy, tvReadyStock, tvIdleStock, tvMyStock1, tvIdleStock1;
    JSONObject item;
    private ImageView imageHolder, ivItem, ivLocation, ivWork, ivMaterial;
    private final int requestCode = 30;
    private int year, month, day;
    Uri photoURI;
    public String mCurrentPhotoPath;
    static final int REQUEST_TAKE_PHOTO = 1;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    public LinearLayout llMoreForm;

    //Calendar myCalendar;
    Calendar myCalendar = new GregorianCalendar();
    public String id_daily_log_temp;

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);

    private static final int request_data_from  = 20;
    private static DecimalFormat df2 = new DecimalFormat(".##");
    public int count = 1;
    private Dialog dialog;
    private RecyclerView recyclerView;
    public List<ListLokasiEnt> listItems = new ArrayList<>();
    public List<ListReadyStockEnt> listItemsReadyStock = new ArrayList<>();
    public List<ListIdleStockEnt> listItemsIdleStock = new ArrayList<>();
    public ListLokasiAdapter adapter;

    @BindView(R.id.btn_bottom_sheet)
    Button btnBottomSheet;

    @BindView(R.id.bottom_sheet)
    LinearLayout layoutBottomSheet;

    @BindView(R.id.btnSaveBar)
    Button btnSaveBar;

    @BindView(R.id.spinnerRecipientBar)
    SearchableSpinner spinnerRecipient;

    @BindView(R.id.ivCourierPhotoBar)
    ImageView ivCourierPhotoBar;

    @BindView(R.id.etTotalReqVol)
    EditText etTotalReqVol;

    @BindView(R.id.etUnitReqItem)
    EditText etUnitReqItem;

    BottomSheetBehavior sheetBehavior;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requestmaterialoutformfinal2);
        ButterKnife.bind(this);

        session = new SessionManager(getApplicationContext());

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if(bd != null)
        {
            fromrequestmaterial = (String) bd.get("fromrequestmaterial");
            Log.d("fromrequestmaterial", fromrequestmaterial);
        }

        //JIKA TIDAK ADA AKSES
        if(!session.getCanInsert().toString().equals("1"))
        {
            Toast.makeText(getApplicationContext(),"You dont have Privilege to Insert!", Toast.LENGTH_SHORT).show();
            this.finish();
        }

        today = dateFormat.format(myCalendar.getTime());

        year = myCalendar.get(Calendar.YEAR);
        month = myCalendar.get(Calendar.MONTH);
        day = myCalendar.get(Calendar.DAY_OF_MONTH);

        llMoreForm = (LinearLayout) findViewById(R.id.llMoreForm);
        spinnerMaterial1  = (SearchableSpinner) findViewById(R.id.spinnerMaterial1);
        btnSave = (Button) findViewById(R.id.btnSave);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);

        getSupportActionBar().setTitle("Request Material Out");
        session = new SessionManager(getApplicationContext());

        controller = new MMController();
        pDialog = new ProgressDialog(this);
        dialog = new Dialog(RequestMaterialOutFinal2Activity.this);

        //Start Recycler View
        recyclerView = (RecyclerView) findViewById(R.id.recyclerVListLokasi);
        adapter = new ListLokasiAdapter(getApplicationContext(), listItems, listItemsReadyStock, listItemsIdleStock,session,controller,sheetBehavior, btnBottomSheet, etTotalReqVol, etUnitReqItem, this);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapter);

        getDataForDDL6(spinnerMaterial1, "SPT_SPR_Detail_Inq_MM_Param", "ID_M_Katalog", "Spec", ddlArrayListMaterial, "@TotalRecords", "", "@currentpage", "1", "@pagesize", "3000", "@sortby", "", "@wherecond", " WHERE b.Kode_Proyek ='" + session.getKodeProyek() + "' AND f.UserCode ='" + work + "' AND convert(varchar, M_Area.ID) ='" + area + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");

        spinnerMaterial1.setTitle("Select Material");
        spinnerMaterial1.setPositiveButton("OK");
        spinnerMaterial1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonMaterialFormEnt commonEnt = (CommonMaterialFormEnt) parent.getSelectedItem();
                material = commonEnt.getValue();
                thsprid = commonEnt.getValue4();
                unit = commonEnt.getValue3();
                tdsprid = commonEnt.getValue6();
                typeitem = commonEnt.getValue7();

                if(!material.equals("-")) {

                    Log.d("materialChoose", material);
                    Log.d("materialChoose", thsprid);
                    Log.d("materialChoose", unit);
                    Log.d("LogMo1",typeitem);
                    Log.d("LogMo1Floor",floorfull);
                    Log.d("LogMo1Area",area);
//                    gtvIdleStockQty(material);
//                    if(!tdsprid.equals("null") || !tdsprid.equals(""))
//                    {

                        if(typeitem.equals("Prelim"))
                        {
                            getTowerLantaiZona(spinnerTower, "SPM_GetTowerLantaiAreaTempParam", "", "", ListTowerTemp, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "100", "@sortby", "", "@wherecond", " WHERE thspr.ID ='" + thsprid + "' "+typeitem+" AND tdspr.Kd_Anak_Kd_Material = '"+material+"'", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", ListFloorTemp, ListAreaTemp, ListWorkTemp, spinnerFloor, spinnerArea, spinnerWork, material, typeitem);
                        }
                        else
                        {
                            getTowerLantaiZona(spinnerTower, "SPM_GetTowerLantaiAreaTempParam", "", "", ListTowerTemp, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "100", "@sortby", "", "@wherecond", " WHERE thspr.ID ='" + thsprid +"' AND tdspr.Kd_Anak_Kd_Material = '"+material+"'", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", ListFloorTemp, ListAreaTemp,ListWorkTemp, spinnerFloor, spinnerArea, spinnerWork, material, typeitem);
                        }
//                    }
//                    else
//                    {
////                        gtvReadyStockQty(material, typeitem);
//                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnSaveBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!savePath.equals(""))
                {
                    saveRequestMOLoop();
                }
                else
                {
                    Toast toast = Toast.makeText(RequestMaterialOutFinal2Activity.this,"Courier Picture required!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
//                for (int index = 0; index < listItems.size(); index++)
//                {
//                    Log.d("LogListItemsId", listItems.get(index).getId());
//                    Log.d("LogListItemsKodeTower", listItems.get(index).getKodeTower());
//                    Log.d("LogListItemsNamaZona", listItems.get(index).getNama_Zona());
//                    Log.d("LogListItemsKodeLokasi", listItems.get(index).getKodeLokasi());
//                    Log.d("LogListItemsNama_Lokasi", listItems.get(index).getNama_Lokasi());
//                    Log.d("LogListItemsKodeArea", listItems.get(index).getKodeArea());
//                    Log.d("LogListItemsDeskripsi", listItems.get(index).getDeskripsi());
//                    Log.d("LogListItemsKodePek", listItems.get(index).getKodePekerjaan());
//                    Log.d("LogListItemsNamaPek", listItems.get(index).getNamaPekerjaan());
//                    Log.d("LogListItemsThSprId", listItems.get(index).getThsprid());
//                    Log.d("LogListItemsKodeItem", listItems.get(index).getKodeItem());
//                    Log.d("LogListItemsVolReady", listItems.get(index).getVolRequest());
//                    Log.d("LogListItemsVolIdle", listItems.get(index).getVolIdle());
//
//                }
            }
        });

        InitialRecipientSpinner();

        ivCourierPhotoBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent photoCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //startActivityForResult(photoCaptureIntent, requestCode);

                dispatchTakePictureIntent();
            }
        });

    }

    public void InitialRecipientSpinner()
    {
        CommonListRecipientEnt itemEntsinit = new CommonListRecipientEnt(
                session.getUserName(), session.getKeyNik(),
                "-","-",
                "-","-",
                "-","-",
                "-","-"
        );
        recipientData.add(itemEntsinit);

        ArrayAdapter<CommonListRecipientEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                android.R.layout.simple_spinner_dropdown_item, recipientData);
        spinnerRecipient.setAdapter(adapterDDLGlobal);
    }

//    public void setTotalRequestItem()
//    {
//        float total = 0;
//       for(int x = 0; x < listItems.size(); x++)
//       {
//           total = total+Float.parseFloat(listItems.get(x).getVolRequest().toString());
//       }
//
//        etTotalReqVol.setText(String.valueOf(total));
//
//        if(listItemsReadyStock.size() > 0)
//        {
//            etUnitReqItem.setText(listItemsReadyStock.get(0).getUnit().toString());
//        }
//    }

    @OnClick(R.id.btn_bottom_sheet)
    public void toggleBottomSheet() {
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            btnBottomSheet.setText("Close sheet");
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            btnBottomSheet.setText("Expand sheet");
        }
    }

    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //MainActivity activity = (MainActivity)getActivity();

        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
//            photoFile = getOutputMediaFile(); //untuk sementara
//            try {
//                photoFile = createImageFile();
//
//            } catch (IOException ex) {
//                Toast toast = Toast.makeText(activity, "There was a problem saving the photo...", Toast.LENGTH_SHORT);
//                toast.show();
//            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                mCurrentPhotoPath = photoFile.getAbsolutePath();
//                Log.d("path",mCurrentPhotoPath);
//                photoURI = Uri.fromFile(photoFile);

                photoURI = FileProvider.getUriForFile(getApplicationContext(), "com.totalbp.mmrequest.fileprovider", photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,photoURI);
//                takePictureIntent.putExtra("id_daily_log",id_daily_log);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private  File createImageFile() throws IOException { //bener2 simpen filenya di storage directory
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()){
            if (!storageDir.mkdirs()){
                return null;
            }
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        return image;
    }

    private void displayDialog(String id_daily_log) {
        UploadPicture(id_daily_log);
        //materialDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            Toast.makeText(getBaseContext(), "Photo Taken",Toast.LENGTH_SHORT).show();

            displayDialog(id_daily_log_temp);

            final Bitmap selectedImage = decodeSampledBitmapFromFile(mCurrentPhotoPath,500, 250);
            String encodedImage = getStringImage(selectedImage);

            byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            //Start
            //Code By Ezra
            //bitmapnya dideclare ulang terlebih dahulu untuk dibenarkan rotatenya kemudian baru diset ke imageview
            ExifInterface ei = null;
            try {
                ei = new ExifInterface(mCurrentPhotoPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            Bitmap rotatedBitmap = null;
            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(decodedByte, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(decodedByte, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(decodedByte, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = decodedByte;
            }
            //Close
            ivCourierPhotoBar.setImageBitmap(rotatedBitmap);

        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private void UploadPicture(String id_daily_log){
        final Bitmap selectedImage = decodeSampledBitmapFromFile(mCurrentPhotoPath,500, 500);
        String filename="DP" +"_"+ mCurrentPhotoPath.substring(mCurrentPhotoPath.lastIndexOf("/")+1);
        String encodedImage = getStringImage(selectedImage);

        //Start
        //Code by Ezra
        //Decode terlebih dahulu untuk dibenarkan rotatenya kemudian baru di upload
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        ExifInterface ei = null;
        try {
            ei = new ExifInterface(mCurrentPhotoPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap rotatedBitmap = null;
        switch(orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(decodedByte, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(decodedByte, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(decodedByte, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = decodedByte;
        }

        String encodedImage2 = getStringImage(rotatedBitmap);
        //end
        UploadFileToServer(encodedImage2,filename, id_daily_log);

    }

    public void UploadFileToServer(String base64encode, String filename, final String id_hse_assessment){

        MProgressDialog.showProgressDialog(RequestMaterialOutFinal2Activity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        RequestQueue requestQueue;
        session = new SessionManager(getApplicationContext());
        JSONObject request = new JSONObject();

        try {
            request.put("Pengubah", session.getUserDetails().get("nik"));
            request.put("FileName", filename);
            request.put("TokenID", session.getUserToken());
            request.put("FormID", "AR.03.01");
            request.put("KodeProyek",session.getKodeProyek()); //Dummy
            request.put("Base64String",base64encode);
        } catch (Exception e) {
            e.printStackTrace();
        }

        requestQueue = Volley.newRequestQueue(getApplicationContext());
//        http://10.100.223.51:20173/API/TQA/Upload
        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, session.getUrlConfig()+AppConfig.URL_UPLOAD_GAMBAR,request,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("LogUploadPicPenerimaGbr",response.toString());
                            if(response.getString("IsSucceed").equals("true")){
                               // Toast.makeText(getApplicationContext(), response.getString("Message"), Toast.LENGTH_LONG).show();
//                                String savePath = response.getString("VarOutput");
                                String pathDatabase = response.getString("VarOutput"); //hashcode
                                String yearstring = String.valueOf(year);
                                String monthstring = "0"+String.valueOf(month+1);
                                savePath = session.getUrlConfig()+URL_IMAGE_PREFIX + "/GeneralFile/"+yearstring.substring(yearstring.length()-2)+"/"+monthstring.substring(monthstring.length()-2)+"/" + response.getString("Message") + ".jpg";

                                Log.d("savePath",savePath);
                                MProgressDialog.dismissProgressDialog();

                            }
                            else{
                                Toast.makeText(getApplicationContext(), response.getString("Message "+"Please Re-Login"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Error Response " + error.toString(), Toast.LENGTH_LONG).show();
                    }
                });
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(strReq);
    }

    private void saveRequestMO(String qty, String qty2) {

        MProgressDialog.showProgressDialog(RequestMaterialOutFinal2Activity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        if(tdsprid.equals("null"))
        {
            tdsprid = "0";
        }
        Gson gsonHeader = new Gson();
        CommonRequestMoFormEnt commonRequestMoFormEnt = new CommonRequestMoFormEnt();
        commonRequestMoFormEnt.setID("0");
        commonRequestMoFormEnt.setKode_MO(session.getKodeProyek());
        commonRequestMoFormEnt.setKode_Proyek(session.getKodeProyek());
        commonRequestMoFormEnt.setKode_Item(material);
        commonRequestMoFormEnt.setVolume(qty);
        commonRequestMoFormEnt.setUnit(unit);
        commonRequestMoFormEnt.setWork_Code(work2);
        commonRequestMoFormEnt.setKode_Tower(tower2);
        commonRequestMoFormEnt.setKode_Lantai(floorfull2);
        commonRequestMoFormEnt.setKode_Zona(area2);
        commonRequestMoFormEnt.setCourierPhoto(savePath);
        commonRequestMoFormEnt.setRefHeaderSPR(thsprid);
        commonRequestMoFormEnt.setRefDetailSPR(tdsprid);
        commonRequestMoFormEnt.setStatusRequest("1");
        commonRequestMoFormEnt.setApprovalNo("");
        commonRequestMoFormEnt.setStatusAktif("True");
        commonRequestMoFormEnt.setPembuat(session.getUserName());
        commonRequestMoFormEnt.setNama_Pembuat(session.getKeyNik());
        commonRequestMoFormEnt.setWaktuBuat(today);
        commonRequestMoFormEnt.setPengubah(session.getKeyNik());
        commonRequestMoFormEnt.setNama_Pengubah(session.getKeyNik());
        commonRequestMoFormEnt.setWaktuUbah(today);
        commonRequestMoFormEnt.setComment("");
        commonRequestMoFormEnt.setTokenID(session.getUserToken());
        commonRequestMoFormEnt.setVolumeIdle(qty2);
        //commonRequestMoFormEnt.setModeReq("POSTING");
        commonRequestMoFormEnt.setFormId("MAMAN.02");
        String jsonString = gsonHeader.toJson(commonRequestMoFormEnt);

        controller.SaveGeneralObjectRequestMo(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                    MProgressDialog.dismissProgressDialog();
                    try {

                        JSONObject obj = new JSONObject(output);

                        if(!obj.getString("IsSucceed").equals("false")) {

                            String output2 = obj.getString("VarOutput");
                            String[] split = output2.split("#");
                            String firstString = split[0];
                            String firstSecondstring = split[1];

                            isCanPosting(firstString, firstSecondstring);
                        }
                        else
                        {
                            Toast toast = Toast.makeText(RequestMaterialOutFinal2Activity.this,"Save Failed, "+obj.getString("Message")+" ! ", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                            Intent intent = new Intent();
                            intent.putExtra("", "");
                            setResult(request_data_from, intent);
                            finish();
                        }

                    } catch (Throwable t) {
                        Log.e("My App", "Could not parse malformed JSON: \"" + output + "\"");
                    }
            }
        });
    }

    private void saveRequestMOLoop() {
        int x = 0;

        for(x = 0; x < listItems.size(); x++)
        {
            //yang statusnya checkbox true
            if(listItems.get(x).getStatusPost().toString().equals("1"))
            {
                MProgressDialog.showProgressDialog(RequestMaterialOutFinal2Activity.this, true, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        MProgressDialog.dismissProgressDialog();
                    }
                });

                if (listItems.get(x).getId().toString().equals("null")) {
                    tdsprid = "0";
                }
                Gson gsonHeader = new Gson();
                CommonRequestMoFormEnt commonRequestMoFormEnt = new CommonRequestMoFormEnt();
                commonRequestMoFormEnt.setID("0");
                commonRequestMoFormEnt.setKode_MO(session.getKodeProyek());
                commonRequestMoFormEnt.setKode_Proyek(session.getKodeProyek());
                commonRequestMoFormEnt.setKode_Item(listItems.get(x).getKodeItem().toString());
                commonRequestMoFormEnt.setVolume(listItems.get(x).getVolRequest().toString());
                commonRequestMoFormEnt.setUnit(listItemsReadyStock.get(0).getUnit().toString());
                commonRequestMoFormEnt.setWork_Code("0");
                commonRequestMoFormEnt.setKode_Tower(listItems.get(x).getKodeTower().toString());
                commonRequestMoFormEnt.setKode_Lantai(listItems.get(x).getKodeLokasi().toString());
                commonRequestMoFormEnt.setKode_Zona(listItems.get(x).getKodeArea().toString());
                commonRequestMoFormEnt.setCourierPhoto(savePath);
                commonRequestMoFormEnt.setRefHeaderSPR(listItems.get(x).getThsprid().toString());
                commonRequestMoFormEnt.setRefDetailSPR(tdsprid);
                commonRequestMoFormEnt.setStatusRequest("1");
                commonRequestMoFormEnt.setApprovalNo("");
                commonRequestMoFormEnt.setStatusAktif("True");
                commonRequestMoFormEnt.setPembuat(session.getKeyNik());
                commonRequestMoFormEnt.setNama_Pembuat(session.getKeyNik());
                commonRequestMoFormEnt.setWaktuBuat(today);
                commonRequestMoFormEnt.setPengubah(session.getKeyNik());
                commonRequestMoFormEnt.setNama_Pengubah(session.getKeyNik());
                commonRequestMoFormEnt.setWaktuUbah(today);
                commonRequestMoFormEnt.setComment("");
                commonRequestMoFormEnt.setTokenID(session.getUserToken());
                commonRequestMoFormEnt.setVolumeIdle(listItems.get(x).getVolIdle().toString());
                //commonRequestMoFormEnt.setModeReq("POSTING");
                commonRequestMoFormEnt.setFormId("MAMAN.02");
                String jsonString = gsonHeader.toJson(commonRequestMoFormEnt);

                int finalX = x + 1;
                controller.SaveGeneralObjectRequestMo(getApplicationContext(), jsonString, new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        // MProgressDialog.dismissProgressDialog();
                    }

                    @Override
                    public void onSave(String output) {

                        MProgressDialog.dismissProgressDialog();
                        try {

                            JSONObject obj = new JSONObject(output);

                            if (!obj.getString("IsSucceed").equals("false")) {

                                String output2 = obj.getString("VarOutput");
                                String[] split = output2.split("#");
                                String firstString = split[0];
                                String firstSecondstring = split[1];

                                isCanPosting(firstString, firstSecondstring);
                                Toast toast = Toast.makeText(RequestMaterialOutFinal2Activity.this, "Save Success, " + firstString + " ! ", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                            } else {
                                Toast toast = Toast.makeText(RequestMaterialOutFinal2Activity.this, "Save Failed, " + obj.getString("Message") + " ! ", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();

                                Intent intent = new Intent();
                                intent.putExtra("", "");
                                setResult(request_data_from, intent);
                                finish();
                            }

                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + output + "\"");
                        }
                    }
                });
            }
            else
            {
                Toast toast = Toast.makeText(RequestMaterialOutFinal2Activity.this, "No Item Input!", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }

    }

    private void isCanPosting(String kodematerialout, String IdMo) {

        MProgressDialog.showProgressDialog(RequestMaterialOutFinal2Activity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("is_can_posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
//        approvalEnt.setApproval_Name("Approval Material Out Management Mobile");
        approvalEnt.setTokenID(session.getUserToken());

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

                if(jsonObject.get("IsCanPosting").getAsString().equals("true"))
                {
                    dialog = new Dialog(RequestMaterialOutFinal2Activity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.general_dialog);
                    dialog.setCanceledOnTouchOutside(false);

                    TextView dTittle = (TextView) dialog.findViewById(R.id.dTittle);
                    TextView dContent = (TextView) dialog.findViewById(R.id.dContent);
                    Button dBtnCancel = (Button) dialog.findViewById(R.id.dBtnCancel);
                    Button dBtnSubmit = (Button) dialog.findViewById(R.id.dBtnSubmit);

                    dTittle.setText("Material Out");
                    dContent.setText("Do you want Post Mo number "+kodematerialout+" ?");

                    dBtnSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onPosting(kodematerialout, IdMo);
                        }
                    });


                    dBtnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            Intent intent = new Intent();
                            intent.putExtra("", "");
                            setResult(request_data_from, intent);
                            finish();
                        }
                    });

                    dialog.show();


                }
                else
                {
                    Toast toast = Toast.makeText(RequestMaterialOutFinal2Activity.this,"Request Material Save Success!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    Intent intent = new Intent();
                    intent.putExtra("", "");
                    setResult(request_data_from, intent);
                    finish();
                }

            }
        });
    }

    private void onPosting(String kodematerialout, String IdMo) {

        MProgressDialog.showProgressDialog(RequestMaterialOutFinal2Activity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        //Log.d("OmOm", kodematerialout+"|"+IdMo);
        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
        approvalEnt.setTokenID(session.getUserToken());

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {


                Log.d("OmOm2", output);
                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

//                if(jsonObject.get("IsCanApprove").getAsString().equals("true"))
//                {
//                    Log.d("OmOm3", output);
                    updateRequestMO(jsonObject.get("Approval_Number").getAsString(), IdMo, kodematerialout);
//                }
//                else
//                {
//                    Toast toast = Toast.makeText(RequestMaterialOutActivity.this,"You already posting !", Toast.LENGTH_LONG);
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();
//
//                    Intent intent = new Intent();
//                    intent.putExtra("", "");
//                    setResult(request_data_from, intent);
//                    finish();
//                }

            }
        });
    }

    private void IsCanApprove(String kodematerialout, String ApprovalNumber, String IdMo) {

        MProgressDialog.showProgressDialog(RequestMaterialOutFinal2Activity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        //Log.d("OmOm", kodematerialout+"|"+IdMo);
        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
        approvalEnt.setTokenID(session.getUserToken());

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

//                Log.d("OmOm2", output);
//                MProgressDialog.dismissProgressDialog();
//                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
//                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

                //updateRequestMO(jsonObject.get("Approval_Number").getAsString(), IdMo);
            }
        });
    }

    private void updateRequestMO(String ApprovalNumber, String IdMo, String KodeMaterialOut) {
        Log.d("OmOm4", ApprovalNumber);
        MProgressDialog.showProgressDialog(RequestMaterialOutFinal2Activity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();

        CommonRequestMoFormEnt commonRequestMoFormEnt = new CommonRequestMoFormEnt();
        commonRequestMoFormEnt.setID(IdMo);
        commonRequestMoFormEnt.setKode_MO(KodeMaterialOut);
        commonRequestMoFormEnt.setKode_Proyek(session.getKodeProyek());
        commonRequestMoFormEnt.setKode_Item("");
        commonRequestMoFormEnt.setVolume("1");
        commonRequestMoFormEnt.setUnit("");
        commonRequestMoFormEnt.setWork_Code("");
        commonRequestMoFormEnt.setKode_Tower("");
        commonRequestMoFormEnt.setKode_Lantai("");
        commonRequestMoFormEnt.setKode_Zona("0");
        commonRequestMoFormEnt.setCourierPhoto("");
        commonRequestMoFormEnt.setRefHeaderSPR("0");
        commonRequestMoFormEnt.setRefDetailSPR("0");
        commonRequestMoFormEnt.setStatusRequest("1");
        commonRequestMoFormEnt.setApprovalNo(ApprovalNumber);
        commonRequestMoFormEnt.setStatusAktif("True");
        commonRequestMoFormEnt.setPembuat(session.getUserName());
        commonRequestMoFormEnt.setNama_Pembuat(session.getUserName());
        commonRequestMoFormEnt.setWaktuBuat(today);
        commonRequestMoFormEnt.setPengubah(session.getKeyNik());
        commonRequestMoFormEnt.setNama_Pengubah(session.getUserName());
        commonRequestMoFormEnt.setWaktuUbah(today);
        commonRequestMoFormEnt.setComment("");
        commonRequestMoFormEnt.setTokenID(session.getUserToken());
        //commonRequestMoFormEnt.setModeReq("POSTING");
        commonRequestMoFormEnt.setFormId("MAMAN.02");
//
        String jsonString = gsonHeader.toJson(commonRequestMoFormEnt);
        Log.d("OmOm5", ApprovalNumber);
        controller.SaveGeneralObjectRequestMo(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                Toast toast = Toast.makeText(RequestMaterialOutFinal2Activity.this,KodeMaterialOut+" Posting Success !", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                Intent intent = new Intent();
                intent.putExtra("", "");
                setResult(request_data_from, intent);
                finish();

            }
        });
    }

    public void initControls(){


        //getDataForDDL(spinnerSpr,"SPM_GetThSprMobile_Param","ID","Kode_Spr",ddlArrayListTower,"@KodeProject",session.getKodeProyek(),"@currentpage","1","@pagesize","10000","@sortby","","@wherecond"," AND Kode_Proyek ='"+session.getKodeProyek()+"' ");
        //spinnerSpr.setTitle("Select SPR");
        //spinnerSpr.setPositiveButton("OK");
        //spinnerSpr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                final CommonEnt commonEnt = (CommonEnt) parent.getSelectedItem();
//                spr = commonEnt.getValue();
//                Log.d("sprChoose", spr);

//                if(!spr.equals("-")) {
                    getDataForDDL2(spinnerTower, "SPM_GetTower_Param", "Kode_Tower", "Nama_Tower", ddlArrayListTower2, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "100", "@sortby", "", "@wherecond", " AND Kode_Proyek ='" + session.getKodeProyek() + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    getDataForDDL6(spinnerMaterial, "SPT_SPR_Detail_Inq_MM_Param", "ID_M_Katalog", "Spec", ddlArrayListMaterial, "@TotalRecords", "", "@currentpage", "1", "@pagesize", "3000", "@sortby", "", "@wherecond", " WHERE b.Kode_Proyek ='" + session.getKodeProyek() + "' AND f.UserCode ='" + work + "' AND convert(varchar, M_Area.ID) ='" + area + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");

        //
                    //gtvReadyStockQty(spr);
                //}
            //}
            //@Override
            //public void onNothingSelected(AdapterView<?> parent) {
            //}
        //});

        spinnerTower.setTitle("Select Tower");
        spinnerTower.setPositiveButton("OK");
        spinnerTower.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonTowerFormEnt commonEnt = (CommonTowerFormEnt) parent.getSelectedItem();
                tower = commonEnt.getValue();
                if(!tower.equals("-")) {
                    getDataForDDL3(spinnerFloor, "SPM_GetFloor_Param", "Kode_Lantai", "Nama_Lantai", ddlArrayListFloor, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", " Nama_Lantai ASC ", "@wherecond", " AND M_Zona.Kode_Proyek ='" + session.getKodeProyek() + "' AND M_Zona.Kode_Zona ='" + tower + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    Log.d("towerChoose", tower);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerFloor.setTitle("Select Floor");
        spinnerFloor.setPositiveButton("OK");
        spinnerFloor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonFloorFormEnt commonEnt = (CommonFloorFormEnt) parent.getSelectedItem();
                floor = commonEnt.getValue();
                floortype = commonEnt.getValue2();
                floorfull = floortype+floor;
                //work = commonEnt.getValue();
                if(!floor.equals("-")) {
                    getDataForDDL4(spinnerArea, "SPM_GetArea_Param", "Kode_Lokasi", "Nama_Area", ddlArrayListArea, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", "", "@wherecond", " AND M_Zona.Kode_Proyek ='" + session.getKodeProyek() + "' AND M_Zona.Kode_Zona ='" + tower + "' AND M_Area.Kode_Lokasi ='" + floorfull + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    //getDataForDDL5(spinnerWork, "SPM_GetWorkBasedSprAndTowerAndFloorMobile_Param", "Kode_Work", "Nama_Work", ddlArrayListWork, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", "", "@wherecond", " AND a.Kode_Proyek ='" + session.getKodeProyek() + "' AND d.Kode_Zona ='" + tower + "' AND c.Kode_Lokasi ='" + floor + "' ");
                    Log.d("floorChoose", floor);
                   // Log.d("workChoose", work);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerArea.setTitle("Select Area");
        spinnerArea.setPositiveButton("OK");
        spinnerArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonAreaFormEnt commonEnt = (CommonAreaFormEnt) parent.getSelectedItem();
                area = commonEnt.getValue();
                if(!area.equals("-")) {
                    getDataForDDL5(spinnerWork, "SPM_GetWork_Param", "Kode_Pekerjaan", "Nama_Pekerjaan", ddlArrayListWork, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", "", "@wherecond", " WHERE [M_Area].Kode_Proyek = '"+session.getKodeProyek()+"' AND m_zona.Kode_Zona = '"+tower+"' AND m_lokasi.Tipe_Lokasi+m_lokasi.Kode_Lokasi = '"+floorfull+"' AND [M_Area].ID = '"+area+"' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    Log.d("areaChoose", area);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerWork.setTitle("Select Work");
        spinnerWork.setPositiveButton("OK");
        spinnerWork.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonWorkFormEnt commonEnt = (CommonWorkFormEnt) parent.getSelectedItem();
                work = commonEnt.getValue();
                if(!work.equals("-")) {
                    //getDataForDDL6(spinnerMaterial, "SPT_SPR_Detail_Inq_MM_Param", "ID_M_Katalog", "Spec", ddlArrayListMaterial, "@TotalRecords", "", "@currentpage", "1", "@pagesize", "1000", "@sortby", "", "@wherecond", " AND b.Kode_Proyek ='" + session.getKodeProyek() + "' AND f.UserCode ='"+work+"' ", "@nik", String.valueOf(session.isLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    getDataForDDL6(spinnerMaterial, "SPT_SPR_Detail_Inq_MM_Param", "ID_M_Katalog", "Spec", ddlArrayListMaterial, "@TotalRecords", "", "@currentpage", "1", "@pagesize", "2000", "@sortby", "", "@wherecond", " WHERE b.Kode_Proyek ='" + session.getKodeProyek() + "' AND f.UserCode ='" + work + "' AND convert(varchar, M_Area.ID) ='" + area + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");

                    Log.d("workChoose", work);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerMaterial.setTitle("Select Material");
        spinnerMaterial.setPositiveButton("OK");
        spinnerMaterial.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonMaterialFormEnt commonEnt = (CommonMaterialFormEnt) parent.getSelectedItem();
                material = commonEnt.getValue();
                thsprid = commonEnt.getValue4();
                unit = commonEnt.getValue3();
                tdsprid = commonEnt.getValue6();
                typeitem = commonEnt.getValue7();



                if(!material.equals("-")) {

                    Log.d("materialChoose", material);
                    Log.d("materialChoose", thsprid);
                    Log.d("materialChoose", unit);
                    Log.d("LogMo1",typeitem);
                    Log.d("LogMo1Floor",floorfull);
                    Log.d("LogMo1Area",area);
                    gtvIdleStockQty(material, tdsprid, "0");
                    if(!tdsprid.equals("null") || !tdsprid.equals(""))
                    {

                       if(typeitem.equals("Prelim"))
                       {
                           getTowerLantaiZona(spinnerTower, "SPM_GetTowerLantaiAreaTempParam", "", "", ListTowerTemp, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "100", "@sortby", "", "@wherecond", " WHERE tdspr.ID ='" + tdsprid + "' "+typeitem, "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", ListFloorTemp, ListAreaTemp, ListWorkTemp, spinnerFloor, spinnerArea, spinnerWork, material, typeitem);
                       }
                       else
                       {
                           getTowerLantaiZona(spinnerTower, "SPM_GetTowerLantaiAreaTempParam", "", "", ListTowerTemp, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "100", "@sortby", "", "@wherecond", " WHERE tdspr.ID ='" + tdsprid + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", ListFloorTemp, ListAreaTemp,ListWorkTemp, spinnerFloor, spinnerArea, spinnerWork, material, typeitem);
                       }
                    }
                    else
                    {
//                        gtvReadyStockQty(material, typeitem);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //getDataForDDL3(spinnerZone,"SPM_GetArea_Param","Kode_Area","Nama_Area",ddlArrayListTower2,"@KodeProject",session.getKodeProyek(),"@currentpage","1","@pagesize","100","@sortby","","@wherecond"," AND Kode_Proyek ='"+session.getKodeProyek()+"' GROUP BY ID, Deskripsi");
        //getDataForDDL4(spinnerMaterial,"SPM_GetMaterial_Param","Kode_Item","Nama_Item",ddlArrayListTower3,"@KodeProject",session.getKodeProyek(),"@currentpage","1","@pagesize","200","@sortby","","@wherecond"," GROUP BY ID, Deskripsi");
    }

    public void showDialog() {
        pDialog.setMessage("Loading ...");
        if (!pDialog.isShowing())
            pDialog.show();
    }

    public void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void getDataForDDL(final SearchableSpinner spinner, final String uniqueSPName,
                              final String paramName, final String paramVal,
                              final ArrayList<CommonEnt> ddlArrayList,
                              final String paramName1, final String paramVal1,
                              final String paramName2, final String paramVal2,
                              final String paramName3, final String paramVal3,
                              final String paramName4, final String paramVal4,
                              final String paramName5, final String paramVal5,
                              final String paramName6, final String paramVal6,
                              final String paramName7, final String paramVal7,
                              final String paramName8, final String paramVal8){
        ddlArrayList.clear();

        MProgressDialog.showProgressDialog(RequestMaterialOutFinal2Activity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
            @Override
            public void onSuccess(JSONArray result) {
                try {
                    if (result.length() > 0) {

                        CommonEnt itemEntsinit = new CommonEnt(
                                "-","-",
                                "-","-",
                                "-","-",
                                "-","-",
                                "-","-"
                        );
                        ddlArrayList.add(itemEntsinit);

                        for (int i = 0; i < result.length(); i++) {
                            JSONObject item = result.getJSONObject(i);

                            CommonEnt itemEnts = new CommonEnt(item.getString(paramVal), item.getString(paramName),
                                    item.getString(paramName), item.getString(paramName),
                                    item.getString(paramName), item.getString(paramName),
                                    item.getString(paramName), item.getString(paramName),
                                    item.getString(paramName), item.getString(paramName)
                            );
                            ddlArrayList.add(itemEnts);
                        }
                        MProgressDialog.dismissProgressDialog();
                        ArrayAdapter<CommonEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                        spinner.setAdapter(adapterDDLGlobal);
                    }
                    else{
                        CommonEnt itemEnts = new CommonEnt(
                                "No Item Found","",
                                "No Item Found","",
                                "No Item Found","",
                                "No Item Found","",
                                "No Item Found",""
                        );
                        ddlArrayList.add(itemEnts);
                        ArrayAdapter<CommonEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                        spinner.setAdapter(adapterDDLGlobal);
                    }
                    MProgressDialog.dismissProgressDialog();
                }catch (JSONException e){
                    Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                }
            }


            @Override
            public void onSave(String output) {

            }
        });
    }

    public void getDataForDDL2(final SearchableSpinner spinner, final String uniqueSPName,
                              final String paramName, final String paramVal,
                              final ArrayList<CommonTowerFormEnt> ddlArrayList,
                              final String paramName1, final String paramVal1,
                              final String paramName2, final String paramVal2,
                              final String paramName3, final String paramVal3,
                              final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();

//        MProgressDialog.showProgressDialog(RequestMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                MProgressDialog.dismissProgressDialog();
//            }
//        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {

                                CommonTowerFormEnt itemEntsinit = new CommonTowerFormEnt(
                                        "-select tower-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );
                                ddlArrayList.add(itemEntsinit);

                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonTowerFormEnt itemEnts = new CommonTowerFormEnt(item.getString(paramVal), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                }
                               // MProgressDialog.dismissProgressDialog();
                                ArrayAdapter<CommonTowerFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonTowerFormEnt itemEnts = new CommonTowerFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonTowerFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            //MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void getTowerLantaiZona(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonTowerFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                                   final ArrayList<CommonFloorFormEnt> ddlArrayList2,
                                   final ArrayList<CommonAreaFormEnt> ddlArrayList3,
                                   final ArrayList<CommonWorkFormEnt> ddlArrayList4,
                                   final SearchableSpinner spinner2, final SearchableSpinner spinner3, final SearchableSpinner spinner4, String materialparam, String typeitemparam){
        ddlArrayList.clear();
        ddlArrayList2.clear();
        ddlArrayList3.clear();
        ddlArrayList4.clear();
        listItems.clear();


        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {

                                for (int i = 0; i < result.length(); i++) {

                                    JSONObject item = result.getJSONObject(i);

                                    ListLokasiEnt listLokasiEnt = new ListLokasiEnt(
                                            item.getString("Id"), item.getString("KodeTower"),
                                            item.getString("Nama_Zona"), item.getString("KodeLokasi"),
                                            item.getString("Nama_Lokasi"),item.getString("KodeArea"),
                                            item.getString("Deskripsi"),item.getString("KodePekerjaan"),
                                            item.getString("NamaPekerjaan"),item.getString("thsprid"),
                                            item.getString("KodeItem"), "0", "0", typeitemparam, "0"
                                    );

                                    listItems.add(listLokasiEnt);
                                    gtvReadyStockQty(item.getString("KodeItem"), typeitemparam, item.getString("Id"),  item.getString("KodeTower"),  item.getString("KodeLokasi"),  item.getString("KodeArea"), String.valueOf(i));
                                }
                            }
                            else{
//                                etLokasi1.setText("-");
//                                gtvReadyStockQty(materialparam, typeitemparam);
                                ListLokasiEnt listLokasiEnt = new ListLokasiEnt(
                                        "0", "0",
                                        "-", "0",
                                        "-","0",
                                        "-","0",
                                        "-","0",
                                        "-","0",
                                        "0","-","0"
                                );
                                listItems.add(listLokasiEnt);
                            }
                            MProgressDialog.dismissProgressDialog();
                            adapter.notifyDataSetChanged();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void gtvReadyStockQty(String material, String typeitem, String tdsprid, String kodetower, String kodelantai, String kodearea, String index){
        Log.d("material", material);

        String where = "";
        floorfull2 = floorfull;
        area2 = area;
        work2 = work;
        tower2 = tower;

        //Jagaan saat parameter kode area kosong berarti wherecond diganti untuk menampilkan item migrasi
        if(typeitem.equals("Migrasi-SPR") || typeitem.equals("Migrasi-Kontrak") || typeitem.equals("Migrasi-OtherKontrak") || typeitem.equals("Migrasi-AddKontrak") || typeitem.equals("Migrasi-NPO") || typeitem.equals("Prelim"))
        {
            where = " WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' ";
        }
        else
        {
            where = " WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' AND Kode_Lokasi = '"+kodelantai+"' AND Kode_Area = '"+kodearea+"' ";
        }

        session = new SessionManager(getApplicationContext());
        controller.InqGeneral3(getApplicationContext(),"SPM_GetQtyReadyStockFromViewParam",
                "@KodeProject",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond",where,
//                "@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' AND Kode_Lokasi = '"+floortype+floor+"' AND Kode_Area = '"+area+"' ",
                //"@nik",String.valueOf(session.isLoggedIn()),
                //"@formid","MAMAN.02",
                //"@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("EZRAgtvReadyStockQty", result.toString());
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);

                                    ListReadyStockEnt listReadyEnt = new ListReadyStockEnt(
                                            tdsprid, item.getString("LatestVol"), item.getString("Unit")
                                    );

                                    listItemsReadyStock.add(listReadyEnt);

//                                    latestvol = item.getString("LatestVol");
                                }
//                                adapter.notifyDataSetChanged();
//                                tvMyStock1.setText(latestvol+" "+item.getString("Unit"));
//                                etUnit1.setText(item.getString("Unit"));
                                Log.d("LogTotal1",item.getString("LatestVol").toString());
                            }
                            else
                            {
                                ListReadyStockEnt listReadyEnt = new ListReadyStockEnt(
                                        tdsprid, "0","-"
                                );

                                listItemsReadyStock.add(listReadyEnt);
//                                Log.d("LogTotal2",item.getString("LatestVol").toString());
//                                latestvol = "0";
//                                tvMyStock1.setText("0");
                            }
                            gtvIdleStockQty(material, tdsprid, index);
                            adapter.notifyDataSetChanged();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH gtvReadyStockQty", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public String gtvReadyStockQtyChecker(String material){
        Log.d("material", material);
        session = new SessionManager(getApplicationContext());
        controller.InqGeneral3(getApplicationContext(),"SPM_GetQtyReadyStockFromViewParam",
                "@KodeProject",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' AND Kode_Tower = '"+tower+"' AND Kode_Lokasi = '"+floorfull+"' AND Kode_Area = '"+area+"' ",
                //"@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' +"' AND Requester = '"+session.getUserName()+"' ",
                //"@nik",String.valueOf(session.isLoggedIn()),
                //"@formid","MAMAN.02",
                //"@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("EZRAgtvReadyStockQty", result.toString());
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);
                                    latestvol = item.getString("LatestVol2");
                                }
                            }
                            else
                            {
                                latestvol = "0";
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH gtvReadyStockQty", Toast.LENGTH_LONG).show();
                        }
                    }
                });

        return (latestvol);
    }

    public void gtvIdleStockQty(String material, String tdsprid, String index){
        Log.d("material", material);
        session = new SessionManager(getApplicationContext());
        controller.InqGeneral3(getApplicationContext(),"SPM_GetQtyIdleStockFromView_Param",
                "@KodeProject",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond"," WHERE ttrans.Kode_Proyek = '"+session.getKodeProyek()+"' AND ttrans.Kode_Item = '"+material+"' AND ttrans.FlagReturnGudang = 1 ",
                //"@wherecond"," WHERE ttrans.Kode_Proyek = '"+session.getKodeProyek()+"' AND ttrans.Kode_Item = '"+material+"' AND ttrans.ToKode_Tower = '"+tower+"' AND ttrans.FlagReturnGudang = 1 ",
                //"@nik",String.valueOf(session.isLoggedIn()),
                //"@formid","MAMAN.02",
                //"@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("EZRAgtvIdleStockQty", result.toString());
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);
                                    idlveol = item.getString("vol");

                                    ListIdleStockEnt listIdleEnt = new ListIdleStockEnt(
                                            tdsprid, item.getString("vol")
                                    );

                                    listItemsIdleStock.add(listIdleEnt);

                                    //setelah vol idle yang sebenarnya didapatkan maka update vol idle di listlokasi yang sebelumnya didapatkan agar data valid
                                    ListLokasiEnt listLokasiEnt = new ListLokasiEnt(
                                            listItems.get(Integer.parseInt(index)).getId().toString(), listItems.get(Integer.parseInt(index)).getKodeTower().toString(),
                                            listItems.get(Integer.parseInt(index)).getNama_Zona().toString(), listItems.get(Integer.parseInt(index)).getKodeLokasi().toString(),
                                            listItems.get(Integer.parseInt(index)).getNama_Lokasi().toString(),listItems.get(Integer.parseInt(index)).getKodeArea().toString(),
                                            listItems.get(Integer.parseInt(index)).getDeskripsi().toString(),listItems.get(Integer.parseInt(index)).getKodePekerjaan().toString(),
                                            listItems.get(Integer.parseInt(index)).getNamaPekerjaan().toString(),listItems.get(Integer.parseInt(index)).getThsprid().toString(),
                                            listItems.get(Integer.parseInt(index)).getKodeItem().toString(), "0", item.getString("vol"),listItems.get(Integer.parseInt(index)).getTypeItem().toString(),
                                            listItems.get(Integer.parseInt(index)).getStatusPost().toString()
                                    );

                                    listItems.set((Integer.parseInt(index)),listLokasiEnt);
                                }
//                                tvIdleStock1.setText(idlveol);
                            }
                            else
                            {
                                ListIdleStockEnt listIdleEnt = new ListIdleStockEnt(
                                        tdsprid, "0"
                                );

                                listItemsIdleStock.add(listIdleEnt);
//                                idlveol = "0";
//                                tvIdleStock1.setText("0");
                            }
                            adapter.notifyDataSetChanged();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH IdleStockQty", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void getDataForDDL3(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonFloorFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();

        MProgressDialog.showProgressDialog(RequestMaterialOutFinal2Activity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {

                                CommonFloorFormEnt itemEntsinit = new CommonFloorFormEnt(
                                        "-select floor-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );
                                ddlArrayList.add(itemEntsinit);

                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonFloorFormEnt itemEnts = new CommonFloorFormEnt(item.getString(paramVal), item.getString(paramName),
                                            item.getString("Tipe_Lokasi"), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                }
                                MProgressDialog.dismissProgressDialog();
                                ArrayAdapter<CommonFloorFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonFloorFormEnt itemEnts = new CommonFloorFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonFloorFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void getDataForDDL4(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonAreaFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();

        MProgressDialog.showProgressDialog(RequestMaterialOutFinal2Activity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                CommonAreaFormEnt itemEntsinit = new CommonAreaFormEnt(
                                        "-select area-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );

                                ddlArrayList.add(itemEntsinit);
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonAreaFormEnt itemEnts = new CommonAreaFormEnt(item.getString(paramVal), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                }
                                MProgressDialog.dismissProgressDialog();
                                ArrayAdapter<CommonAreaFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonAreaFormEnt itemEnts = new CommonAreaFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonAreaFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void getDataForDDL5(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonWorkFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();

        MProgressDialog.showProgressDialog(RequestMaterialOutFinal2Activity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                CommonWorkFormEnt itemEntsinit = new CommonWorkFormEnt(
                                        "-select work-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );

                                ddlArrayList.add(itemEntsinit);
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonWorkFormEnt itemEnts = new CommonWorkFormEnt(item.getString(paramVal), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                }
                                MProgressDialog.dismissProgressDialog();
                                ArrayAdapter<CommonWorkFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonWorkFormEnt itemEnts = new CommonWorkFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonWorkFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void getDataForDDL6(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonMaterialFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();

        MProgressDialog.showProgressDialog(RequestMaterialOutFinal2Activity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            MProgressDialog.dismissProgressDialog();
                            Log.d("EZRAgetMaterial", result.toString());
                            if (result.length() > 0) {
                                CommonMaterialFormEnt itemEntsinit = new CommonMaterialFormEnt(
                                        "-select material-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );

                                ddlArrayList.add(itemEntsinit);
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonMaterialFormEnt itemEnts = new CommonMaterialFormEnt(item.getString(paramVal)+ " ("+item.getString("spesifikasi")+") - "+item.getString("Kode_SPR")+" | "+item.getString("TypeItem"), item.getString(paramName),
                                    //CommonMaterialFormEnt itemEnts = new CommonMaterialFormEnt(item.getString(paramVal)+ " ("+item.getString("spesifikasi")+") - "+item.getString("Kode_SPR")+"", item.getString(paramName),
                                            item.getString("Kd_Anak_Kd_Material"), item.getString("Unit"),
                                            item.getString("ID"), item.getString("Kode_SPR"),
                                            item.getString("Id_Td_SPR"), item.getString("TypeItem"),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                    //sprqty = item.getString("Volume");
                                    //etSprQty.setText(sprqty);
                                    //Log.d("sprqty", sprqty);
                                }
                               // MProgressDialog.dismissProgressDialog();
                                ArrayAdapter<CommonMaterialFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonMaterialFormEnt itemEnts = new CommonMaterialFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonMaterialFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                           // MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH getMaterial"+e.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MMController.getInstance().setConnectivityListener(this);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;


        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.tvCapMatName1), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);

        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.TOP;
        sbView.setLayoutParams(params);
        snackbar.show();

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    @Override
    public void onIconClicked(int position) {

    }

    @Override
    public void onIconImportantClicked(int position) {

    }

    @Override
    public void onMessageRowClicked(int position) {



    }

    @Override
    public void onRowLongClicked(int position) {

    }

    @Override
    public void onVolRequestFiled(int position, String param, String param2) {

        ListLokasiEnt message = listItems.get(position);
        listItems.set(position, message);

        ListLokasiEnt listLokasiEnt = new ListLokasiEnt(
                        listItems.get(position).getId().toString(), listItems.get(position).getKodeTower().toString(),
                        listItems.get(position).getNama_Zona().toString(), listItems.get(position).getKodeLokasi().toString(),
                        listItems.get(position).getNama_Lokasi().toString(),listItems.get(position).getKodeArea().toString(),
                        listItems.get(position).getDeskripsi().toString(),listItems.get(position).getKodePekerjaan().toString(),
                        listItems.get(position).getNamaPekerjaan().toString(),listItems.get(position).getThsprid().toString(),
                        listItems.get(position).getKodeItem().toString(), param,
                        param2,listItems.get(position).getTypeItem().toString(),
                "1"
        );

        listItems.set((position),listLokasiEnt);

        Log.d("LogVolReqSuccess", param);
    }

    @Override
    public void onVolRequestCanceled(int position, String param) {

        ListLokasiEnt message = listItems.get(position);
        listItems.set(position, message);

        ListLokasiEnt listLokasiEnt = new ListLokasiEnt(
                listItems.get(position).getId().toString(), listItems.get(position).getKodeTower().toString(),
                listItems.get(position).getNama_Zona().toString(), listItems.get(position).getKodeLokasi().toString(),
                listItems.get(position).getNama_Lokasi().toString(),listItems.get(position).getKodeArea().toString(),
                listItems.get(position).getDeskripsi().toString(),listItems.get(position).getKodePekerjaan().toString(),
                listItems.get(position).getNamaPekerjaan().toString(),listItems.get(position).getThsprid().toString(),
                listItems.get(position).getKodeItem().toString(), listItems.get(position).getVolRequest().toString(),
                listItems.get(position).getVolIdle().toString(),listItems.get(position).getTypeItem().toString(),
                "0"
        );

        listItems.set((position),listLokasiEnt);

        Log.d("LogVolReqCancel", param);
    }
}
