package com.totalbp.mmrequest.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.totalbp.mmrequest.R;
import com.totalbp.mmrequest.config.AppConfig;
import com.totalbp.mmrequest.config.SessionManager;
import com.totalbp.mmrequest.controller.MMController;
import com.totalbp.mmrequest.interfaces.VolleyCallback;
import com.totalbp.mmrequest.model.ApprovalEnt;
import com.totalbp.mmrequest.model.CommonAreaFormEnt;
import com.totalbp.mmrequest.model.CommonEnt;
import com.totalbp.mmrequest.model.CommonFloorFormEnt;
import com.totalbp.mmrequest.model.CommonMaterialFormEnt;
import com.totalbp.mmrequest.model.CommonRequestMoFormEnt;
import com.totalbp.mmrequest.model.CommonTowerFormEnt;
import com.totalbp.mmrequest.model.CommonWorkFormEnt;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import static android.R.attr.bitmap;
import static com.totalbp.mmrequest.config.AppConfig.URL_IMAGE_PREFIX;
import static com.totalbp.mmrequest.utils.ImageDecoder.decodeSampledBitmapFromFile;
import static com.totalbp.mmrequest.utils.ImageDecoder.getStringImage;

import com.totalbp.mmrequest.receiver.ConnectivityReceiver;
import com.totalbp.mmrequest.utils.MProgressDialog;

/**
 * Created by Ezra.R on 09/10/2017.
 */

public class RequestMaterialOutActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    private String fromrequestmaterial;

    private SearchableSpinner spinnerSpr, spinnerTower, spinnerFloor, spinnerArea, spinnerWork, spinnerMaterial;
    private FloatingActionButton floatActButton;
    private Button btnSave;
    private String spr = "", tower = "",tower2 = "", floor = "", floortype = "", floorfull = "", floorfull2 = "", area = "", area2 = "", work = "",work2 = "", material = "", sprqty = "", thsprid = "", today = "", unit = "", tdsprid = "", typeitem = "";
    private String savePath;
    private int spnvol, tmovol, ttransvol;
    private String latestvol, idlveol;
    private ArrayList<CommonEnt> ddlArrayListTower = new ArrayList<>();
    private ArrayList<CommonTowerFormEnt> ddlArrayListTower2 = new ArrayList<>();
    private ArrayList<CommonFloorFormEnt> ddlArrayListFloor = new ArrayList<>();
    private ArrayList<CommonAreaFormEnt> ddlArrayListArea = new ArrayList<>();
    private ArrayList<CommonWorkFormEnt> ddlArrayListWork = new ArrayList<>();
    private ArrayList<CommonMaterialFormEnt> ddlArrayListMaterial = new ArrayList<>();

    private ArrayList<CommonTowerFormEnt> ListTowerTemp = new ArrayList<>();
    private ArrayList<CommonFloorFormEnt> ListFloorTemp = new ArrayList<>();
    private ArrayList<CommonAreaFormEnt> ListAreaTemp = new ArrayList<>();
    private ArrayList<CommonWorkFormEnt> ListWorkTemp = new ArrayList<>();

    private SessionManager session;
    MMController controller;
    public ProgressDialog pDialog;
    private EditText etVolumeRequest, etTower,etFloor,etArea,etWork;
    private TextView tvUnggahFoto, tvRequestBy, tvReadyStock, tvIdleStock;
    JSONObject item;
    private ImageView imageHolder, ivItem, ivLocation, ivWork, ivMaterial;
    private final int requestCode = 30;
    private int year, month, day;
    Uri photoURI;
    public String mCurrentPhotoPath;
    static final int REQUEST_TAKE_PHOTO = 1;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private Dialog dialog;


    //Calendar myCalendar;
    Calendar myCalendar = new GregorianCalendar();
    public String id_daily_log_temp;

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);

    private static final int request_data_from  = 20;
    private static DecimalFormat df2 = new DecimalFormat(".##");


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requestmaterialoutform);

        session = new SessionManager(getApplicationContext());

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if(bd != null)
        {
            fromrequestmaterial = (String) bd.get("fromrequestmaterial");
            Log.d("fromrequestmaterial", fromrequestmaterial);
        }

        //JIKA TIDAK ADA AKSES
        if(!session.getCanInsert().toString().equals("1"))
        {
            Toast.makeText(getApplicationContext(),"You dont have Privilege to Insert!", Toast.LENGTH_SHORT).show();
            this.finish();
        }

        today = dateFormat.format(myCalendar.getTime());

        year = myCalendar.get(Calendar.YEAR);
        month = myCalendar.get(Calendar.MONTH);
        day = myCalendar.get(Calendar.DAY_OF_MONTH);

        spinnerTower = (SearchableSpinner) findViewById(R.id.spinnerTower);
        spinnerFloor = (SearchableSpinner) findViewById(R.id.spinnerFloor);
        spinnerArea = (SearchableSpinner) findViewById(R.id.spinnerArea);
        spinnerWork= (SearchableSpinner) findViewById(R.id.spinnerWork);
        spinnerMaterial = (SearchableSpinner) findViewById(R.id.spinnerMaterial);
        tvRequestBy = (TextView) findViewById(R.id.tvRequestBy);
        tvReadyStock = (TextView) findViewById(R.id.tvReadyStock);
        tvIdleStock = (TextView) findViewById(R.id.tvIdleStock);
        //etSprQty = (EditText) findViewById(R.id.etSprQty);
        etVolumeRequest = (EditText) findViewById(R.id.etVolumeRequest);

//        etTower= (EditText) findViewById(R.id.etTower);
//        etFloor= (EditText) findViewById(R.id.etFloor);
//        etArea= (EditText) findViewById(R.id.etArea);
//        etWork= (EditText) findViewById(R.id.etWork);

        tvUnggahFoto = (TextView) findViewById(R.id.tvUnggahFoto);
        imageHolder = (ImageView)findViewById(R.id.ivItem);
//        ivLocation= (ImageView) findViewById(R.id.ivLocation);
//        ivWork= (ImageView) findViewById(R.id.ivWork);
//        ivMaterial = (ImageView) findViewById(R.id.ivMaterial);

        floatActButton = (FloatingActionButton) findViewById(R.id.action_header_search_btn);
        btnSave=(Button) findViewById(R.id.btnSave);

        getSupportActionBar().setTitle("Request Material Out");
        session = new SessionManager(getApplicationContext());

        controller = new MMController();
        pDialog = new ProgressDialog(this);

//        ivWork.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_gray));
//        ivMaterial.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_gray));

        tvRequestBy.setText(session.getUserName());
        //tvRequestBy.setEnabled(false);
        //tvReadyStock.setEnabled(false);
        //tvIdleStock.setEnabled(false);


        dialog = new Dialog(RequestMaterialOutActivity.this);

        initControls();

        tvUnggahFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent photoCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //startActivityForResult(photoCaptureIntent, requestCode);

                dispatchTakePictureIntent();
            }
        });

        imageHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent photoCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //startActivityForResult(photoCaptureIntent, requestCode);

                dispatchTakePictureIntent();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Jika edittext tidak kosong
                String et_ReadyStock = tvReadyStock.getText().toString().trim();
                String et_VolumeRequest = etVolumeRequest.getText().toString().trim();
                String et_IdleStock = tvIdleStock.getText().toString().trim();


                if(!material.equals(""))
                {
                    if (et_ReadyStock.length() != 0) {
                        if (et_IdleStock.length() != 0) {
                            if (et_VolumeRequest.length() != 0) {
                                if(et_VolumeRequest.matches("\\d+(?:\\.\\d+)?") && !et_VolumeRequest.equals("0.0") && !et_VolumeRequest.equals("0"))
                                {

                                    String where = "";
                                    Log.d("LogMoFloor",floorfull2);
                                    Log.d("LogMoArea",area2);

                                    //Jagaan saat parameter kode area kosong berarti wherecond diganti untuk menampilkan item migrasi
                                    if(typeitem.equals("Migrasi-SPR") || typeitem.equals("Migrasi-Kontrak") || typeitem.equals("Migrasi-OtherKontrak") || typeitem.equals("Migrasi-AddKontrak") || typeitem.equals("Migrasi-NPO") || typeitem.equals("Prelim"))
                                    {
                                        where = " WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' ";
                                    }
                                    else
                                    {
                                        where = " WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' AND Kode_Lokasi = '"+floorfull2+"' AND Kode_Area = '"+area2+"' ";
                                    }

                                session = new SessionManager(getApplicationContext());
                                controller.InqGeneral3(getApplicationContext(),"SPM_GetQtyReadyStockFromViewParam",
                                        "@KodeProject",session.getKodeProyek(),
                                        "@currentpage","1",
                                        "@pagesize","10",
                                        "@sortby","",
                                        "@wherecond",where,
                                        //"@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' AND Kode_Tower = '"+tower+"' AND Kode_Lokasi = '"+floortype+floor+"' AND Kode_Area = '"+area+"' ",
                                        //"@nik",String.valueOf(session.isLoggedIn()),
                                        //"@formid","MAMAN.02",
                                        //"@zonaid","",
                                        new VolleyCallback() {
                                            @Override
                                            public void onSave(String output) {

                                            }

                                            @Override
                                            public void onSuccess(JSONArray result) {
                                                try {
                                                    Log.d("EZRAgtvReadyStockQty", result.toString());
                                                    if (result.length() > 0)
                                                    {
                                                        for (int i = 0; i < result.length(); i++) {
                                                            item = result.getJSONObject(i);
                                                            latestvol = item.getString("LatestVol2");
                                                        }


                                                        //String ReadyStockNow =  latestvol;
                                                        float volumerequest = Float.parseFloat(etVolumeRequest.getText().toString());
                                                        float volumereadystock = Float.parseFloat(tvReadyStock.getText().toString());
                                                        float volumeidlestock = Float.parseFloat(tvIdleStock.getText().toString());

                                                        float readystocknow = Float.parseFloat(latestvol);
                                                        Log.d("LogVolumRequest", String.valueOf(readystocknow));
                        //                              if (volumerequest <= volumereadystock) {
                                                        if (volumerequest <= readystocknow) {
                                                            String input = String.format(Locale.ENGLISH,"%.4f", volumerequest);
                                                            Log.d("LogVolumeInput", String.valueOf(input));
                                                            saveRequestMO(input, "0");
//                                                            saveRequestMO(etVolumeRequest.getText().toString(), "0");
                                                        } else {

                                                            if (volumerequest <= (readystocknow + volumeidlestock)) {
                                                                float qtyfinal1 = volumerequest - readystocknow;

                                                                String inputreadystock = String.format(Locale.ENGLISH,"%.4f", volumereadystock);
                                                                saveRequestMO(inputreadystock, String.valueOf(qtyfinal1));
//                                                                saveRequestMO(tvReadyStock.getText().toString(), String.valueOf(qtyfinal1));
                                                            } else {
                                                                Toast toast = Toast.makeText(RequestMaterialOutActivity.this, "Volume Request must less or equal than Ready Stock ("+readystocknow+"), or (Ready Stock + Idle Stock)!", Toast.LENGTH_LONG);
                                                                toast.setGravity(Gravity.CENTER, 0, 0);
                                                                toast.show();

                                                                etVolumeRequest.setText("");
                                                                etVolumeRequest.requestFocus();
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        latestvol = "0";
                                                        Toast.makeText(RequestMaterialOutActivity.this, "Check Qty Ready Stcok Latest Fail!", Toast.LENGTH_LONG).show();
                                                    }
                                                }catch (JSONException e){
                                                    Toast.makeText(getApplicationContext(), "CATCH gtvReadyStockQty", Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });
                                }
                                else
                                {
                                    Toast toast = Toast.makeText(RequestMaterialOutActivity.this, "Volume Request must Number, Not Empty and > 0!", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();

                                    etVolumeRequest.requestFocus();
                                }
                            } else {
                                Toast toast = Toast.makeText(RequestMaterialOutActivity.this, "Volume Request required!", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();

                                etVolumeRequest.requestFocus();
                            }
                        } else {
                            Toast toast = Toast.makeText(RequestMaterialOutActivity.this, "Idle Stock required!", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    } else {

                        Toast toast = Toast.makeText(RequestMaterialOutActivity.this, "Ready Stock required!", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
                else
                {
                    Toast toast = Toast.makeText(RequestMaterialOutActivity.this, "Select Material Required!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

            }
        });


    }


    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //MainActivity activity = (MainActivity)getActivity();

        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
//            photoFile = getOutputMediaFile(); //untuk sementara
//            try {
//                photoFile = createImageFile();
//
//            } catch (IOException ex) {
//                Toast toast = Toast.makeText(activity, "There was a problem saving the photo...", Toast.LENGTH_SHORT);
//                toast.show();
//            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                mCurrentPhotoPath = photoFile.getAbsolutePath();
//                Log.d("path",mCurrentPhotoPath);
//                photoURI = Uri.fromFile(photoFile);

                photoURI = FileProvider.getUriForFile(getApplicationContext(), "com.totalbp.mmrequest.fileprovider", photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,photoURI);
//                takePictureIntent.putExtra("id_daily_log",id_daily_log);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private  File createImageFile() throws IOException { //bener2 simpen filenya di storage directory
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()){
            if (!storageDir.mkdirs()){
                return null;
            }
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        return image;
    }

    private void displayDialog(String id_daily_log) {
        UploadPicture(id_daily_log);
        //materialDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            Toast.makeText(getBaseContext(), "Photo Taken",Toast.LENGTH_SHORT).show();

            displayDialog(id_daily_log_temp);

            final Bitmap selectedImage = decodeSampledBitmapFromFile(mCurrentPhotoPath,500, 250);
            String encodedImage = getStringImage(selectedImage);

            byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            //Start
            //Code By Ezra
            //bitmapnya dideclare ulang terlebih dahulu untuk dibenarkan rotatenya kemudian baru diset ke imageview
            ExifInterface ei = null;
            try {
                ei = new ExifInterface(mCurrentPhotoPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            Bitmap rotatedBitmap = null;
            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(decodedByte, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(decodedByte, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(decodedByte, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = decodedByte;
            }
            //Close
            imageHolder.setImageBitmap(rotatedBitmap);

        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private void UploadPicture(String id_daily_log){
        final Bitmap selectedImage = decodeSampledBitmapFromFile(mCurrentPhotoPath,500, 500);
        String filename="DP" +"_"+ mCurrentPhotoPath.substring(mCurrentPhotoPath.lastIndexOf("/")+1);
        String encodedImage = getStringImage(selectedImage);

        //Start
        //Code by Ezra
        //Decode terlebih dahulu untuk dibenarkan rotatenya kemudian baru di upload
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        ExifInterface ei = null;
        try {
            ei = new ExifInterface(mCurrentPhotoPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap rotatedBitmap = null;
        switch(orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(decodedByte, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(decodedByte, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(decodedByte, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = decodedByte;
        }

        String encodedImage2 = getStringImage(rotatedBitmap);
        //end
        UploadFileToServer(encodedImage2,filename, id_daily_log);

    }

    public void UploadFileToServer(String base64encode, String filename, final String id_hse_assessment){

        MProgressDialog.showProgressDialog(RequestMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        RequestQueue requestQueue;
        session = new SessionManager(getApplicationContext());
        JSONObject request = new JSONObject();

        try {
            request.put("Pengubah", session.getUserDetails().get("nik"));
            request.put("FileName", filename);
            request.put("TokenID", session.getUserToken());
            request.put("FormID", "AR.03.01");
            request.put("KodeProyek",session.getKodeProyek()); //Dummy
            request.put("Base64String",base64encode);
        } catch (Exception e) {
            e.printStackTrace();
        }

        requestQueue = Volley.newRequestQueue(getApplicationContext());
//        http://10.100.223.51:20173/API/TQA/Upload
        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, session.getUrlConfig()+AppConfig.URL_UPLOAD_GAMBAR,request,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("LogUploadPicPenerimaGbr",response.toString());
                            if(response.getString("IsSucceed").equals("true")){
                               // Toast.makeText(getApplicationContext(), response.getString("Message"), Toast.LENGTH_LONG).show();
//                                String savePath = response.getString("VarOutput");
                                String pathDatabase = response.getString("VarOutput"); //hashcode
                                String yearstring = String.valueOf(year);
                                String monthstring = "0"+String.valueOf(month+1);
                                savePath = session.getUrlConfig()+URL_IMAGE_PREFIX + "/GeneralFile/"+yearstring.substring(yearstring.length()-2)+"/"+monthstring.substring(monthstring.length()-2)+"/" + response.getString("Message") + ".jpg";

                                Log.d("savePath",savePath);
                                MProgressDialog.dismissProgressDialog();

                            }
                            else{
                                Toast.makeText(getApplicationContext(), response.getString("Message "+"Please Re-Login"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Error Response " + error.toString(), Toast.LENGTH_LONG).show();
                    }
                });
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(strReq);
    }

    private void saveRequestMO(String qty, String qty2) {

        MProgressDialog.showProgressDialog(RequestMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        if(tdsprid.equals("null"))
        {
            tdsprid = "0";
        }
        Gson gsonHeader = new Gson();
        CommonRequestMoFormEnt commonRequestMoFormEnt = new CommonRequestMoFormEnt();
        commonRequestMoFormEnt.setID("0");
        commonRequestMoFormEnt.setKode_MO(session.getKodeProyek());
        commonRequestMoFormEnt.setKode_Proyek(session.getKodeProyek());
        commonRequestMoFormEnt.setKode_Item(material);
        commonRequestMoFormEnt.setVolume(qty);
        commonRequestMoFormEnt.setUnit(unit);
        commonRequestMoFormEnt.setWork_Code(work2);
        commonRequestMoFormEnt.setKode_Tower(tower2);
        commonRequestMoFormEnt.setKode_Lantai(floorfull2);
        commonRequestMoFormEnt.setKode_Zona(area2);
        commonRequestMoFormEnt.setCourierPhoto(savePath);
        commonRequestMoFormEnt.setRefHeaderSPR(thsprid);
        commonRequestMoFormEnt.setRefDetailSPR(tdsprid);
        commonRequestMoFormEnt.setStatusRequest("1");
        commonRequestMoFormEnt.setApprovalNo("");
        commonRequestMoFormEnt.setStatusAktif("True");
        commonRequestMoFormEnt.setPembuat(session.getUserName());
        commonRequestMoFormEnt.setNama_Pembuat(session.getKeyNik());
        commonRequestMoFormEnt.setWaktuBuat(today);
        commonRequestMoFormEnt.setPengubah(session.getKeyNik());
        commonRequestMoFormEnt.setNama_Pengubah(session.getKeyNik());
        commonRequestMoFormEnt.setWaktuUbah(today);
        commonRequestMoFormEnt.setComment("");
        commonRequestMoFormEnt.setTokenID(session.getUserToken());
        commonRequestMoFormEnt.setVolumeIdle(qty2);
        //commonRequestMoFormEnt.setModeReq("POSTING");
        commonRequestMoFormEnt.setFormId("MAMAN.02");
        String jsonString = gsonHeader.toJson(commonRequestMoFormEnt);

        controller.SaveGeneralObjectRequestMo(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                    MProgressDialog.dismissProgressDialog();
                    try {

                        JSONObject obj = new JSONObject(output);

                        if(!obj.getString("IsSucceed").equals("false")) {

                            String output2 = obj.getString("VarOutput");
                            String[] split = output2.split("#");
                            String firstString = split[0];
                            String firstSecondstring = split[1];

                            isCanPosting(firstString, firstSecondstring);
                        }
                        else
                        {
                            Toast toast = Toast.makeText(RequestMaterialOutActivity.this,"Save Failed, "+obj.getString("Message")+" ! ", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                            Intent intent = new Intent();
                            intent.putExtra("", "");
                            setResult(request_data_from, intent);
                            finish();
                        }

                    } catch (Throwable t) {
                        Log.e("My App", "Could not parse malformed JSON: \"" + output + "\"");
                    }
//                }
//                else
//                {
//                    Toast toast = Toast.makeText(RequestMaterialOutActivity.this,"Save Failed!", Toast.LENGTH_LONG);
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();
//
//                    Intent intent = new Intent();
//                    intent.putExtra("", "");
//                    setResult(request_data_from, intent);
//                    finish();
//                }
            }
        });

    }


    private void isCanPosting(String kodematerialout, String IdMo) {

        MProgressDialog.showProgressDialog(RequestMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("is_can_posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
//        approvalEnt.setApproval_Name("Approval Material Out Management Mobile");
        approvalEnt.setTokenID(session.getUserToken());

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

                if(jsonObject.get("IsCanPosting").getAsString().equals("true"))
                {
                    dialog = new Dialog(RequestMaterialOutActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.general_dialog);
                    dialog.setCanceledOnTouchOutside(false);

                    TextView dTittle = (TextView) dialog.findViewById(R.id.dTittle);
                    TextView dContent = (TextView) dialog.findViewById(R.id.dContent);
                    Button dBtnCancel = (Button) dialog.findViewById(R.id.dBtnCancel);
                    Button dBtnSubmit = (Button) dialog.findViewById(R.id.dBtnSubmit);

                    dTittle.setText("Material Out");
                    dContent.setText("Do you want Post Mo number "+kodematerialout+" ?");

                    dBtnSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onPosting(kodematerialout, IdMo);
                        }
                    });


                    dBtnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            Intent intent = new Intent();
                            intent.putExtra("", "");
                            setResult(request_data_from, intent);
                            finish();
                        }
                    });

                    dialog.show();


                }
                else
                {
                    Toast toast = Toast.makeText(RequestMaterialOutActivity.this,"Request Material Save Success!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    Intent intent = new Intent();
                    intent.putExtra("", "");
                    setResult(request_data_from, intent);
                    finish();
                }

            }
        });
    }

    private void onPosting(String kodematerialout, String IdMo) {

        MProgressDialog.showProgressDialog(RequestMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        //Log.d("OmOm", kodematerialout+"|"+IdMo);
        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
        approvalEnt.setTokenID(session.getUserToken());

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {


                Log.d("OmOm2", output);
                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

//                if(jsonObject.get("IsCanApprove").getAsString().equals("true"))
//                {
//                    Log.d("OmOm3", output);
                    updateRequestMO(jsonObject.get("Approval_Number").getAsString(), IdMo, kodematerialout);
//                }
//                else
//                {
//                    Toast toast = Toast.makeText(RequestMaterialOutActivity.this,"You already posting !", Toast.LENGTH_LONG);
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();
//
//                    Intent intent = new Intent();
//                    intent.putExtra("", "");
//                    setResult(request_data_from, intent);
//                    finish();
//                }

            }
        });
    }

    private void IsCanApprove(String kodematerialout, String ApprovalNumber, String IdMo) {

        MProgressDialog.showProgressDialog(RequestMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        //Log.d("OmOm", kodematerialout+"|"+IdMo);
        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
        approvalEnt.setTokenID(session.getUserToken());

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

//                Log.d("OmOm2", output);
//                MProgressDialog.dismissProgressDialog();
//                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
//                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

                //updateRequestMO(jsonObject.get("Approval_Number").getAsString(), IdMo);
            }
        });
    }

    private void updateRequestMO(String ApprovalNumber, String IdMo, String KodeMaterialOut) {
        Log.d("OmOm4", ApprovalNumber);
        MProgressDialog.showProgressDialog(RequestMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();

        CommonRequestMoFormEnt commonRequestMoFormEnt = new CommonRequestMoFormEnt();
        commonRequestMoFormEnt.setID(IdMo);
        commonRequestMoFormEnt.setKode_MO(KodeMaterialOut);
        commonRequestMoFormEnt.setKode_Proyek(session.getKodeProyek());
        commonRequestMoFormEnt.setKode_Item("");
        commonRequestMoFormEnt.setVolume("1");
        commonRequestMoFormEnt.setUnit("");
        commonRequestMoFormEnt.setWork_Code("");
        commonRequestMoFormEnt.setKode_Tower("");
        commonRequestMoFormEnt.setKode_Lantai("");
        commonRequestMoFormEnt.setKode_Zona("0");
        commonRequestMoFormEnt.setCourierPhoto("");
        commonRequestMoFormEnt.setRefHeaderSPR("0");
        commonRequestMoFormEnt.setRefDetailSPR("0");
        commonRequestMoFormEnt.setStatusRequest("1");
        commonRequestMoFormEnt.setApprovalNo(ApprovalNumber);
        commonRequestMoFormEnt.setStatusAktif("True");
        commonRequestMoFormEnt.setPembuat(session.getUserName());
        commonRequestMoFormEnt.setNama_Pembuat(session.getUserName());
        commonRequestMoFormEnt.setWaktuBuat(today);
        commonRequestMoFormEnt.setPengubah(session.getKeyNik());
        commonRequestMoFormEnt.setNama_Pengubah(session.getUserName());
        commonRequestMoFormEnt.setWaktuUbah(today);
        commonRequestMoFormEnt.setComment("");
        commonRequestMoFormEnt.setTokenID(session.getUserToken());
        //commonRequestMoFormEnt.setModeReq("POSTING");
        commonRequestMoFormEnt.setFormId("MAMAN.02");
//
        String jsonString = gsonHeader.toJson(commonRequestMoFormEnt);
        Log.d("OmOm5", ApprovalNumber);
        controller.SaveGeneralObjectRequestMo(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                Toast toast = Toast.makeText(RequestMaterialOutActivity.this,KodeMaterialOut+" Posting Success !", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                Intent intent = new Intent();
                intent.putExtra("", "");
                setResult(request_data_from, intent);
                finish();

            }
        });
    }

    public void initControls(){


        //getDataForDDL(spinnerSpr,"SPM_GetThSprMobile_Param","ID","Kode_Spr",ddlArrayListTower,"@KodeProject",session.getKodeProyek(),"@currentpage","1","@pagesize","10000","@sortby","","@wherecond"," AND Kode_Proyek ='"+session.getKodeProyek()+"' ");
        //spinnerSpr.setTitle("Select SPR");
        //spinnerSpr.setPositiveButton("OK");
        //spinnerSpr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                final CommonEnt commonEnt = (CommonEnt) parent.getSelectedItem();
//                spr = commonEnt.getValue();
//                Log.d("sprChoose", spr);

//                if(!spr.equals("-")) {
                    getDataForDDL2(spinnerTower, "SPM_GetTower_Param", "Kode_Tower", "Nama_Tower", ddlArrayListTower2, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "100", "@sortby", "", "@wherecond", " AND Kode_Proyek ='" + session.getKodeProyek() + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    getDataForDDL6(spinnerMaterial, "SPT_SPR_Detail_Inq_MM_Param", "ID_M_Katalog", "Spec", ddlArrayListMaterial, "@TotalRecords", "", "@currentpage", "1", "@pagesize", "3000", "@sortby", "", "@wherecond", " WHERE b.Kode_Proyek ='" + session.getKodeProyek() + "' AND f.UserCode ='" + work + "' AND convert(varchar, M_Area.ID) ='" + area + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");

        //
                    //gtvReadyStockQty(spr);
                //}
            //}
            //@Override
            //public void onNothingSelected(AdapterView<?> parent) {
            //}
        //});

        spinnerTower.setTitle("Select Tower");
        spinnerTower.setPositiveButton("OK");
        spinnerTower.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonTowerFormEnt commonEnt = (CommonTowerFormEnt) parent.getSelectedItem();
                tower = commonEnt.getValue();
                if(!tower.equals("-")) {
                    getDataForDDL3(spinnerFloor, "SPM_GetFloor_Param", "Kode_Lantai", "Nama_Lantai", ddlArrayListFloor, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", " Nama_Lantai ASC ", "@wherecond", " AND M_Zona.Kode_Proyek ='" + session.getKodeProyek() + "' AND M_Zona.Kode_Zona ='" + tower + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    Log.d("towerChoose", tower);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerFloor.setTitle("Select Floor");
        spinnerFloor.setPositiveButton("OK");
        spinnerFloor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonFloorFormEnt commonEnt = (CommonFloorFormEnt) parent.getSelectedItem();
                floor = commonEnt.getValue();
                floortype = commonEnt.getValue2();
                floorfull = floortype+floor;
                //work = commonEnt.getValue();
                if(!floor.equals("-")) {
                    getDataForDDL4(spinnerArea, "SPM_GetArea_Param", "Kode_Lokasi", "Nama_Area", ddlArrayListArea, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", "", "@wherecond", " AND M_Zona.Kode_Proyek ='" + session.getKodeProyek() + "' AND M_Zona.Kode_Zona ='" + tower + "' AND M_Area.Kode_Lokasi ='" + floorfull + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    //getDataForDDL5(spinnerWork, "SPM_GetWorkBasedSprAndTowerAndFloorMobile_Param", "Kode_Work", "Nama_Work", ddlArrayListWork, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", "", "@wherecond", " AND a.Kode_Proyek ='" + session.getKodeProyek() + "' AND d.Kode_Zona ='" + tower + "' AND c.Kode_Lokasi ='" + floor + "' ");
                    Log.d("floorChoose", floor);
                   // Log.d("workChoose", work);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerArea.setTitle("Select Area");
        spinnerArea.setPositiveButton("OK");
        spinnerArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonAreaFormEnt commonEnt = (CommonAreaFormEnt) parent.getSelectedItem();
                area = commonEnt.getValue();
                if(!area.equals("-")) {
                    getDataForDDL5(spinnerWork, "SPM_GetWork_Param", "Kode_Pekerjaan", "Nama_Pekerjaan", ddlArrayListWork, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", "", "@wherecond", " WHERE [M_Area].Kode_Proyek = '"+session.getKodeProyek()+"' AND m_zona.Kode_Zona = '"+tower+"' AND m_lokasi.Tipe_Lokasi+m_lokasi.Kode_Lokasi = '"+floorfull+"' AND [M_Area].ID = '"+area+"' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    Log.d("areaChoose", area);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerWork.setTitle("Select Work");
        spinnerWork.setPositiveButton("OK");
        spinnerWork.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonWorkFormEnt commonEnt = (CommonWorkFormEnt) parent.getSelectedItem();
                work = commonEnt.getValue();
                if(!work.equals("-")) {
                    //getDataForDDL6(spinnerMaterial, "SPT_SPR_Detail_Inq_MM_Param", "ID_M_Katalog", "Spec", ddlArrayListMaterial, "@TotalRecords", "", "@currentpage", "1", "@pagesize", "1000", "@sortby", "", "@wherecond", " AND b.Kode_Proyek ='" + session.getKodeProyek() + "' AND f.UserCode ='"+work+"' ", "@nik", String.valueOf(session.isLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    getDataForDDL6(spinnerMaterial, "SPT_SPR_Detail_Inq_MM_Param", "ID_M_Katalog", "Spec", ddlArrayListMaterial, "@TotalRecords", "", "@currentpage", "1", "@pagesize", "2000", "@sortby", "", "@wherecond", " WHERE b.Kode_Proyek ='" + session.getKodeProyek() + "' AND f.UserCode ='" + work + "' AND convert(varchar, M_Area.ID) ='" + area + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");

                    Log.d("workChoose", work);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerMaterial.setTitle("Select Material");
        spinnerMaterial.setPositiveButton("OK");
        spinnerMaterial.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonMaterialFormEnt commonEnt = (CommonMaterialFormEnt) parent.getSelectedItem();
                material = commonEnt.getValue();
                thsprid = commonEnt.getValue4();
                unit = commonEnt.getValue3();
                tdsprid = commonEnt.getValue6();
                typeitem = commonEnt.getValue7();



                if(!material.equals("-")) {

                    Log.d("materialChoose", material);
                    Log.d("materialChoose", thsprid);
                    Log.d("materialChoose", unit);
                    Log.d("LogMo1",typeitem);
                    Log.d("LogMo1Floor",floorfull);
                    Log.d("LogMo1Area",area);
                    gtvIdleStockQty(material);
                    if(!tdsprid.equals("null") || !tdsprid.equals(""))
                    {

                       if(typeitem.equals("Prelim"))
                       {
                           getTowerLantaiZona(spinnerTower, "SPM_GetTowerLantaiAreaTempParam", "", "", ListTowerTemp, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "100", "@sortby", "", "@wherecond", " WHERE tdspr.ID ='" + tdsprid + "' "+typeitem, "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", ListFloorTemp, ListAreaTemp, ListWorkTemp, spinnerFloor, spinnerArea, spinnerWork, material, typeitem);
                       }
                       else
                       {
                           getTowerLantaiZona(spinnerTower, "SPM_GetTowerLantaiAreaTempParam", "", "", ListTowerTemp, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "100", "@sortby", "", "@wherecond", " WHERE tdspr.ID ='" + tdsprid + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", ListFloorTemp, ListAreaTemp,ListWorkTemp, spinnerFloor, spinnerArea, spinnerWork, material, typeitem);
                       }
                    }
                    else
                    {
                        gtvReadyStockQty(material, typeitem);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //getDataForDDL3(spinnerZone,"SPM_GetArea_Param","Kode_Area","Nama_Area",ddlArrayListTower2,"@KodeProject",session.getKodeProyek(),"@currentpage","1","@pagesize","100","@sortby","","@wherecond"," AND Kode_Proyek ='"+session.getKodeProyek()+"' GROUP BY ID, Deskripsi");
        //getDataForDDL4(spinnerMaterial,"SPM_GetMaterial_Param","Kode_Item","Nama_Item",ddlArrayListTower3,"@KodeProject",session.getKodeProyek(),"@currentpage","1","@pagesize","200","@sortby","","@wherecond"," GROUP BY ID, Deskripsi");
    }

    public void showDialog() {
        pDialog.setMessage("Loading ...");
        if (!pDialog.isShowing())
            pDialog.show();
    }

    public void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void getDataForDDL(final SearchableSpinner spinner, final String uniqueSPName,
                              final String paramName, final String paramVal,
                              final ArrayList<CommonEnt> ddlArrayList,
                              final String paramName1, final String paramVal1,
                              final String paramName2, final String paramVal2,
                              final String paramName3, final String paramVal3,
                              final String paramName4, final String paramVal4,
                              final String paramName5, final String paramVal5,
                              final String paramName6, final String paramVal6,
                              final String paramName7, final String paramVal7,
                              final String paramName8, final String paramVal8){
        ddlArrayList.clear();

        MProgressDialog.showProgressDialog(RequestMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
            @Override
            public void onSuccess(JSONArray result) {
                try {
                    if (result.length() > 0) {

                        CommonEnt itemEntsinit = new CommonEnt(
                                "-","-",
                                "-","-",
                                "-","-",
                                "-","-",
                                "-","-"
                        );
                        ddlArrayList.add(itemEntsinit);

                        for (int i = 0; i < result.length(); i++) {
                            JSONObject item = result.getJSONObject(i);

                            CommonEnt itemEnts = new CommonEnt(item.getString(paramVal), item.getString(paramName),
                                    item.getString(paramName), item.getString(paramName),
                                    item.getString(paramName), item.getString(paramName),
                                    item.getString(paramName), item.getString(paramName),
                                    item.getString(paramName), item.getString(paramName)
                            );
                            ddlArrayList.add(itemEnts);
                        }
                        MProgressDialog.dismissProgressDialog();
                        ArrayAdapter<CommonEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                        spinner.setAdapter(adapterDDLGlobal);
                    }
                    else{
                        CommonEnt itemEnts = new CommonEnt(
                                "No Item Found","",
                                "No Item Found","",
                                "No Item Found","",
                                "No Item Found","",
                                "No Item Found",""
                        );
                        ddlArrayList.add(itemEnts);
                        ArrayAdapter<CommonEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                        spinner.setAdapter(adapterDDLGlobal);
                    }
                    MProgressDialog.dismissProgressDialog();
                }catch (JSONException e){
                    Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                }
            }


            @Override
            public void onSave(String output) {

            }
        });
    }

    public void getDataForDDL2(final SearchableSpinner spinner, final String uniqueSPName,
                              final String paramName, final String paramVal,
                              final ArrayList<CommonTowerFormEnt> ddlArrayList,
                              final String paramName1, final String paramVal1,
                              final String paramName2, final String paramVal2,
                              final String paramName3, final String paramVal3,
                              final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();

//        MProgressDialog.showProgressDialog(RequestMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                MProgressDialog.dismissProgressDialog();
//            }
//        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {

                                CommonTowerFormEnt itemEntsinit = new CommonTowerFormEnt(
                                        "-select tower-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );
                                ddlArrayList.add(itemEntsinit);

                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonTowerFormEnt itemEnts = new CommonTowerFormEnt(item.getString(paramVal), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                }
                               // MProgressDialog.dismissProgressDialog();
                                ArrayAdapter<CommonTowerFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonTowerFormEnt itemEnts = new CommonTowerFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonTowerFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            //MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void getTowerLantaiZona(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonTowerFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                                   final ArrayList<CommonFloorFormEnt> ddlArrayList2,
                                   final ArrayList<CommonAreaFormEnt> ddlArrayList3,
                                   final ArrayList<CommonWorkFormEnt> ddlArrayList4,
                                   final SearchableSpinner spinner2, final SearchableSpinner spinner3, final SearchableSpinner spinner4, String materialparam, String typeitemparam){
        ddlArrayList.clear();
        ddlArrayList2.clear();
        ddlArrayList3.clear();
        ddlArrayList4.clear();

//        MProgressDialog.showProgressDialog(RequestMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                MProgressDialog.dismissProgressDialog();
//            }
//        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {

                                for (int i = 0; i < result.length(); i++) {

                                    JSONObject item = result.getJSONObject(i);

                                    CommonTowerFormEnt itemEnts = new CommonTowerFormEnt(item.getString("Nama_Zona"), "-",
                                            "", "",
                                            "", "",
                                            "", "",
                                            "", ""
                                    );
                                    ddlArrayList.add(itemEnts);

                                    CommonFloorFormEnt itemEnts2 = new CommonFloorFormEnt(item.getString("Nama_Lokasi"), "-",
                                            "", "",
                                            "", "",
                                            "", "",
                                            "", ""
                                    );
                                    ddlArrayList2.add(itemEnts2);

                                    CommonAreaFormEnt itemEnts3 = new CommonAreaFormEnt(item.getString("Deskripsi"), "-",
                                            "", "",
                                            "", "",
                                            "", "",
                                            "", ""
                                    );
                                    ddlArrayList3.add(itemEnts3);

                                    CommonWorkFormEnt itemEnts4 = new CommonWorkFormEnt(item.getString("NamaPekerjaan"), "-",
                                            "", "",
                                            "", "",
                                            "", "",
                                            "", ""
                                    );
                                    ddlArrayList4.add(itemEnts4);


//                                    spinner.setVisibility(View.GONE);
//                                    spinner2.setVisibility(View.GONE);
//                                    spinner3.setVisibility(View.GONE);
//                                    spinner4.setVisibility(View.GONE);
//
//                                    etTower.setText(item.getString("Nama_Zona"));
//                                    etFloor.setText(item.getString("Nama_Lokasi"));
//                                    etArea.setText(item.getString("Deskripsi"));
//                                    etWork.setText(item.getString("NamaPekerjaan"));
//
//                                    etTower.setVisibility(View.VISIBLE);
//                                    etFloor.setVisibility(View.VISIBLE);
//                                    etArea.setVisibility(View.VISIBLE);
//                                    etWork.setVisibility(View.VISIBLE);

                                    floorfull = item.getString("KodeLokasi");
                                    area = item.getString("KodeArea");
                                    tower =item.getString("KodeTower");
                                    work = item.getString("KodePekerjaan");
                                    gtvReadyStockQty(materialparam, typeitemparam);
                                }
                                 MProgressDialog.dismissProgressDialog();
                                ArrayAdapter<CommonTowerFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);

                                ArrayAdapter<CommonFloorFormEnt> adapterDDLGlobal2 = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList2);
                                spinner2.setAdapter(adapterDDLGlobal2);

                                ArrayAdapter<CommonAreaFormEnt> adapterDDLGlobal3 = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList3);
                                spinner3.setAdapter(adapterDDLGlobal3);

                                ArrayAdapter<CommonWorkFormEnt> adapterDDLGlobal4 = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList4);
                                spinner4.setAdapter(adapterDDLGlobal4);
                            }
                            else{
                                CommonTowerFormEnt itemEnts = new CommonTowerFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonTowerFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);

                                CommonFloorFormEnt itemEnts2 = new CommonFloorFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList2.add(itemEnts2);
                                ArrayAdapter<CommonFloorFormEnt> adapterDDLGlobal2 = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList2);
                                spinner2.setAdapter(adapterDDLGlobal2);

                                CommonAreaFormEnt itemEnts3 = new CommonAreaFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList3.add(itemEnts3);
                                ArrayAdapter<CommonAreaFormEnt> adapterDDLGlobal3 = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList3);
                                spinner3.setAdapter(adapterDDLGlobal3);

                                CommonWorkFormEnt itemEnts4 = new CommonWorkFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList4.add(itemEnts4);
                                ArrayAdapter<CommonWorkFormEnt> adapterDDLGlobal4 = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList4);
                                spinner4.setAdapter(adapterDDLGlobal4);

                                gtvReadyStockQty(materialparam, typeitemparam);
                            }

                            //MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void gtvReadyStockQty(String material, String typeitem){
        Log.d("material", material);

        String where = "";
        floorfull2 = floorfull;
        area2 = area;
        work2 = work;
        tower2 = tower;

        //Jagaan saat parameter kode area kosong berarti wherecond diganti untuk menampilkan item migrasi
        if(typeitem.equals("Migrasi-SPR") || typeitem.equals("Migrasi-Kontrak") || typeitem.equals("Migrasi-OtherKontrak") || typeitem.equals("Migrasi-AddKontrak") || typeitem.equals("Migrasi-NPO") || typeitem.equals("Prelim"))
        {
            where = " WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' ";
        }
        else
        {
            where = " WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' AND Kode_Lokasi = '"+floorfull+"' AND Kode_Area = '"+area+"' ";
        }

        session = new SessionManager(getApplicationContext());
        controller.InqGeneral3(getApplicationContext(),"SPM_GetQtyReadyStockFromViewParam",
                "@KodeProject",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond",where,
//                "@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' AND Kode_Lokasi = '"+floortype+floor+"' AND Kode_Area = '"+area+"' ",
                //"@nik",String.valueOf(session.isLoggedIn()),
                //"@formid","MAMAN.02",
                //"@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("EZRAgtvReadyStockQty", result.toString());
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);
                                    latestvol = item.getString("LatestVol");
                                }
                                tvReadyStock.setText(latestvol);
                            }
                            else
                            {
                                tvReadyStock.setText("0");
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH gtvReadyStockQty", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public String gtvReadyStockQtyChecker(String material){
        Log.d("material", material);
        session = new SessionManager(getApplicationContext());
        controller.InqGeneral3(getApplicationContext(),"SPM_GetQtyReadyStockFromViewParam",
                "@KodeProject",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' AND Kode_Tower = '"+tower+"' AND Kode_Lokasi = '"+floorfull+"' AND Kode_Area = '"+area+"' ",
                //"@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' +"' AND Requester = '"+session.getUserName()+"' ",
                //"@nik",String.valueOf(session.isLoggedIn()),
                //"@formid","MAMAN.02",
                //"@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("EZRAgtvReadyStockQty", result.toString());
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);
                                    latestvol = item.getString("LatestVol2");
                                }
                            }
                            else
                            {
                                latestvol = "0";
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH gtvReadyStockQty", Toast.LENGTH_LONG).show();
                        }
                    }
                });

        return (latestvol);
    }

    public void gtvIdleStockQty(String material){
        Log.d("material", material);
        session = new SessionManager(getApplicationContext());
        controller.InqGeneral3(getApplicationContext(),"SPM_GetQtyIdleStockFromView_Param",
                "@KodeProject",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond"," WHERE ttrans.Kode_Proyek = '"+session.getKodeProyek()+"' AND ttrans.Kode_Item = '"+material+"' AND ttrans.FlagReturnGudang = 1 ",
                //"@wherecond"," WHERE ttrans.Kode_Proyek = '"+session.getKodeProyek()+"' AND ttrans.Kode_Item = '"+material+"' AND ttrans.ToKode_Tower = '"+tower+"' AND ttrans.FlagReturnGudang = 1 ",
                //"@nik",String.valueOf(session.isLoggedIn()),
                //"@formid","MAMAN.02",
                //"@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("EZRAgtvIdleStockQty", result.toString());
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);
                                    idlveol = item.getString("vol");
                                }
                                tvIdleStock.setText(idlveol);
                            }
                            else
                            {
                                tvIdleStock.setText("0");
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH IdleStockQty", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void getDataForDDL3(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonFloorFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();

        MProgressDialog.showProgressDialog(RequestMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {

                                CommonFloorFormEnt itemEntsinit = new CommonFloorFormEnt(
                                        "-select floor-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );
                                ddlArrayList.add(itemEntsinit);

                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonFloorFormEnt itemEnts = new CommonFloorFormEnt(item.getString(paramVal), item.getString(paramName),
                                            item.getString("Tipe_Lokasi"), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                }
                                MProgressDialog.dismissProgressDialog();
                                ArrayAdapter<CommonFloorFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonFloorFormEnt itemEnts = new CommonFloorFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonFloorFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void getDataForDDL4(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonAreaFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();

        MProgressDialog.showProgressDialog(RequestMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                CommonAreaFormEnt itemEntsinit = new CommonAreaFormEnt(
                                        "-select area-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );

                                ddlArrayList.add(itemEntsinit);
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonAreaFormEnt itemEnts = new CommonAreaFormEnt(item.getString(paramVal), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                }
                                MProgressDialog.dismissProgressDialog();
                                ArrayAdapter<CommonAreaFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonAreaFormEnt itemEnts = new CommonAreaFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonAreaFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void getDataForDDL5(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonWorkFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();

        MProgressDialog.showProgressDialog(RequestMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                CommonWorkFormEnt itemEntsinit = new CommonWorkFormEnt(
                                        "-select work-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );

                                ddlArrayList.add(itemEntsinit);
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonWorkFormEnt itemEnts = new CommonWorkFormEnt(item.getString(paramVal), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                }
                                MProgressDialog.dismissProgressDialog();
                                ArrayAdapter<CommonWorkFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonWorkFormEnt itemEnts = new CommonWorkFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonWorkFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void getDataForDDL6(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonMaterialFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();

        MProgressDialog.showProgressDialog(RequestMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            MProgressDialog.dismissProgressDialog();
                            Log.d("EZRAgetMaterial", result.toString());
                            if (result.length() > 0) {
                                CommonMaterialFormEnt itemEntsinit = new CommonMaterialFormEnt(
                                        "-select material-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );

                                ddlArrayList.add(itemEntsinit);
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonMaterialFormEnt itemEnts = new CommonMaterialFormEnt(item.getString(paramVal)+ " ("+item.getString("spesifikasi")+") - "+item.getString("Kode_SPR")+" | "+item.getString("TypeItem"), item.getString(paramName),
                                    //CommonMaterialFormEnt itemEnts = new CommonMaterialFormEnt(item.getString(paramVal)+ " ("+item.getString("spesifikasi")+") - "+item.getString("Kode_SPR")+"", item.getString(paramName),
                                            item.getString("Kd_Anak_Kd_Material"), item.getString("Unit"),
                                            item.getString("ID"), item.getString("Kode_SPR"),
                                            item.getString("Id_Td_SPR"), item.getString("TypeItem"),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                    //sprqty = item.getString("Volume");
                                    //etSprQty.setText(sprqty);
                                    //Log.d("sprqty", sprqty);
                                }
                               // MProgressDialog.dismissProgressDialog();
                                ArrayAdapter<CommonMaterialFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonMaterialFormEnt itemEnts = new CommonMaterialFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonMaterialFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                           // MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH getMaterial"+e.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MMController.getInstance().setConnectivityListener(this);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;


        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.tvIdleStock), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);

        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.TOP;
        sbView.setLayoutParams(params);
        snackbar.show();

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
}
