package com.totalbp.mmrequest.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.totalbp.mmrequest.R;
import com.totalbp.mmrequest.config.AppConfig;
import com.totalbp.mmrequest.config.SessionManager;
import com.totalbp.mmrequest.controller.MMController;
import com.totalbp.mmrequest.interfaces.VolleyCallback;
import com.totalbp.mmrequest.model.ApprovalEnt;
import com.totalbp.mmrequest.model.CommonAreaFormEnt;
import com.totalbp.mmrequest.model.CommonEnt;
import com.totalbp.mmrequest.model.CommonFloorFormEnt;
import com.totalbp.mmrequest.model.CommonMaterialFormEnt;
import com.totalbp.mmrequest.model.CommonRequestMoFormEnt;
import com.totalbp.mmrequest.model.CommonTowerFormEnt;
import com.totalbp.mmrequest.model.CommonWorkFormEnt;
import com.totalbp.mmrequest.receiver.ConnectivityReceiver;
import com.totalbp.mmrequest.utils.MProgressDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import static com.totalbp.mmrequest.R.color.colorPrimaryDark;
import static com.totalbp.mmrequest.R.color.login_forget_password_foot_color;
import static com.totalbp.mmrequest.config.AppConfig.URL_IMAGE_PREFIX;
import static com.totalbp.mmrequest.utils.ImageDecoder.decodeSampledBitmapFromFile;
import static com.totalbp.mmrequest.utils.ImageDecoder.getStringImage;

/**
 * Created by Ezra.R on 09/10/2017.
 */

public class RequestMaterialOutFinalActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    private String fromrequestmaterial;

    private SearchableSpinner spinnerSpr, spinnerTower, spinnerFloor, spinnerArea, spinnerWork, spinnerMaterial, spinnerMaterial1;
    private FloatingActionButton floatActButton;
    private Button btnAddMoreUnit;
    private String spr = "", tower = "",tower2 = "", floor = "", floortype = "", floorfull = "", floorfull2 = "", area = "", area2 = "", work = "",work2 = "", material = "", sprqty = "", thsprid = "", today = "", unit = "", tdsprid = "", typeitem = "";
    private String savePath;
    private int spnvol, tmovol, ttransvol;
    private String latestvol, idlveol, latestvolupdate;
    private ArrayList<CommonEnt> ddlArrayListTower = new ArrayList<>();
    private ArrayList<CommonTowerFormEnt> ddlArrayListTower2 = new ArrayList<>();
    private ArrayList<CommonFloorFormEnt> ddlArrayListFloor = new ArrayList<>();
    private ArrayList<CommonAreaFormEnt> ddlArrayListArea = new ArrayList<>();
    private ArrayList<CommonWorkFormEnt> ddlArrayListWork = new ArrayList<>();
    private ArrayList<CommonMaterialFormEnt> ddlArrayListMaterial = new ArrayList<>();

    private ArrayList<CommonTowerFormEnt> ListTowerTemp = new ArrayList<>();
    private ArrayList<CommonFloorFormEnt> ListFloorTemp = new ArrayList<>();
    private ArrayList<CommonAreaFormEnt> ListAreaTemp = new ArrayList<>();
    private ArrayList<CommonWorkFormEnt> ListWorkTemp = new ArrayList<>();

    private SessionManager session;
    MMController controller;
    public ProgressDialog pDialog;
    private EditText etVolumeRequest, etTower,etFloor,etArea,etWork, etLokasi1, etUnit1, etRequestVolume1;
    private TextView tvUnggahFoto, tvRequestBy, tvReadyStock, tvIdleStock, tvMyStock1, tvIdleStock1;
    JSONObject item;
    private ImageView imageHolder, ivItem, ivLocation, ivWork, ivMaterial;
    private final int requestCode = 30;
    private int year, month, day;
    Uri photoURI;
    public String mCurrentPhotoPath;
    static final int REQUEST_TAKE_PHOTO = 1;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    public LinearLayout llMoreForm;

    //Calendar myCalendar;
    Calendar myCalendar = new GregorianCalendar();
    public String id_daily_log_temp;

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);

    private static final int request_data_from  = 20;
    private static DecimalFormat df2 = new DecimalFormat(".##");
    public int count = 1;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requestmaterialoutformfinal);

        session = new SessionManager(getApplicationContext());

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if(bd != null)
        {
            fromrequestmaterial = (String) bd.get("fromrequestmaterial");
            Log.d("fromrequestmaterial", fromrequestmaterial);
        }

        //JIKA TIDAK ADA AKSES
        if(!session.getCanInsert().toString().equals("1"))
        {
            Toast.makeText(getApplicationContext(),"You dont have Privilege to Insert!", Toast.LENGTH_SHORT).show();
            this.finish();
        }

        today = dateFormat.format(myCalendar.getTime());

        year = myCalendar.get(Calendar.YEAR);
        month = myCalendar.get(Calendar.MONTH);
        day = myCalendar.get(Calendar.DAY_OF_MONTH);

        btnAddMoreUnit = (Button) findViewById(R.id.btnAddMoreUnit);
        llMoreForm = (LinearLayout) findViewById(R.id.llMoreForm);
        spinnerMaterial1  = (SearchableSpinner) findViewById(R.id.spinnerMaterial1);
        etLokasi1 = (EditText) findViewById(R.id.etLokasi1);
        etUnit1 = (EditText) findViewById(R.id.etUnit1);
        etRequestVolume1 = (EditText) findViewById(R.id.etRequestVolume1);
        tvIdleStock1 = (TextView) findViewById(R.id.tvIdleStock1);
        tvMyStock1 = (TextView) findViewById(R.id.tvMyStock1);


        getSupportActionBar().setTitle("Request Material Out");
        session = new SessionManager(getApplicationContext());

        controller = new MMController();
        pDialog = new ProgressDialog(this);
        dialog = new Dialog(RequestMaterialOutFinalActivity.this);

        btnAddMoreUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDinamicForm();
            }
        });

//        etLokasi1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                addDialogLokasi();
//            }
//        });

        getDataForDDL6(spinnerMaterial1, "SPT_SPR_Detail_Inq_MM_Param", "ID_M_Katalog", "Spec", ddlArrayListMaterial, "@TotalRecords", "", "@currentpage", "1", "@pagesize", "3000", "@sortby", "", "@wherecond", " WHERE b.Kode_Proyek ='" + session.getKodeProyek() + "' AND f.UserCode ='" + work + "' AND convert(varchar, M_Area.ID) ='" + area + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");

        spinnerMaterial1.setTitle("Select Material");
        spinnerMaterial1.setPositiveButton("OK");
        spinnerMaterial1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonMaterialFormEnt commonEnt = (CommonMaterialFormEnt) parent.getSelectedItem();
                material = commonEnt.getValue();
                thsprid = commonEnt.getValue4();
                unit = commonEnt.getValue3();
                tdsprid = commonEnt.getValue6();
                typeitem = commonEnt.getValue7();

                if(!material.equals("-")) {

                    Log.d("materialChoose", material);
                    Log.d("materialChoose", thsprid);
                    Log.d("materialChoose", unit);
                    Log.d("LogMo1",typeitem);
                    Log.d("LogMo1Floor",floorfull);
                    Log.d("LogMo1Area",area);
                    gtvIdleStockQty(material);
                    if(!tdsprid.equals("null") || !tdsprid.equals(""))
                    {

                        if(typeitem.equals("Prelim"))
                        {
                            getTowerLantaiZona(spinnerTower, "SPM_GetTowerLantaiAreaTempParam", "", "", ListTowerTemp, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "100", "@sortby", "", "@wherecond", " WHERE tdspr.ID ='" + tdsprid + "' "+typeitem, "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", ListFloorTemp, ListAreaTemp, ListWorkTemp, spinnerFloor, spinnerArea, spinnerWork, material, typeitem);
                        }
                        else
                        {
                            getTowerLantaiZona(spinnerTower, "SPM_GetTowerLantaiAreaTempParam", "", "", ListTowerTemp, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "100", "@sortby", "", "@wherecond", " WHERE tdspr.ID ='" + tdsprid + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", ListFloorTemp, ListAreaTemp,ListWorkTemp, spinnerFloor, spinnerArea, spinnerWork, material, typeitem);
                        }
                    }
                    else
                    {
                        gtvReadyStockQty(material, typeitem);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etRequestVolume1.addTextChangedListener(mTextEditorWatcher);



    }

    private final TextWatcher mTextEditorWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
            // When No Password Entered
            //textViewPasswordStrengthIndiactor.setText("Not Entered");
        }

        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

        }

        public void afterTextChanged(Editable s)
        {
            String et_ReadyStock = latestvol;
            String et_VolumeRequest = etRequestVolume1.getText().toString().trim();
            String et_IdleStock = idlveol;

            if (!et_ReadyStock.equals("0") || !et_ReadyStock.equals("0.0")) {
                if (!et_IdleStock.equals("0") || !et_IdleStock.equals("0.0")) {
                        if(et_VolumeRequest.matches("\\d+(?:\\.\\d+)?") && !et_VolumeRequest.equals("0.0") && !et_VolumeRequest.equals("0"))
                        {

                            String where = "";
                            Log.d("LogMoFloor",floorfull2);
                            Log.d("LogMoArea",area2);

                            //Jagaan saat parameter kode area kosong berarti wherecond diganti untuk menampilkan item migrasi
                            if(typeitem.equals("Migrasi-SPR") || typeitem.equals("Migrasi-Kontrak") || typeitem.equals("Migrasi-OtherKontrak") || typeitem.equals("Migrasi-AddKontrak") || typeitem.equals("Migrasi-NPO") || typeitem.equals("Prelim"))
                            {
                                where = " WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' ";
                            }
                            else
                            {
                                where = " WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' AND Kode_Lokasi = '"+floorfull2+"' AND Kode_Area = '"+area2+"' ";
                            }

                            session = new SessionManager(getApplicationContext());
                            controller.InqGeneral3(getApplicationContext(),"SPM_GetQtyReadyStockFromViewParam",
                                    "@KodeProject",session.getKodeProyek(),
                                    "@currentpage","1",
                                    "@pagesize","10",
                                    "@sortby","",
                                    "@wherecond",where,
                                    //"@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' AND Kode_Tower = '"+tower+"' AND Kode_Lokasi = '"+floortype+floor+"' AND Kode_Area = '"+area+"' ",
                                    //"@nik",String.valueOf(session.isLoggedIn()),
                                    //"@formid","MAMAN.02",
                                    //"@zonaid","",
                                    new VolleyCallback() {
                                        @Override
                                        public void onSave(String output) {

                                        }

                                        @Override
                                        public void onSuccess(JSONArray result) {
                                            try {
                                                Log.d("EZRAgtvReadyStockQty", result.toString());
                                                if (result.length() > 0)
                                                {
                                                    for (int i = 0; i < result.length(); i++) {
                                                        item = result.getJSONObject(i);
                                                        latestvolupdate = item.getString("LatestVol2");
                                                    }


                                                    //String ReadyStockNow =  latestvol;
                                                    float volumerequest = Float.parseFloat(etRequestVolume1.getText().toString());
                                                    float volumereadystock = Float.parseFloat(latestvol);
                                                    float volumeidlestock = Float.parseFloat(idlveol);

                                                    float readystocknow = Float.parseFloat(latestvolupdate);
                                                    Log.d("LogVolumRequest", String.valueOf(readystocknow));
                                                    //                              if (volumerequest <= volumereadystock) {
                                                    if (volumerequest <= readystocknow) {
                                                        String input = String.format(Locale.ENGLISH,"%.4f", volumerequest);
                                                        Log.d("LogVolumeInput", String.valueOf(input));
                                                        saveRequestMO(input, "0");
                                                    } else {

                                                        if (volumerequest <= (readystocknow + volumeidlestock)) {
                                                            float qtyfinal1 = volumerequest - readystocknow;

                                                            String inputreadystock = String.format(Locale.ENGLISH,"%.4f", volumereadystock);
                                                            saveRequestMO(inputreadystock, String.valueOf(qtyfinal1));
                                                        } else {
                                                            Toast toast = Toast.makeText(RequestMaterialOutFinalActivity.this, "Volume Request must less or equal than Ready Stock ("+readystocknow+"), or (Ready Stock + Idle Stock)!", Toast.LENGTH_LONG);
                                                            toast.setGravity(Gravity.CENTER, 0, 0);
                                                            toast.show();

                                                            etRequestVolume1.setText("");
                                                            etRequestVolume1.requestFocus();
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    latestvolupdate = "0";
                                                    Toast.makeText(RequestMaterialOutFinalActivity.this, "Check Qty Ready Stcok Latest Fail!", Toast.LENGTH_LONG).show();
                                                }
                                            }catch (JSONException e){
                                                Toast.makeText(getApplicationContext(), "CATCH gtvReadyStockQty", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });
                        }
                        else
                        {
                            Toast toast = Toast.makeText(RequestMaterialOutFinalActivity.this, "Volume Request must Number, Not Empty and > 0!", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                            etRequestVolume1.requestFocus();
                        }
                } else {
                    Toast toast = Toast.makeText(RequestMaterialOutFinalActivity.this, "Idle Stock required!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            } else {

                Toast toast = Toast.makeText(RequestMaterialOutFinalActivity.this, "Ready Stock required!", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    };

    public void addDinamicForm(){
        //CardView
        CardView cardView = new CardView(RequestMaterialOutFinalActivity.this);
        cardView.setId(View.generateViewId());
        //cardView.setCardBackgroundColor(ContextCompat.getColor(RequestMaterialOutFinalActivity.this, R.color.login_forget_password_foot_color));
        cardView.setRadius(4);
        AppBarLayout.LayoutParams params = new AppBarLayout.LayoutParams(
                AppBarLayout.LayoutParams.FILL_PARENT,
                AppBarLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(0,5,0,0);
        cardView.setLayoutParams(params);

        //LinearLayout llSec1
        LinearLayout llSec1 = new LinearLayout(RequestMaterialOutFinalActivity.this);
        AppBarLayout.LayoutParams paramsllSec1 = new AppBarLayout.LayoutParams(
                AppBarLayout.LayoutParams.MATCH_PARENT,
                AppBarLayout.LayoutParams.WRAP_CONTENT
        );
        llSec1.setLayoutParams(paramsllSec1);
        llSec1.setOrientation(LinearLayout.VERTICAL);

        //LinearLayout llSecDet11
        LinearLayout llSecDet11 = new LinearLayout(RequestMaterialOutFinalActivity.this);
        AppBarLayout.LayoutParams paramsllSecDet11 = new AppBarLayout.LayoutParams(
                AppBarLayout.LayoutParams.WRAP_CONTENT,
                AppBarLayout.LayoutParams.WRAP_CONTENT
        );
        paramsllSecDet11.setMargins(5,5,5,0);
        llSecDet11.setLayoutParams(paramsllSecDet11);
        llSecDet11.setOrientation(LinearLayout.HORIZONTAL);

        //TextView tvCapMatName1
        TextView tvCapMatName1 = new TextView(RequestMaterialOutFinalActivity.this);
        AppBarLayout.LayoutParams paramstvCapMatName1 = new AppBarLayout.LayoutParams(
                AppBarLayout.LayoutParams.MATCH_PARENT,
                AppBarLayout.LayoutParams.WRAP_CONTENT
        );
        paramstvCapMatName1.setMargins(15,0,0,0);
        tvCapMatName1.setLayoutParams(paramstvCapMatName1);
        tvCapMatName1.setText("MATERIAL NAME )*");
        tvCapMatName1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);

        //TextView tvCapMatNameDet1
        TextView tvCapMatNameDet1 = new TextView(RequestMaterialOutFinalActivity.this);
        AppBarLayout.LayoutParams paramstvCapMatNameDet1 = new AppBarLayout.LayoutParams(
                AppBarLayout.LayoutParams.MATCH_PARENT,
                AppBarLayout.LayoutParams.WRAP_CONTENT
        );
        paramstvCapMatNameDet1.setMargins(5,0,0,0);
        tvCapMatNameDet1.setLayoutParams(paramstvCapMatNameDet1);
        tvCapMatNameDet1.setText("Only material stocks that is already requested");
        tvCapMatNameDet1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);

        //DetilSec1
        llSecDet11.addView(tvCapMatName1);
        llSecDet11.addView(tvCapMatNameDet1);

        //LinearLayout llSecDet12
        LinearLayout llSecDet12 = new LinearLayout(RequestMaterialOutFinalActivity.this);
        AppBarLayout.LayoutParams paramsllSecDet12 = new AppBarLayout.LayoutParams(
                AppBarLayout.LayoutParams.MATCH_PARENT,
                AppBarLayout.LayoutParams.WRAP_CONTENT
        );
        paramsllSecDet12.setMargins(20,10,20,0);
        llSecDet12.setLayoutParams(paramsllSecDet12);
        llSecDet12.setOrientation(LinearLayout.HORIZONTAL);
        llSecDet12.setWeightSum(1);

            //LinearLayout llSecDetMaterial1
            LinearLayout llSecDetMaterial1 = new LinearLayout(RequestMaterialOutFinalActivity.this);
            AppBarLayout.LayoutParams paramsllSecDetMaterial1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    AppBarLayout.LayoutParams.MATCH_PARENT, (float) 0.5
            );
            llSecDetMaterial1.setLayoutParams(paramsllSecDetMaterial1);
            llSecDetMaterial1.setOrientation(LinearLayout.VERTICAL);
            llSecDetMaterial1.setBackgroundColor(ContextCompat.getColor(RequestMaterialOutFinalActivity.this, R.color.softyellow));

            //SearchableSpinner SearchableSpinner
            SearchableSpinner spinnerMaterial1 = new SearchableSpinner(RequestMaterialOutFinalActivity.this);
            final float scale = getResources().getDisplayMetrics().density;
            int dpWeightInPx = (int) (40 * scale);
            AppBarLayout.LayoutParams paramsspinnerMaterial1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    dpWeightInPx,1
            );

            int dpInPx = (int) (5 * scale);
            paramsspinnerMaterial1.setMargins(dpInPx,dpInPx,dpInPx,dpInPx);
            spinnerMaterial1.setLayoutParams(paramsspinnerMaterial1);

            llSecDetMaterial1.addView(spinnerMaterial1);
            llSecDet12.addView(llSecDetMaterial1);

            getDataForDDL6(spinnerMaterial1, "SPT_SPR_Detail_Inq_MM_Param", "ID_M_Katalog", "Spec", ddlArrayListMaterial, "@TotalRecords", "", "@currentpage", "1", "@pagesize", "3000", "@sortby", "", "@wherecond", " WHERE b.Kode_Proyek ='" + session.getKodeProyek() + "' AND f.UserCode ='" + work + "' AND convert(varchar, M_Area.ID) ='" + area + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");


        //LinearLayout llSecDet13
        LinearLayout llSecDet13 = new LinearLayout(RequestMaterialOutFinalActivity.this);
        AppBarLayout.LayoutParams paramsllSecDet13 = new AppBarLayout.LayoutParams(
                AppBarLayout.LayoutParams.MATCH_PARENT,
                AppBarLayout.LayoutParams.WRAP_CONTENT
        );
        paramsllSecDet13.setMargins(20,10,20,0);
        llSecDet13.setLayoutParams(paramsllSecDet13);
        llSecDet13.setOrientation(LinearLayout.HORIZONTAL);
        llSecDet13.setWeightSum(1);

            //LinearLayout llSecDetMaterial1
            LinearLayout llSecDetLokasi1 = new LinearLayout(RequestMaterialOutFinalActivity.this);
            AppBarLayout.LayoutParams paramsllSecDetLokasi1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    AppBarLayout.LayoutParams.WRAP_CONTENT, (float) 0.5
            );
            llSecDetLokasi1.setLayoutParams(paramsllSecDetLokasi1);
            llSecDetLokasi1.setOrientation(LinearLayout.VERTICAL);

            //TextView tvCapReqLokasi1
            TextView tvCapReqLokasi1 = new TextView(RequestMaterialOutFinalActivity.this);
            AppBarLayout.LayoutParams paramstvCapReqLokasi1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    AppBarLayout.LayoutParams.WRAP_CONTENT, 1
            );
//            paramstvCapReqLokasi1.setMargins(5,0,0,0);
            tvCapReqLokasi1.setLayoutParams(paramstvCapReqLokasi1);
            tvCapReqLokasi1.setText("REQUEST FOR AREA");
            tvCapReqLokasi1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);


            //TextView etLokasi1

            final float scalelokasi = getResources().getDisplayMetrics().density;
            int dpLokasiInPx = (int) (40 * scalelokasi);
            EditText etLokasi1 = new EditText(RequestMaterialOutFinalActivity.this);
            AppBarLayout.LayoutParams paramsetLokasi1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    dpLokasiInPx, 1
            );
            etLokasi1.setLayoutParams(paramsetLokasi1);
            etLokasi1.setTextColor(ContextCompat.getColor(RequestMaterialOutFinalActivity.this, R.color.icon_gray));
            etLokasi1.setBackgroundColor(ContextCompat.getColor(RequestMaterialOutFinalActivity.this, R.color.softyellow));

            llSecDetLokasi1.addView(tvCapReqLokasi1);
            llSecDetLokasi1.addView(etLokasi1);

            llSecDet13.addView(llSecDetLokasi1);

        //LinearLayout llSecDet14
        LinearLayout llSecDet14 = new LinearLayout(RequestMaterialOutFinalActivity.this);
        AppBarLayout.LayoutParams paramsllSecDet14 = new AppBarLayout.LayoutParams(
                AppBarLayout.LayoutParams.MATCH_PARENT,
                AppBarLayout.LayoutParams.WRAP_CONTENT
        );
        paramsllSecDet14.setMargins(20,10,20,0);
        llSecDet14.setLayoutParams(paramsllSecDet14);
        llSecDet14.setOrientation(LinearLayout.HORIZONTAL);
        llSecDet14.setWeightSum(1);

            //LinearLayout llSecDetMyStock1
            LinearLayout llSecDetMyStock1 = new LinearLayout(RequestMaterialOutFinalActivity.this);
            AppBarLayout.LayoutParams paramsllSecDetMyStock1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    AppBarLayout.LayoutParams.MATCH_PARENT, (float) 0.5
            );
            //paramsllSecDetMyStock1.setMargins(5,0,5,5);
            llSecDetMyStock1.setLayoutParams(paramsllSecDetMyStock1);
            llSecDetMyStock1.setOrientation(LinearLayout.VERTICAL);

            //TextView tvCapMyStock1
            TextView tvCapMyStock1 = new TextView(RequestMaterialOutFinalActivity.this);
            AppBarLayout.LayoutParams paramstvCapMyStock1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    AppBarLayout.LayoutParams.WRAP_CONTENT, 1
            );
//            paramstvCapMyStock1.setMargins(5,0,0,0);
            tvCapMyStock1.setLayoutParams(paramstvCapMyStock1);
            tvCapMyStock1.setText("MY STOCK [MAX]");
            tvCapMyStock1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);

            //TextView tvMyStock1
            TextView tvMyStock1 = new TextView(RequestMaterialOutFinalActivity.this);
            AppBarLayout.LayoutParams paramstvMyStock1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    AppBarLayout.LayoutParams.WRAP_CONTENT, 1
            );
//            paramstvMyStock1.setMargins(5,0,0,0);
            tvMyStock1.setLayoutParams(paramstvCapMyStock1);
            tvMyStock1.setText("1200 SACK");
            tvMyStock1.setTextColor(ContextCompat.getColor(RequestMaterialOutFinalActivity.this, R.color.colorPrimary));
            tvMyStock1.setTypeface(Typeface.DEFAULT_BOLD);

            llSecDetMyStock1.addView(tvCapMyStock1);
            llSecDetMyStock1.addView(tvMyStock1);

            //LinearLayout llSecDetIdleStock1
            LinearLayout llSecDetIdleStock1 = new LinearLayout(RequestMaterialOutFinalActivity.this);
            AppBarLayout.LayoutParams paramsllSecDetIdleStock1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    AppBarLayout.LayoutParams.MATCH_PARENT, (float) 0.5
            );
            //paramsllSecDetIdleStock1.setMargins(5,0,5,5);
            llSecDetIdleStock1.setLayoutParams(paramsllSecDetIdleStock1);
            llSecDetIdleStock1.setOrientation(LinearLayout.VERTICAL);

            //TextView tvCapIdleStock1
            TextView tvCapIdleStock1 = new TextView(RequestMaterialOutFinalActivity.this);
            AppBarLayout.LayoutParams paramstvCapIdleStock1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    AppBarLayout.LayoutParams.WRAP_CONTENT, 1
            );
            paramstvCapIdleStock1.setMargins(5,0,0,0);
            tvCapIdleStock1.setLayoutParams(paramstvCapIdleStock1);
            tvCapIdleStock1.setText("IDLE STOCK [MAX]");
            tvCapIdleStock1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);

            //TextView tvMyStock1
            TextView tvIdleStock1 = new TextView(RequestMaterialOutFinalActivity.this);
            AppBarLayout.LayoutParams paramstvIdleStock1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    AppBarLayout.LayoutParams.WRAP_CONTENT, 1
            );
            paramstvMyStock1.setMargins(5,0,0,0);
            tvIdleStock1.setLayoutParams(paramstvCapMyStock1);
            tvIdleStock1.setText("700 SACK");
            tvIdleStock1.setTextColor(ContextCompat.getColor(RequestMaterialOutFinalActivity.this, R.color.colorPrimary));
            tvIdleStock1.setTypeface(Typeface.DEFAULT_BOLD);

            llSecDetIdleStock1.addView(tvCapIdleStock1);
            llSecDetIdleStock1.addView(tvIdleStock1);

            llSecDet14.addView(llSecDetMyStock1);
            llSecDet14.addView(llSecDetIdleStock1);

        //LinearLayout llSecDet15
        LinearLayout llSecDet15 = new LinearLayout(RequestMaterialOutFinalActivity.this);
        AppBarLayout.LayoutParams paramsllSecDet15 = new AppBarLayout.LayoutParams(
                AppBarLayout.LayoutParams.MATCH_PARENT,
                AppBarLayout.LayoutParams.WRAP_CONTENT
        );
        paramsllSecDet15.setMargins(20,10,20,5);
        llSecDet15.setLayoutParams(paramsllSecDet15);
        llSecDet15.setOrientation(LinearLayout.HORIZONTAL);
        llSecDet15.setWeightSum(1);

            //LinearLayout llSecDetReqVol1
            LinearLayout llSecDetReqVol1 = new LinearLayout(RequestMaterialOutFinalActivity.this);
            AppBarLayout.LayoutParams paramsllSecDetReqVol1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    AppBarLayout.LayoutParams.MATCH_PARENT, (float) 0.3
            );

            llSecDetReqVol1.setLayoutParams(paramsllSecDetReqVol1);
            llSecDetReqVol1.setOrientation(LinearLayout.VERTICAL);

            //TextView tvCapReqVol1
            TextView tvCapReqVol1 = new TextView(RequestMaterialOutFinalActivity.this);
            AppBarLayout.LayoutParams paramstvCapReqVol1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    AppBarLayout.LayoutParams.WRAP_CONTENT, 1
            );

            tvCapReqVol1.setLayoutParams(paramstvCapReqVol1);
            tvCapReqVol1.setText("REQUEST VOLUME");
            tvCapReqVol1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);

            //Edittext 1
            //final float scalelokasi = getResources().getDisplayMetrics().density;
            int dprevolInPx = (int) (40 * scalelokasi);
            EditText etRequestVolume1 = new EditText(RequestMaterialOutFinalActivity.this);
            AppBarLayout.LayoutParams paramsetRequestVolume1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    dprevolInPx, 1
            );
            paramsetRequestVolume1.setMargins(0,0,10,0);
            etRequestVolume1.setLayoutParams(paramsetRequestVolume1);
            etRequestVolume1.setTextColor(ContextCompat.getColor(RequestMaterialOutFinalActivity.this, R.color.icon_gray));
            etRequestVolume1.setBackgroundColor(ContextCompat.getColor(RequestMaterialOutFinalActivity.this, R.color.softyellow));


            llSecDetReqVol1.addView(tvCapReqVol1);
            llSecDetReqVol1.addView(etRequestVolume1);
            //LinearLayout llSecDetUnit1
            LinearLayout llSecDetUnit1 = new LinearLayout(RequestMaterialOutFinalActivity.this);
            AppBarLayout.LayoutParams paramsllSecDetUnit1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    AppBarLayout.LayoutParams.MATCH_PARENT, (float) 0.7
            );
            paramsllSecDetUnit1.setMargins(5,0,0,0);
            llSecDetUnit1.setLayoutParams(paramsllSecDetUnit1);
            llSecDetUnit1.setOrientation(LinearLayout.VERTICAL);

            //TextView tvCapUnit1
            TextView tvCapUnit1 = new TextView(RequestMaterialOutFinalActivity.this);
            AppBarLayout.LayoutParams paramstvCapUnit1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    AppBarLayout.LayoutParams.WRAP_CONTENT, 1
            );
            paramstvCapUnit1.setMargins(5,0,0,0);
            tvCapUnit1.setLayoutParams(paramstvCapUnit1);
            tvCapUnit1.setText("UNIT");
            tvCapUnit1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);

            EditText etUnit1 = new EditText(RequestMaterialOutFinalActivity.this);
            AppBarLayout.LayoutParams paramsetUnit1 = new AppBarLayout.LayoutParams(
                    AppBarLayout.LayoutParams.MATCH_PARENT,
                    dprevolInPx, 1
            );
            paramsetUnit1.setMargins(5,0,0,0);
            etUnit1.setLayoutParams(paramsetUnit1);
            etUnit1.setTextColor(ContextCompat.getColor(RequestMaterialOutFinalActivity.this, R.color.icon_gray));
            etUnit1.setBackgroundColor(ContextCompat.getColor(RequestMaterialOutFinalActivity.this, R.color.softyellow));

            llSecDetUnit1.addView(tvCapUnit1);
            llSecDetUnit1.addView(etUnit1);

            llSecDet15.addView(llSecDetReqVol1);
            llSecDet15.addView(llSecDetUnit1);

        llSec1.addView(llSecDet11);
        llSec1.addView(llSecDet12);
        llSec1.addView(llSecDet13);
        llSec1.addView(llSecDet14);
        llSec1.addView(llSecDet15);
        cardView.addView(llSec1);
        llMoreForm.addView(cardView);
    }

    public void addDialogLokasi(){
        dialog = new Dialog(RequestMaterialOutFinalActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_towerlantaiarea);
        dialog.setCanceledOnTouchOutside(false);

        SearchableSpinner spinnerTower = (SearchableSpinner) dialog.findViewById(R.id.spinnerTower);
        SearchableSpinner spinnerFloor = (SearchableSpinner) dialog.findViewById(R.id.spinnerFloor);
        SearchableSpinner spinnerArea = (SearchableSpinner) dialog.findViewById(R.id.spinnerArea);

        getDataForDDL2(spinnerTower, "SPM_GetTower_Param", "Kode_Tower", "Nama_Tower", ddlArrayListTower2, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "100", "@sortby", "", "@wherecond", " AND Kode_Proyek ='" + session.getKodeProyek() + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
        spinnerTower.setTitle("Select Tower");
        spinnerTower.setPositiveButton("OK");
        spinnerTower.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonTowerFormEnt commonEnt = (CommonTowerFormEnt) parent.getSelectedItem();
                tower = commonEnt.getValue();
                if(!tower.equals("-")) {
                    getDataForDDL3(spinnerFloor, "SPM_GetFloor_Param", "Kode_Lantai", "Nama_Lantai", ddlArrayListFloor, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", " Nama_Lantai ASC ", "@wherecond", " AND M_Zona.Kode_Proyek ='" + session.getKodeProyek() + "' AND M_Zona.Kode_Zona ='" + tower + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    Log.d("towerChoose", tower);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerFloor.setTitle("Select Floor");
        spinnerFloor.setPositiveButton("OK");
        spinnerFloor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonFloorFormEnt commonEnt = (CommonFloorFormEnt) parent.getSelectedItem();
                floor = commonEnt.getValue();
                floortype = commonEnt.getValue2();
                floorfull = floortype+floor;
                //work = commonEnt.getValue();
                if(!floor.equals("-")) {
                    getDataForDDL4(spinnerArea, "SPM_GetArea_Param", "Kode_Lokasi", "Nama_Area", ddlArrayListArea, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", "", "@wherecond", " AND M_Zona.Kode_Proyek ='" + session.getKodeProyek() + "' AND M_Zona.Kode_Zona ='" + tower + "' AND M_Area.Kode_Lokasi ='" + floorfull + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    //getDataForDDL5(spinnerWork, "SPM_GetWorkBasedSprAndTowerAndFloorMobile_Param", "Kode_Work", "Nama_Work", ddlArrayListWork, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", "", "@wherecond", " AND a.Kode_Proyek ='" + session.getKodeProyek() + "' AND d.Kode_Zona ='" + tower + "' AND c.Kode_Lokasi ='" + floor + "' ");
                    Log.d("floorChoose", floor);
                    // Log.d("workChoose", work);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerArea.setTitle("Select Area");
        spinnerArea.setPositiveButton("OK");
        spinnerArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonAreaFormEnt commonEnt = (CommonAreaFormEnt) parent.getSelectedItem();
                area = commonEnt.getValue();
                if(!area.equals("-")) {
//                    getDataForDDL5(spinnerWork, "SPM_GetWork_Param", "Kode_Pekerjaan", "Nama_Pekerjaan", ddlArrayListWork, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", "", "@wherecond", " WHERE [M_Area].Kode_Proyek = '"+session.getKodeProyek()+"' AND m_zona.Kode_Zona = '"+tower+"' AND m_lokasi.Tipe_Lokasi+m_lokasi.Kode_Lokasi = '"+floorfull+"' AND [M_Area].ID = '"+area+"' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    Log.d("areaChoose", area);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ImageButton dIBClose = (ImageButton) dialog.findViewById(R.id.dIBClose);

        dIBClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                etLokasi1.setText(spinnerTower.getSelectedItem().toString() + " : "+spinnerFloor.getSelectedItem().toString()+" : "+spinnerArea.getSelectedItem().toString());
//                Intent intent = new Intent();
//                intent.putExtra("", "");
//                setResult(request_data_from, intent);
//                finish();
            }
        });

        dialog.show();
    }

    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //MainActivity activity = (MainActivity)getActivity();

        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
//            photoFile = getOutputMediaFile(); //untuk sementara
//            try {
//                photoFile = createImageFile();
//
//            } catch (IOException ex) {
//                Toast toast = Toast.makeText(activity, "There was a problem saving the photo...", Toast.LENGTH_SHORT);
//                toast.show();
//            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                mCurrentPhotoPath = photoFile.getAbsolutePath();
//                Log.d("path",mCurrentPhotoPath);
//                photoURI = Uri.fromFile(photoFile);

                photoURI = FileProvider.getUriForFile(getApplicationContext(), "com.totalbp.mmrequest.fileprovider", photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,photoURI);
//                takePictureIntent.putExtra("id_daily_log",id_daily_log);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private  File createImageFile() throws IOException { //bener2 simpen filenya di storage directory
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()){
            if (!storageDir.mkdirs()){
                return null;
            }
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        return image;
    }

    private void displayDialog(String id_daily_log) {
        UploadPicture(id_daily_log);
        //materialDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            Toast.makeText(getBaseContext(), "Photo Taken",Toast.LENGTH_SHORT).show();

            displayDialog(id_daily_log_temp);

            final Bitmap selectedImage = decodeSampledBitmapFromFile(mCurrentPhotoPath,500, 250);
            String encodedImage = getStringImage(selectedImage);

            byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            //Start
            //Code By Ezra
            //bitmapnya dideclare ulang terlebih dahulu untuk dibenarkan rotatenya kemudian baru diset ke imageview
            ExifInterface ei = null;
            try {
                ei = new ExifInterface(mCurrentPhotoPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            Bitmap rotatedBitmap = null;
            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(decodedByte, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(decodedByte, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(decodedByte, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = decodedByte;
            }
            //Close
            imageHolder.setImageBitmap(rotatedBitmap);

        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private void UploadPicture(String id_daily_log){
        final Bitmap selectedImage = decodeSampledBitmapFromFile(mCurrentPhotoPath,500, 500);
        String filename="DP" +"_"+ mCurrentPhotoPath.substring(mCurrentPhotoPath.lastIndexOf("/")+1);
        String encodedImage = getStringImage(selectedImage);

        //Start
        //Code by Ezra
        //Decode terlebih dahulu untuk dibenarkan rotatenya kemudian baru di upload
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        ExifInterface ei = null;
        try {
            ei = new ExifInterface(mCurrentPhotoPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap rotatedBitmap = null;
        switch(orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(decodedByte, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(decodedByte, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(decodedByte, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = decodedByte;
        }

        String encodedImage2 = getStringImage(rotatedBitmap);
        //end
        UploadFileToServer(encodedImage2,filename, id_daily_log);

    }

    public void UploadFileToServer(String base64encode, String filename, final String id_hse_assessment){

        MProgressDialog.showProgressDialog(RequestMaterialOutFinalActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        RequestQueue requestQueue;
        session = new SessionManager(getApplicationContext());
        JSONObject request = new JSONObject();

        try {
            request.put("Pengubah", session.getUserDetails().get("nik"));
            request.put("FileName", filename);
            request.put("TokenID", session.getUserToken());
            request.put("FormID", "AR.03.01");
            request.put("KodeProyek",session.getKodeProyek()); //Dummy
            request.put("Base64String",base64encode);
        } catch (Exception e) {
            e.printStackTrace();
        }

        requestQueue = Volley.newRequestQueue(getApplicationContext());
//        http://10.100.223.51:20173/API/TQA/Upload
        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, session.getUrlConfig()+AppConfig.URL_UPLOAD_GAMBAR,request,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("LogUploadPicPenerimaGbr",response.toString());
                            if(response.getString("IsSucceed").equals("true")){
                               // Toast.makeText(getApplicationContext(), response.getString("Message"), Toast.LENGTH_LONG).show();
//                                String savePath = response.getString("VarOutput");
                                String pathDatabase = response.getString("VarOutput"); //hashcode
                                String yearstring = String.valueOf(year);
                                String monthstring = "0"+String.valueOf(month+1);
                                savePath = session.getUrlConfig()+URL_IMAGE_PREFIX + "/GeneralFile/"+yearstring.substring(yearstring.length()-2)+"/"+monthstring.substring(monthstring.length()-2)+"/" + response.getString("Message") + ".jpg";

                                Log.d("savePath",savePath);
                                MProgressDialog.dismissProgressDialog();

                            }
                            else{
                                Toast.makeText(getApplicationContext(), response.getString("Message "+"Please Re-Login"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Error Response " + error.toString(), Toast.LENGTH_LONG).show();
                    }
                });
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(strReq);
    }

    private void saveRequestMO(String qty, String qty2) {

        MProgressDialog.showProgressDialog(RequestMaterialOutFinalActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        if(tdsprid.equals("null"))
        {
            tdsprid = "0";
        }
        Gson gsonHeader = new Gson();
        CommonRequestMoFormEnt commonRequestMoFormEnt = new CommonRequestMoFormEnt();
        commonRequestMoFormEnt.setID("0");
        commonRequestMoFormEnt.setKode_MO(session.getKodeProyek());
        commonRequestMoFormEnt.setKode_Proyek(session.getKodeProyek());
        commonRequestMoFormEnt.setKode_Item(material);
        commonRequestMoFormEnt.setVolume(qty);
        commonRequestMoFormEnt.setUnit(unit);
        commonRequestMoFormEnt.setWork_Code(work2);
        commonRequestMoFormEnt.setKode_Tower(tower2);
        commonRequestMoFormEnt.setKode_Lantai(floorfull2);
        commonRequestMoFormEnt.setKode_Zona(area2);
        commonRequestMoFormEnt.setCourierPhoto(savePath);
        commonRequestMoFormEnt.setRefHeaderSPR(thsprid);
        commonRequestMoFormEnt.setRefDetailSPR(tdsprid);
        commonRequestMoFormEnt.setStatusRequest("1");
        commonRequestMoFormEnt.setApprovalNo("");
        commonRequestMoFormEnt.setStatusAktif("True");
        commonRequestMoFormEnt.setPembuat(session.getUserName());
        commonRequestMoFormEnt.setNama_Pembuat(session.getKeyNik());
        commonRequestMoFormEnt.setWaktuBuat(today);
        commonRequestMoFormEnt.setPengubah(session.getKeyNik());
        commonRequestMoFormEnt.setNama_Pengubah(session.getKeyNik());
        commonRequestMoFormEnt.setWaktuUbah(today);
        commonRequestMoFormEnt.setComment("");
        commonRequestMoFormEnt.setTokenID(session.getUserToken());
        commonRequestMoFormEnt.setVolumeIdle(qty2);
        //commonRequestMoFormEnt.setModeReq("POSTING");
        commonRequestMoFormEnt.setFormId("MAMAN.02");
        String jsonString = gsonHeader.toJson(commonRequestMoFormEnt);

        controller.SaveGeneralObjectRequestMo(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                    MProgressDialog.dismissProgressDialog();
                    try {

                        JSONObject obj = new JSONObject(output);

                        if(!obj.getString("IsSucceed").equals("false")) {

                            String output2 = obj.getString("VarOutput");
                            String[] split = output2.split("#");
                            String firstString = split[0];
                            String firstSecondstring = split[1];

                            isCanPosting(firstString, firstSecondstring);
                        }
                        else
                        {
                            Toast toast = Toast.makeText(RequestMaterialOutFinalActivity.this,"Save Failed, "+obj.getString("Message")+" ! ", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                            Intent intent = new Intent();
                            intent.putExtra("", "");
                            setResult(request_data_from, intent);
                            finish();
                        }

                    } catch (Throwable t) {
                        Log.e("My App", "Could not parse malformed JSON: \"" + output + "\"");
                    }
//                }
//                else
//                {
//                    Toast toast = Toast.makeText(RequestMaterialOutActivity.this,"Save Failed!", Toast.LENGTH_LONG);
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();
//
//                    Intent intent = new Intent();
//                    intent.putExtra("", "");
//                    setResult(request_data_from, intent);
//                    finish();
//                }
            }
        });

    }


    private void isCanPosting(String kodematerialout, String IdMo) {

        MProgressDialog.showProgressDialog(RequestMaterialOutFinalActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("is_can_posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
//        approvalEnt.setApproval_Name("Approval Material Out Management Mobile");
        approvalEnt.setTokenID(session.getUserToken());

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

                if(jsonObject.get("IsCanPosting").getAsString().equals("true"))
                {
                    dialog = new Dialog(RequestMaterialOutFinalActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.general_dialog);
                    dialog.setCanceledOnTouchOutside(false);

                    TextView dTittle = (TextView) dialog.findViewById(R.id.dTittle);
                    TextView dContent = (TextView) dialog.findViewById(R.id.dContent);
                    Button dBtnCancel = (Button) dialog.findViewById(R.id.dBtnCancel);
                    Button dBtnSubmit = (Button) dialog.findViewById(R.id.dBtnSubmit);

                    dTittle.setText("Material Out");
                    dContent.setText("Do you want Post Mo number "+kodematerialout+" ?");

                    dBtnSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onPosting(kodematerialout, IdMo);
                        }
                    });


                    dBtnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            Intent intent = new Intent();
                            intent.putExtra("", "");
                            setResult(request_data_from, intent);
                            finish();
                        }
                    });

                    dialog.show();


                }
                else
                {
                    Toast toast = Toast.makeText(RequestMaterialOutFinalActivity.this,"Request Material Save Success!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    Intent intent = new Intent();
                    intent.putExtra("", "");
                    setResult(request_data_from, intent);
                    finish();
                }

            }
        });
    }

    private void onPosting(String kodematerialout, String IdMo) {

        MProgressDialog.showProgressDialog(RequestMaterialOutFinalActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        //Log.d("OmOm", kodematerialout+"|"+IdMo);
        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
        approvalEnt.setTokenID(session.getUserToken());

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {


                Log.d("OmOm2", output);
                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

//                if(jsonObject.get("IsCanApprove").getAsString().equals("true"))
//                {
//                    Log.d("OmOm3", output);
                    updateRequestMO(jsonObject.get("Approval_Number").getAsString(), IdMo, kodematerialout);
//                }
//                else
//                {
//                    Toast toast = Toast.makeText(RequestMaterialOutActivity.this,"You already posting !", Toast.LENGTH_LONG);
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();
//
//                    Intent intent = new Intent();
//                    intent.putExtra("", "");
//                    setResult(request_data_from, intent);
//                    finish();
//                }

            }
        });
    }

    private void IsCanApprove(String kodematerialout, String ApprovalNumber, String IdMo) {

        MProgressDialog.showProgressDialog(RequestMaterialOutFinalActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        //Log.d("OmOm", kodematerialout+"|"+IdMo);
        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
        approvalEnt.setTokenID(session.getUserToken());

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

//                Log.d("OmOm2", output);
//                MProgressDialog.dismissProgressDialog();
//                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
//                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

                //updateRequestMO(jsonObject.get("Approval_Number").getAsString(), IdMo);
            }
        });
    }

    private void updateRequestMO(String ApprovalNumber, String IdMo, String KodeMaterialOut) {
        Log.d("OmOm4", ApprovalNumber);
        MProgressDialog.showProgressDialog(RequestMaterialOutFinalActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();

        CommonRequestMoFormEnt commonRequestMoFormEnt = new CommonRequestMoFormEnt();
        commonRequestMoFormEnt.setID(IdMo);
        commonRequestMoFormEnt.setKode_MO(KodeMaterialOut);
        commonRequestMoFormEnt.setKode_Proyek(session.getKodeProyek());
        commonRequestMoFormEnt.setKode_Item("");
        commonRequestMoFormEnt.setVolume("1");
        commonRequestMoFormEnt.setUnit("");
        commonRequestMoFormEnt.setWork_Code("");
        commonRequestMoFormEnt.setKode_Tower("");
        commonRequestMoFormEnt.setKode_Lantai("");
        commonRequestMoFormEnt.setKode_Zona("0");
        commonRequestMoFormEnt.setCourierPhoto("");
        commonRequestMoFormEnt.setRefHeaderSPR("0");
        commonRequestMoFormEnt.setRefDetailSPR("0");
        commonRequestMoFormEnt.setStatusRequest("1");
        commonRequestMoFormEnt.setApprovalNo(ApprovalNumber);
        commonRequestMoFormEnt.setStatusAktif("True");
        commonRequestMoFormEnt.setPembuat(session.getUserName());
        commonRequestMoFormEnt.setNama_Pembuat(session.getUserName());
        commonRequestMoFormEnt.setWaktuBuat(today);
        commonRequestMoFormEnt.setPengubah(session.getKeyNik());
        commonRequestMoFormEnt.setNama_Pengubah(session.getUserName());
        commonRequestMoFormEnt.setWaktuUbah(today);
        commonRequestMoFormEnt.setComment("");
        commonRequestMoFormEnt.setTokenID(session.getUserToken());
        //commonRequestMoFormEnt.setModeReq("POSTING");
        commonRequestMoFormEnt.setFormId("MAMAN.02");
//
        String jsonString = gsonHeader.toJson(commonRequestMoFormEnt);
        Log.d("OmOm5", ApprovalNumber);
        controller.SaveGeneralObjectRequestMo(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                Toast toast = Toast.makeText(RequestMaterialOutFinalActivity.this,KodeMaterialOut+" Posting Success !", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                Intent intent = new Intent();
                intent.putExtra("", "");
                setResult(request_data_from, intent);
                finish();

            }
        });
    }

    public void initControls(){


        //getDataForDDL(spinnerSpr,"SPM_GetThSprMobile_Param","ID","Kode_Spr",ddlArrayListTower,"@KodeProject",session.getKodeProyek(),"@currentpage","1","@pagesize","10000","@sortby","","@wherecond"," AND Kode_Proyek ='"+session.getKodeProyek()+"' ");
        //spinnerSpr.setTitle("Select SPR");
        //spinnerSpr.setPositiveButton("OK");
        //spinnerSpr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                final CommonEnt commonEnt = (CommonEnt) parent.getSelectedItem();
//                spr = commonEnt.getValue();
//                Log.d("sprChoose", spr);

//                if(!spr.equals("-")) {
                    getDataForDDL2(spinnerTower, "SPM_GetTower_Param", "Kode_Tower", "Nama_Tower", ddlArrayListTower2, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "100", "@sortby", "", "@wherecond", " AND Kode_Proyek ='" + session.getKodeProyek() + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    getDataForDDL6(spinnerMaterial, "SPT_SPR_Detail_Inq_MM_Param", "ID_M_Katalog", "Spec", ddlArrayListMaterial, "@TotalRecords", "", "@currentpage", "1", "@pagesize", "3000", "@sortby", "", "@wherecond", " WHERE b.Kode_Proyek ='" + session.getKodeProyek() + "' AND f.UserCode ='" + work + "' AND convert(varchar, M_Area.ID) ='" + area + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");

        //
                    //gtvReadyStockQty(spr);
                //}
            //}
            //@Override
            //public void onNothingSelected(AdapterView<?> parent) {
            //}
        //});

        spinnerTower.setTitle("Select Tower");
        spinnerTower.setPositiveButton("OK");
        spinnerTower.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonTowerFormEnt commonEnt = (CommonTowerFormEnt) parent.getSelectedItem();
                tower = commonEnt.getValue();
                if(!tower.equals("-")) {
                    getDataForDDL3(spinnerFloor, "SPM_GetFloor_Param", "Kode_Lantai", "Nama_Lantai", ddlArrayListFloor, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", " Nama_Lantai ASC ", "@wherecond", " AND M_Zona.Kode_Proyek ='" + session.getKodeProyek() + "' AND M_Zona.Kode_Zona ='" + tower + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    Log.d("towerChoose", tower);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerFloor.setTitle("Select Floor");
        spinnerFloor.setPositiveButton("OK");
        spinnerFloor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonFloorFormEnt commonEnt = (CommonFloorFormEnt) parent.getSelectedItem();
                floor = commonEnt.getValue();
                floortype = commonEnt.getValue2();
                floorfull = floortype+floor;
                //work = commonEnt.getValue();
                if(!floor.equals("-")) {
                    getDataForDDL4(spinnerArea, "SPM_GetArea_Param", "Kode_Lokasi", "Nama_Area", ddlArrayListArea, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", "", "@wherecond", " AND M_Zona.Kode_Proyek ='" + session.getKodeProyek() + "' AND M_Zona.Kode_Zona ='" + tower + "' AND M_Area.Kode_Lokasi ='" + floorfull + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    //getDataForDDL5(spinnerWork, "SPM_GetWorkBasedSprAndTowerAndFloorMobile_Param", "Kode_Work", "Nama_Work", ddlArrayListWork, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", "", "@wherecond", " AND a.Kode_Proyek ='" + session.getKodeProyek() + "' AND d.Kode_Zona ='" + tower + "' AND c.Kode_Lokasi ='" + floor + "' ");
                    Log.d("floorChoose", floor);
                   // Log.d("workChoose", work);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerArea.setTitle("Select Area");
        spinnerArea.setPositiveButton("OK");
        spinnerArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonAreaFormEnt commonEnt = (CommonAreaFormEnt) parent.getSelectedItem();
                area = commonEnt.getValue();
                if(!area.equals("-")) {
                    getDataForDDL5(spinnerWork, "SPM_GetWork_Param", "Kode_Pekerjaan", "Nama_Pekerjaan", ddlArrayListWork, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "10000", "@sortby", "", "@wherecond", " WHERE [M_Area].Kode_Proyek = '"+session.getKodeProyek()+"' AND m_zona.Kode_Zona = '"+tower+"' AND m_lokasi.Tipe_Lokasi+m_lokasi.Kode_Lokasi = '"+floorfull+"' AND [M_Area].ID = '"+area+"' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    Log.d("areaChoose", area);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerWork.setTitle("Select Work");
        spinnerWork.setPositiveButton("OK");
        spinnerWork.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonWorkFormEnt commonEnt = (CommonWorkFormEnt) parent.getSelectedItem();
                work = commonEnt.getValue();
                if(!work.equals("-")) {
                    //getDataForDDL6(spinnerMaterial, "SPT_SPR_Detail_Inq_MM_Param", "ID_M_Katalog", "Spec", ddlArrayListMaterial, "@TotalRecords", "", "@currentpage", "1", "@pagesize", "1000", "@sortby", "", "@wherecond", " AND b.Kode_Proyek ='" + session.getKodeProyek() + "' AND f.UserCode ='"+work+"' ", "@nik", String.valueOf(session.isLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");
                    getDataForDDL6(spinnerMaterial, "SPT_SPR_Detail_Inq_MM_Param", "ID_M_Katalog", "Spec", ddlArrayListMaterial, "@TotalRecords", "", "@currentpage", "1", "@pagesize", "2000", "@sortby", "", "@wherecond", " WHERE b.Kode_Proyek ='" + session.getKodeProyek() + "' AND f.UserCode ='" + work + "' AND convert(varchar, M_Area.ID) ='" + area + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", "@zonaid","");

                    Log.d("workChoose", work);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerMaterial.setTitle("Select Material");
        spinnerMaterial.setPositiveButton("OK");
        spinnerMaterial.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final CommonMaterialFormEnt commonEnt = (CommonMaterialFormEnt) parent.getSelectedItem();
                material = commonEnt.getValue();
                thsprid = commonEnt.getValue4();
                unit = commonEnt.getValue3();
                tdsprid = commonEnt.getValue6();
                typeitem = commonEnt.getValue7();



                if(!material.equals("-")) {

                    Log.d("materialChoose", material);
                    Log.d("materialChoose", thsprid);
                    Log.d("materialChoose", unit);
                    Log.d("LogMo1",typeitem);
                    Log.d("LogMo1Floor",floorfull);
                    Log.d("LogMo1Area",area);
                    gtvIdleStockQty(material);
                    if(!tdsprid.equals("null") || !tdsprid.equals(""))
                    {

                       if(typeitem.equals("Prelim"))
                       {
                           getTowerLantaiZona(spinnerTower, "SPM_GetTowerLantaiAreaTempParam", "", "", ListTowerTemp, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "100", "@sortby", "", "@wherecond", " WHERE tdspr.ID ='" + tdsprid + "' "+typeitem, "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", ListFloorTemp, ListAreaTemp, ListWorkTemp, spinnerFloor, spinnerArea, spinnerWork, material, typeitem);
                       }
                       else
                       {
                           getTowerLantaiZona(spinnerTower, "SPM_GetTowerLantaiAreaTempParam", "", "", ListTowerTemp, "@KodeProject", session.getKodeProyek(), "@currentpage", "1", "@pagesize", "100", "@sortby", "", "@wherecond", " WHERE tdspr.ID ='" + tdsprid + "' ", "@nik", String.valueOf(session.isNikUserLoggedIn()), "@formid", "MAMAN.02", ListFloorTemp, ListAreaTemp,ListWorkTemp, spinnerFloor, spinnerArea, spinnerWork, material, typeitem);
                       }
                    }
                    else
                    {
                        gtvReadyStockQty(material, typeitem);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //getDataForDDL3(spinnerZone,"SPM_GetArea_Param","Kode_Area","Nama_Area",ddlArrayListTower2,"@KodeProject",session.getKodeProyek(),"@currentpage","1","@pagesize","100","@sortby","","@wherecond"," AND Kode_Proyek ='"+session.getKodeProyek()+"' GROUP BY ID, Deskripsi");
        //getDataForDDL4(spinnerMaterial,"SPM_GetMaterial_Param","Kode_Item","Nama_Item",ddlArrayListTower3,"@KodeProject",session.getKodeProyek(),"@currentpage","1","@pagesize","200","@sortby","","@wherecond"," GROUP BY ID, Deskripsi");
    }

    public void showDialog() {
        pDialog.setMessage("Loading ...");
        if (!pDialog.isShowing())
            pDialog.show();
    }

    public void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void getDataForDDL(final SearchableSpinner spinner, final String uniqueSPName,
                              final String paramName, final String paramVal,
                              final ArrayList<CommonEnt> ddlArrayList,
                              final String paramName1, final String paramVal1,
                              final String paramName2, final String paramVal2,
                              final String paramName3, final String paramVal3,
                              final String paramName4, final String paramVal4,
                              final String paramName5, final String paramVal5,
                              final String paramName6, final String paramVal6,
                              final String paramName7, final String paramVal7,
                              final String paramName8, final String paramVal8){
        ddlArrayList.clear();

        MProgressDialog.showProgressDialog(RequestMaterialOutFinalActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
            @Override
            public void onSuccess(JSONArray result) {
                try {
                    if (result.length() > 0) {

                        CommonEnt itemEntsinit = new CommonEnt(
                                "-","-",
                                "-","-",
                                "-","-",
                                "-","-",
                                "-","-"
                        );
                        ddlArrayList.add(itemEntsinit);

                        for (int i = 0; i < result.length(); i++) {
                            JSONObject item = result.getJSONObject(i);

                            CommonEnt itemEnts = new CommonEnt(item.getString(paramVal), item.getString(paramName),
                                    item.getString(paramName), item.getString(paramName),
                                    item.getString(paramName), item.getString(paramName),
                                    item.getString(paramName), item.getString(paramName),
                                    item.getString(paramName), item.getString(paramName)
                            );
                            ddlArrayList.add(itemEnts);
                        }
                        MProgressDialog.dismissProgressDialog();
                        ArrayAdapter<CommonEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                        spinner.setAdapter(adapterDDLGlobal);
                    }
                    else{
                        CommonEnt itemEnts = new CommonEnt(
                                "No Item Found","",
                                "No Item Found","",
                                "No Item Found","",
                                "No Item Found","",
                                "No Item Found",""
                        );
                        ddlArrayList.add(itemEnts);
                        ArrayAdapter<CommonEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                        spinner.setAdapter(adapterDDLGlobal);
                    }
                    MProgressDialog.dismissProgressDialog();
                }catch (JSONException e){
                    Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                }
            }


            @Override
            public void onSave(String output) {

            }
        });
    }

    public void getDataForDDL2(final SearchableSpinner spinner, final String uniqueSPName,
                              final String paramName, final String paramVal,
                              final ArrayList<CommonTowerFormEnt> ddlArrayList,
                              final String paramName1, final String paramVal1,
                              final String paramName2, final String paramVal2,
                              final String paramName3, final String paramVal3,
                              final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();

//        MProgressDialog.showProgressDialog(RequestMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                MProgressDialog.dismissProgressDialog();
//            }
//        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {

                                CommonTowerFormEnt itemEntsinit = new CommonTowerFormEnt(
                                        "-select tower-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );
                                ddlArrayList.add(itemEntsinit);

                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonTowerFormEnt itemEnts = new CommonTowerFormEnt(item.getString(paramVal), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                }
                               // MProgressDialog.dismissProgressDialog();
                                ArrayAdapter<CommonTowerFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonTowerFormEnt itemEnts = new CommonTowerFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonTowerFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            //MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void getTowerLantaiZona(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonTowerFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                                   final ArrayList<CommonFloorFormEnt> ddlArrayList2,
                                   final ArrayList<CommonAreaFormEnt> ddlArrayList3,
                                   final ArrayList<CommonWorkFormEnt> ddlArrayList4,
                                   final SearchableSpinner spinner2, final SearchableSpinner spinner3, final SearchableSpinner spinner4, String materialparam, String typeitemparam){
        ddlArrayList.clear();
        ddlArrayList2.clear();
        ddlArrayList3.clear();
        ddlArrayList4.clear();

//        MProgressDialog.showProgressDialog(RequestMaterialOutActivity.this, true, new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                MProgressDialog.dismissProgressDialog();
//            }
//        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {

                                for (int i = 0; i < result.length(); i++) {

                                    JSONObject item = result.getJSONObject(i);

//                                    CommonTowerFormEnt itemEnts = new CommonTowerFormEnt(item.getString("Nama_Zona"), "-",
//                                            "", "",
//                                            "", "",
//                                            "", "",
//                                            "", ""
//                                    );
//                                    ddlArrayList.add(itemEnts);
//
//                                    CommonFloorFormEnt itemEnts2 = new CommonFloorFormEnt(item.getString("Nama_Lokasi"), "-",
//                                            "", "",
//                                            "", "",
//                                            "", "",
//                                            "", ""
//                                    );
//                                    ddlArrayList2.add(itemEnts2);
//
//                                    CommonAreaFormEnt itemEnts3 = new CommonAreaFormEnt(item.getString("Deskripsi"), "-",
//                                            "", "",
//                                            "", "",
//                                            "", "",
//                                            "", ""
//                                    );
//                                    ddlArrayList3.add(itemEnts3);
//
//                                    CommonWorkFormEnt itemEnts4 = new CommonWorkFormEnt(item.getString("NamaPekerjaan"), "-",
//                                            "", "",
//                                            "", "",
//                                            "", "",
//                                            "", ""
//                                    );
//                                    ddlArrayList4.add(itemEnts4);


//                                    spinner.setVisibility(View.GONE);
//                                    spinner2.setVisibility(View.GONE);
//                                    spinner3.setVisibility(View.GONE);
//                                    spinner4.setVisibility(View.GONE);
//
//                                    etTower.setText(item.getString("Nama_Zona"));
//                                    etFloor.setText(item.getString("Nama_Lokasi"));
//                                    etArea.setText(item.getString("Deskripsi"));
//                                    etWork.setText(item.getString("NamaPekerjaan"));
//
//                                    etTower.setVisibility(View.VISIBLE);
//                                    etFloor.setVisibility(View.VISIBLE);
//                                    etArea.setVisibility(View.VISIBLE);
//                                    etWork.setVisibility(View.VISIBLE);

                                    etLokasi1.setText(item.getString("Nama_Zona")+ " : "+item.getString("Nama_Lokasi") +" : "+item.getString("Deskripsi")  );

                                    floorfull = item.getString("KodeLokasi");
                                    area = item.getString("KodeArea");
                                    tower =item.getString("KodeTower");
                                    work = item.getString("KodePekerjaan");
                                    gtvReadyStockQty(materialparam, typeitemparam);
                                }
                                 MProgressDialog.dismissProgressDialog();
//                                ArrayAdapter<CommonTowerFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
//                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
//                                spinner.setAdapter(adapterDDLGlobal);
//
//                                ArrayAdapter<CommonFloorFormEnt> adapterDDLGlobal2 = new ArrayAdapter<>(getBaseContext(),
//                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList2);
//                                spinner2.setAdapter(adapterDDLGlobal2);
//
//                                ArrayAdapter<CommonAreaFormEnt> adapterDDLGlobal3 = new ArrayAdapter<>(getBaseContext(),
//                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList3);
//                                spinner3.setAdapter(adapterDDLGlobal3);
//
//                                ArrayAdapter<CommonWorkFormEnt> adapterDDLGlobal4 = new ArrayAdapter<>(getBaseContext(),
//                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList4);
//                                spinner4.setAdapter(adapterDDLGlobal4);
                            }
                            else{
                                etLokasi1.setText("-");
                                gtvReadyStockQty(materialparam, typeitemparam);
                            }

                            //MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void gtvReadyStockQty(String material, String typeitem){
        Log.d("material", material);

        String where = "";
        floorfull2 = floorfull;
        area2 = area;
        work2 = work;
        tower2 = tower;

        //Jagaan saat parameter kode area kosong berarti wherecond diganti untuk menampilkan item migrasi
        if(typeitem.equals("Migrasi-SPR") || typeitem.equals("Migrasi-Kontrak") || typeitem.equals("Migrasi-OtherKontrak") || typeitem.equals("Migrasi-AddKontrak") || typeitem.equals("Migrasi-NPO") || typeitem.equals("Prelim"))
        {
            where = " WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' ";
        }
        else
        {
            where = " WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' AND Kode_Lokasi = '"+floorfull+"' AND Kode_Area = '"+area+"' ";
        }

        session = new SessionManager(getApplicationContext());
        controller.InqGeneral3(getApplicationContext(),"SPM_GetQtyReadyStockFromViewParam",
                "@KodeProject",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond",where,
//                "@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' AND Kode_Lokasi = '"+floortype+floor+"' AND Kode_Area = '"+area+"' ",
                //"@nik",String.valueOf(session.isLoggedIn()),
                //"@formid","MAMAN.02",
                //"@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("EZRAgtvReadyStockQty", result.toString());
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);
                                    latestvol = item.getString("LatestVol");
                                }
                                tvMyStock1.setText(latestvol+" "+item.getString("Unit"));
                                etUnit1.setText(item.getString("Unit"));
                            }
                            else
                            {
                                latestvol = "0";
                                tvMyStock1.setText("0");
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH gtvReadyStockQty", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public String gtvReadyStockQtyChecker(String material){
        Log.d("material", material);
        session = new SessionManager(getApplicationContext());
        controller.InqGeneral3(getApplicationContext(),"SPM_GetQtyReadyStockFromViewParam",
                "@KodeProject",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' AND Kode_Tower = '"+tower+"' AND Kode_Lokasi = '"+floorfull+"' AND Kode_Area = '"+area+"' ",
                //"@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' +"' AND Requester = '"+session.getUserName()+"' ",
                //"@nik",String.valueOf(session.isLoggedIn()),
                //"@formid","MAMAN.02",
                //"@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("EZRAgtvReadyStockQty", result.toString());
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);
                                    latestvol = item.getString("LatestVol2");
                                }
                            }
                            else
                            {
                                latestvol = "0";
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH gtvReadyStockQty", Toast.LENGTH_LONG).show();
                        }
                    }
                });

        return (latestvol);
    }

    public void gtvIdleStockQty(String material){
        Log.d("material", material);
        session = new SessionManager(getApplicationContext());
        controller.InqGeneral3(getApplicationContext(),"SPM_GetQtyIdleStockFromView_Param",
                "@KodeProject",session.getKodeProyek(),
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond"," WHERE ttrans.Kode_Proyek = '"+session.getKodeProyek()+"' AND ttrans.Kode_Item = '"+material+"' AND ttrans.FlagReturnGudang = 1 ",
                //"@wherecond"," WHERE ttrans.Kode_Proyek = '"+session.getKodeProyek()+"' AND ttrans.Kode_Item = '"+material+"' AND ttrans.ToKode_Tower = '"+tower+"' AND ttrans.FlagReturnGudang = 1 ",
                //"@nik",String.valueOf(session.isLoggedIn()),
                //"@formid","MAMAN.02",
                //"@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("EZRAgtvIdleStockQty", result.toString());
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);
                                    idlveol = item.getString("vol");
                                }
                                tvIdleStock1.setText(idlveol);
                            }
                            else
                            {
                                idlveol = "0";
                                tvIdleStock1.setText("0");
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH IdleStockQty", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void getDataForDDL3(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonFloorFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();

        MProgressDialog.showProgressDialog(RequestMaterialOutFinalActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {

                                CommonFloorFormEnt itemEntsinit = new CommonFloorFormEnt(
                                        "-select floor-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );
                                ddlArrayList.add(itemEntsinit);

                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonFloorFormEnt itemEnts = new CommonFloorFormEnt(item.getString(paramVal), item.getString(paramName),
                                            item.getString("Tipe_Lokasi"), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                }
                                MProgressDialog.dismissProgressDialog();
                                ArrayAdapter<CommonFloorFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonFloorFormEnt itemEnts = new CommonFloorFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonFloorFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void getDataForDDL4(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonAreaFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();

        MProgressDialog.showProgressDialog(RequestMaterialOutFinalActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                CommonAreaFormEnt itemEntsinit = new CommonAreaFormEnt(
                                        "-select area-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );

                                ddlArrayList.add(itemEntsinit);
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonAreaFormEnt itemEnts = new CommonAreaFormEnt(item.getString(paramVal), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                }
                                MProgressDialog.dismissProgressDialog();
                                ArrayAdapter<CommonAreaFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonAreaFormEnt itemEnts = new CommonAreaFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonAreaFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void getDataForDDL5(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonWorkFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();

        MProgressDialog.showProgressDialog(RequestMaterialOutFinalActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            if (result.length() > 0) {
                                CommonWorkFormEnt itemEntsinit = new CommonWorkFormEnt(
                                        "-select work-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );

                                ddlArrayList.add(itemEntsinit);
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonWorkFormEnt itemEnts = new CommonWorkFormEnt(item.getString(paramVal), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                }
                                MProgressDialog.dismissProgressDialog();
                                ArrayAdapter<CommonWorkFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonWorkFormEnt itemEnts = new CommonWorkFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonWorkFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    public void getDataForDDL6(final SearchableSpinner spinner, final String uniqueSPName,
                               final String paramName, final String paramVal,
                               final ArrayList<CommonMaterialFormEnt> ddlArrayList,
                               final String paramName1, final String paramVal1,
                               final String paramName2, final String paramVal2,
                               final String paramName3, final String paramVal3,
                               final String paramName4, final String paramVal4,
                               final String paramName5, final String paramVal5,
                               final String paramName6, final String paramVal6,
                               final String paramName7, final String paramVal7,
                               final String paramName8, final String paramVal8){
        ddlArrayList.clear();

        MProgressDialog.showProgressDialog(RequestMaterialOutFinalActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        controller.InqGeneral3(getApplicationContext(), uniqueSPName, paramName1, paramVal1, paramName2, paramVal2, paramName3, paramVal3,paramName4, paramVal4,paramName5, paramVal5,
                new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            MProgressDialog.dismissProgressDialog();
                            Log.d("EZRAgetMaterial", result.toString());
                            if (result.length() > 0) {
                                CommonMaterialFormEnt itemEntsinit = new CommonMaterialFormEnt(
                                        "-select material-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-",
                                        "-","-"
                                );

                                ddlArrayList.add(itemEntsinit);
                                for (int i = 0; i < result.length(); i++) {
                                    JSONObject item = result.getJSONObject(i);
                                    CommonMaterialFormEnt itemEnts = new CommonMaterialFormEnt(item.getString(paramVal)+ " ("+item.getString("spesifikasi")+") - "+item.getString("Kode_SPR")+" | "+item.getString("TypeItem"), item.getString(paramName),
                                    //CommonMaterialFormEnt itemEnts = new CommonMaterialFormEnt(item.getString(paramVal)+ " ("+item.getString("spesifikasi")+") - "+item.getString("Kode_SPR")+"", item.getString(paramName),
                                            item.getString("Kd_Anak_Kd_Material"), item.getString("Unit"),
                                            item.getString("ID"), item.getString("Kode_SPR"),
                                            item.getString("Id_Td_SPR"), item.getString("TypeItem"),
                                            item.getString(paramName), item.getString(paramName)
                                    );
                                    ddlArrayList.add(itemEnts);
                                    //sprqty = item.getString("Volume");
                                    //etSprQty.setText(sprqty);
                                    //Log.d("sprqty", sprqty);
                                }
                               // MProgressDialog.dismissProgressDialog();
                                ArrayAdapter<CommonMaterialFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                            else{
                                CommonMaterialFormEnt itemEnts = new CommonMaterialFormEnt(
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-",
                                        "No Item Found","-"
                                );
                                ddlArrayList.add(itemEnts);
                                ArrayAdapter<CommonMaterialFormEnt> adapterDDLGlobal = new ArrayAdapter<>(getBaseContext(),
                                        android.R.layout.simple_spinner_dropdown_item, ddlArrayList);
                                spinner.setAdapter(adapterDDLGlobal);
                            }
                           // MProgressDialog.dismissProgressDialog();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH getMaterial"+e.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onSave(String output) {

                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MMController.getInstance().setConnectivityListener(this);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;


        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.tvCapMatName1), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);

        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.TOP;
        sbView.setLayoutParams(params);
        snackbar.show();

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
}
