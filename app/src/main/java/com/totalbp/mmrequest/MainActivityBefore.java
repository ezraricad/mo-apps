package com.totalbp.mmrequest;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.Gson;
import com.orm.SugarContext;
import com.totalbp.mmrequest.activity.ApprovalMaterialActivity;
import com.totalbp.mmrequest.activity.QtyIdleItemsActivity;
import com.totalbp.mmrequest.activity.RequestMaterialFilterActivity;
import com.totalbp.mmrequest.activity.RequestMaterialOutFinal2Activity;
import com.totalbp.mmrequest.adapter.ListRequestItemAdapter;
import com.totalbp.mmrequest.config.SessionManager;
import com.totalbp.mmrequest.controller.MMController;
import com.totalbp.mmrequest.fragments.ProfilePictureDialogFragment;
import com.totalbp.mmrequest.interfaces.VolleyCallback;
import com.totalbp.mmrequest.model.ListItemRequestOut;
import com.totalbp.mmrequest.model.UserPrivilegeEnt;
import com.totalbp.mmrequest.receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;
import static com.totalbp.mmrequest.config.AppConfig.urlProfileFromTBP;

public class MainActivityBefore extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, ListRequestItemAdapter.MessageAdapterListener, ConnectivityReceiver.ConnectivityReceiverListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Dialog dialog;
    private BottomNavigationView bottomNavigation;
    private View navHeader;
    private int mSelectedItem;
    private long lastBackPressTime = 0;
    private Toast toast;
    private TextView selectProject, projectSelected;
    private TextView tvTowerFloorZona, tvNamaPekerjaan, tvUserNameSideBar, tvEmailSideBar;
    private ImageView imgProfile;
    //private ImageView btnTransfer, btnApproval, btnReport, btnRequest;
//    private LinearLayout llRequest,llApproval, llTransfer,llReport;

    private static final int request_data_from_project_items  = 99;
    private SessionManager session;
    TextView projectName;
    public String idspk, kodespk, tower, floor, zona, namapekerjaan, namavendor, percentage;
    public  Bundle extras;

    private FloatingActionButton fab,fab1,fab2;
    private TextView tvLabelNewMo, tvLabelFiler;

    private Animation fab_open,fab_close,rotate_forward,rotate_backward;

    private static final int request_data_from  = 20;
    private static final int request_data_from_2  = 21;
    private static final int request_data_frommain  = 23;
    private Boolean isFabOpen = false;

    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    public ListRequestItemAdapter adapter;

    public List<ListItemRequestOut> listItems = new ArrayList<>();

    private ActionModeCallback actionModeCallback;
    private ActionMode actionMode;
    public Integer currentPage = 1;
    public Integer pageSize = 10;

    public Integer searchActive = 0;
    String today;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd", Locale.ENGLISH);
    JSONObject item;

    private MMController controller;

    private static final int request_data_to_confirmation  = 22;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE  = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SugarContext.init(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        controller = new MMController();
        session = new SessionManager(getApplicationContext());

        selectProject = (TextView) findViewById(R.id.select_project);
        projectSelected = (TextView) findViewById(R.id.project_selected);
        dialog = new Dialog(MainActivityBefore.this);

        Intent intent = getIntent();
        extras = intent.getExtras();
        if(extras!=null){
            if(extras.getString("nik")!=null)
            {
                //Jika url tidak dapat dari launcher
                if(extras.getString("url") == null){
                    session.setUrlConfig("http://testcis.totalbp.com/");
                }
                else
                    {
                    session.setUrlConfig(extras.getString("url"));
                }

                session.setKodeProyek(extras.getString("projectid"));
                session.setNamaProyek(extras.getString("projectname"));

                session.createLoginSession(true,extras.getString("nik"),extras.getString("email"), "role", extras.getString("token"), extras.getString("nama"));
                Log.d("login_session_mm",extras.getString("nik")+extras.getString("email")+ "role"+ extras.getString("token")+ extras.getString("nama"));

//                session.setCanView(extras.getString("toapprove"));
//                session.setCanEdit(extras.getString("todelete"));
//                session.setCanInsert(extras.getString("toedit"));
//                session.setCanDelete(extras.getString("toinsert"));
//                session.setCanApprove(extras.getString("toview"));
//                session.setUrlConfig(extras.getString("url"));

                CheckCanUserPreviledge();

                if(!session.getCanView().toString().equals("1"))
                {
                    Toast.makeText(getApplicationContext(),"You dont have Privilege to View!", Toast.LENGTH_SHORT).show();
                    this.finish();
                }
            }
//            Toast.makeText(getApplicationContext(),extras.getString("nik"),Toast.LENGTH_SHORT).show();
        }
        else{
            Intent inent = new Intent("com.totalbp.cis.main_action");
            PackageManager pm = getPackageManager();
            List<ResolveInfo> resolveInfos = pm.queryIntentActivities(inent, PackageManager.GET_RESOLVED_FILTER);
            if(resolveInfos.isEmpty()) {
                Toast.makeText(getApplicationContext(),"Application Not Installed", Toast.LENGTH_SHORT).show();
                Log.i("NoneResolved", "No Activities");
                new AlertDialog.Builder(this)
                        .setTitle("Warning")
                        .setMessage("Please using CIS launcher to use the apps")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                finish();
                            }}).show();
            } else {
                Log.i("Resolved!", "There are activities" + resolveInfos);
                startActivity(inent);
                this.finish();
            }
        }

        getSupportActionBar().setTitle("List Item Material");

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navHeader = navigationView.getHeaderView(0);

        tvUserNameSideBar = (TextView) navHeader.findViewById(R.id.tv_UsernameSideBar);
        tvEmailSideBar = (TextView) navHeader.findViewById(R.id.tv_EmailSideBar);
        imgProfile = (ImageView) navHeader.findViewById(R.id.imageProfile);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        if (!session.getKodeProyek().equals("")) {
            projectSelected.setText(session.getNamaProyek());
        }

        tvUserNameSideBar.setText(session.getUserName());
        tvEmailSideBar.setText(session.getUserEmail());

        //Start Recycler View
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        adapter = new ListRequestItemAdapter(getApplicationContext(), listItems, this);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapter);
        actionModeCallback = new ActionModeCallback();
        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        if(isNetworkAvailable()){
                            SetListView("", currentPage.toString(), pageSize.toString());
                        }
                        else
                        {
                        }
                    }
                }
        );

        fab = (FloatingActionButton)findViewById(R.id.fab);
        fab1 = (FloatingActionButton)findViewById(R.id.fab1);
        fab2 = (FloatingActionButton)findViewById(R.id.fab2);

        tvLabelNewMo = (TextView)findViewById(R.id.tvLabelNewMo);
        tvLabelFiler = (TextView) findViewById(R.id.tvLabelFiler);

        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_backward);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateFAB();
            }
        });
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivityBefore.this,RequestMaterialOutFinal2Activity.class);
                intent.putExtra("fromrequestmaterial","true");
                startActivityForResult(intent, request_data_from);
            }
        });
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivityBefore.this,QtyIdleItemsActivity.class);
                intent.putExtra("fromrequestmaterial","true");
                startActivityForResult(intent, request_data_frommain);
            }
        });

        Glide.with(getApplicationContext()).load(session.getUrlConfig()+urlProfileFromTBP+session.getUserDetails().get("nik")+".jpg")
                .asBitmap()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new BitmapImageViewTarget(imgProfile) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        imgProfile.setImageDrawable(circularBitmapDrawable);
                    }
                });


        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", session.getUrlConfig()+urlProfileFromTBP+session.getUserDetails().get("nik")+".jpg");
                bundle.putInt("position", 0);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ProfilePictureDialogFragment newFragment = ProfilePictureDialogFragment.newInstance();
                newFragment.setArguments(bundle);
                newFragment.show(ft, "slideshow");
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //JIKA BUKAN HASIL SEARCH MAKA ADD SCROOL AKTIF
                if(!searchActive.equals("1")){
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= PAGE_SIZE) {

                        currentPage = currentPage + 1;
                        SetListView("", currentPage.toString(), pageSize.toString());
                        Log.d("onScroll", currentPage.toString());

                    }
                }
                else
                {

                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        }

//        try {
//            Badges.setBadge(getApplicationContext(), 5);
//        } catch (BadgesNotSupportedException badgesNotSupportedException) {
//            Log.d("LogBadges", badgesNotSupportedException.getMessage());
//        }

    }

    public void CheckCanUserPreviledge(){

        Gson gsonHeader = new Gson();
        UserPrivilegeEnt userPrivilegeEnt = new UserPrivilegeEnt();
        userPrivilegeEnt.setUserID(session.isNikUserLoggedIn());
        userPrivilegeEnt.setKodeProyek(session.getKodeProyek());
        userPrivilegeEnt.setApp_ID("MAMAN.01");
        userPrivilegeEnt.setForm_ID("MAMAN.02");

        String jsonString = gsonHeader.toJson(userPrivilegeEnt);
        Log.d("LogParamInput",jsonString.toString());

        controller.getUserPrivilege(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                //MProgressDialog.dismissProgressDialog();
                try {
                    Log.d("LogPreviledge", result.toString());
                    //swipeRefreshLayout.setRefreshing(false);
                    if (result.length() > 0) {

                        for (int i = 0; i < result.length(); i++) {

                            item = result.getJSONObject(i);

                            session.setCanView(item.getString("To_View"));
                            session.setCanEdit(item.getString("To_Edit"));
                            session.setCanInsert(item.getString("To_Insert"));
                            session.setCanDelete(item.getString("To_Delete"));
                            session.setCanApprove(item.getString("To_Approve"));

                            Log.d("LOG.USERPRIVILEGE","To Approve : "+item.getString("To_Approve")+ " To Delete :" +item.getString("To_Delete")+ " To Edit :"+ item.getString("To_Edit")+ " To Insert :" + item.getString("To_Insert")+ " To View : "+ item.getString("To_View"));

                        }


                        //Toast.makeText(getApplicationContext(), "Can view HSE : " + session.getviewhse() + " , Can view Quality : " + session.getviewquality() + " , Can Approve : " + session.getcanapprovencf(), Toast.LENGTH_LONG).show();
                        //adapterNotification.notifyDataSetChanged();
                        //swipeRefreshLayout.setRefreshing(false);
                    }
                }catch (JSONException e){
                    Toast.makeText(getApplicationContext(), "ERROR CheckCanViewQuality", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onSave(String output) {

            }
        });
    }

    public void animateFAB(){

        if(isFabOpen){

            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            tvLabelNewMo.startAnimation(fab_close);
            tvLabelFiler.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            isFabOpen = false;
            Log.d("Raj", "close");

        } else {

            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            tvLabelNewMo.startAnimation(fab_open);
            tvLabelFiler.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            isFabOpen = true;
            Log.d("Raj","open");

        }
    }

    @Override
    public void onIconClicked(int position) {

    }

    @Override
    public void onIconImportantClicked(int position) {

    }

    @Override
    public void onMessageRowClicked(int position) {
        if (adapter.getSelectedItemCount() > 0) {
            enableActionMode(position);
        } else {
            // read the message which removes bold from the row
            ListItemRequestOut message = listItems.get(position);
            //message.setRead(true);
            listItems.set(position, message);
            adapter.notifyDataSetChanged();

//            if(message.getStatusRequest().equals("4"))
//            {
            Calendar calendar = new GregorianCalendar();
            today = dateFormat.format(calendar.getTime());
            Log.d("EzzCommenttoday", today);
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
            Date convertedDate = new Date();
            try {
                convertedDate = dateFormat2.parse(message.getTanggal_Dibuat().toString());
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            SimpleDateFormat dateFormat3 = new SimpleDateFormat("yyyy-MMM-dd", Locale.ENGLISH);
            String date2 = dateFormat3.format(convertedDate);
            Log.d("EzzCommentdate2", date2);

            Log.d("dadapaha", message.getID().toString());
            Intent intent = new Intent(this, ApprovalMaterialActivity.class);
            intent.putExtra("IdMo", message.getID().toString());
            intent.putExtra("KodeMo", message.getKode_MO().toString());
            intent.putExtra("IdMaterial", message.getKode_Item().toString());
            intent.putExtra("ApprovalNumber", message.getApprovalNo().toString());
            intent.putExtra("StatusRequest", message.getStatusRequest().toString());

            intent.putExtra("KodeTower", message.getKode_Tower().toString());
            intent.putExtra("KodeLantai", message.getKode_Lantai().toString());
            intent.putExtra("KodeArea", message.getKode_Zona().toString());

            intent.putExtra("Comment", message.getComment().toString());
            intent.putExtra("Nama_Pembuat", message.getNama_Pembuat().toString());
            intent.putExtra("Nama_Pengubah", session.isNikUserLoggedIn().toString());
            intent.putExtra("Pembuat", message.getNama_Pembuat());
            intent.putExtra("Pengubah", session.isNikUserLoggedIn());
            intent.putExtra("Tanggal_Dibuat", date2);
            intent.putExtra("Tanggal_Diubah", today);

            startActivityForResult(intent, request_data_to_confirmation);
        }
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;

            listItems.clear();
            adapter.notifyDataSetChanged();
            currentPage = 1;
            SetListView("", "1", "10");

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;


        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.fab), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);

        CoordinatorLayout.LayoutParams params =(CoordinatorLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.TOP;
        sbView.setLayoutParams(params);
        snackbar.show();

    }

    @Override
    public void onRowLongClicked(int position) {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            //mode.getMenuInflater().inflate(R.menu.detilpo_item, menu);

            // disable swipe refresh if action mode is enabled
            swipeRefreshLayout.setEnabled(false);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_filter:
                    // delete all the selected messages
                    //deleteMessages();
                    mode.finish();
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            //adapter.clearSelections();
            swipeRefreshLayout.setEnabled(true);
            actionMode = null;
            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    adapter.resetAnimationIndex();
                    // mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);


        if( requestCode == request_data_from ) {
            if (data != null){
                //projectSelected.setText(data.getExtras().getString("projectname"));
                //session.setKodeProyek(data.getExtras().getString("kodeProyek"));
                //session.setNamaProyek(data.getExtras().getString("projectname"));
                listItems.clear();
                SetListView("", "1", "10");
            }
        }
        else if( requestCode == request_data_from_2 ) {
            if (data != null){

                listItems.clear();
                adapter.notifyDataSetChanged();
                searchActive = 1;

                SetListViewSearch("", "1", "100", data.getExtras().getString("towerid"), data.getExtras().getString("floorid"), data.getExtras().getString("areaid"), data.getExtras().getString("materialid"),data.getExtras().getString("status"));
            }
        }
        else if( requestCode == request_data_to_confirmation ) {
            if (data != null){

                listItems.clear();
                SetListView("", "1", "10");
            }
        }

    }

    public boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            // Network is present and connected
            isAvailable = true;
            //btnConnect.setVisibility(View.GONE);
            //h2.removeCallbacks(run);
        }
        else
        {
            isAvailable = false;
            ///h2.postDelayed(run, 0);
            //btnConnect.setVisibility(View.VISIBLE);
        }
        return isAvailable;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getApplicationContext().getAssets().open("list_items.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void SetListView(String wherecond, String currentpage, String pagesize){
        swipeRefreshLayout.setRefreshing(true);
        session = new SessionManager(getApplicationContext());
        //listItems.clear();
        String whereCond = "";
        if (!wherecond.equals("")){
            whereCond = wherecond;
        }
        controller.InqGeneral2(getApplicationContext(),"SPM_GetListRequestMaterialOut_Param",
                "@KodeProject","'"+session.getKodeProyek()+"'",
                "@currentpage",currentpage,
                "@pagesize",pagesize,
                "@sortby","",
                "@wherecond"," AND T_MO.Kode_Proyek ="+"'"+session.getKodeProyek()+"'",
                "@nik",String.valueOf(session.isNikUserLoggedIn()),
                "@formid","MAMAN.02",
                "@zonaid","",

                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("EZRA", result.toString());
                            swipeRefreshLayout.setRefreshing(false);
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);
                                    ListItemRequestOut listProjectEnt = new ListItemRequestOut(
                                            item.getString("ID"), item.getString("Kode_MO"),
                                            item.getString("Kode_Proyek"), item.getString("Kode_Item"),
                                            item.getString("Work_Code"),item.getString("Kode_Tower"),
                                            item.getString("Kode_Lantai"),item.getString("Kode_Zona"),
                                            item.getString("Volume"),item.getString("Unit"),
                                            item.getString("StatusApproval"),item.getString("StatusAktif"),
                                            item.getString("ApprovalNo"),item.getString("Nama_Pembuat"),
                                            item.getString("Tanggal_Dibuat"),item.getString("Nama_Pengubah"),
                                            item.getString("Tanggal_Diubah"),item.getString("Deskripsi"),
                                            item.getString("Nama_Tower"),item.getString("Nama_Lantai"),
                                            item.getString("Nama_Zona"),  item.getString("StatusRequest"),
                                            item.getString("Comment"),item.getString("NamaPembuatReal")
                                    );

                                    listItems.add(listProjectEnt);
                                }
                                adapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void SetListViewSearch(String wherecond, String currentpage, String pagesize, String tower, String floor, String area, String material, String status){
        swipeRefreshLayout.setRefreshing(true);
        session = new SessionManager(getApplicationContext());
        //listItems.clear();
        String whereCond = "";
        if (!wherecond.equals("")){
            whereCond = wherecond;
        }

        if(tower.equals("-"))
        {
            tower = "";
        }
        else
        {
            tower = " AND T_MO.Kode_Tower = '"+tower+"' ";
        }

        if(floor.equals("--"))
        {
            floor = "";
        } else
        {
            floor = " AND T_MO.Kode_Lantai = '"+floor+"' ";
        }

        if(area.equals("-"))
        {
            area = "";
        } else
        {
            area = " AND M_Area.Deskripsi = '"+area+"' ";
        }

        if(material.equals("-"))
        {
            material = "";
        } else
        {
            material = " AND T_MO.Kode_Item = '"+material+"' ";
        }

        if(!status.equals(""))
        {
            //String InitialStatus = null;
            if(status.equals("NOT POSTED"))
            {
                status = " AND T_MO.StatusRequest = 1 AND T_MO.ApprovalNo = '' ";
            }
            else if(status.equals("WAITING FOR APPROVAL"))
            {
                status = " AND T_MO.StatusRequest = 1 AND T_MO.ApprovalNo != '' ";
            }
            else if(status.equals("APPROVED BY SM"))
            {
                status = " AND T_MO.StatusRequest = 2 ";
            }
            else if(status.equals("REJECTED BY SM"))
            {
                status = " AND T_MO.StatusRequest = 3 ";
            }
            else if(status.equals("CONFIRMED BY STORE KEEPER"))
            {
                status = " AND T_MO.StatusRequest = 4 ";
            }
            else if(status.equals("REJECTED BY STORE KEEPER"))
            {
                status = " AND T_MO.StatusRequest = 5 ";
            }
            else if(status.equals("CONFIRMED BY QSPV"))
            {
                status = " AND T_MO.StatusRequest = 6 ";
            }
            else if(status.equals("REJECTED BY QSPV"))
            {
                status = " AND T_MO.StatusRequest = 7 ";
            }

        }


        controller.InqGeneral2(getApplicationContext(),"SPM_GetListRequestMaterialOut_Param",
                "@KodeProject","'"+session.getKodeProyek()+"'",
                "@currentpage",currentpage,
                "@pagesize",pagesize,
                "@sortby","",
                "@wherecond"," AND T_MO.Kode_Proyek = '"+session.getKodeProyek()+"' "+tower+floor+area+material+status,
                "@nik",String.valueOf(session.isNikUserLoggedIn()),
                "@formid","MAMAN.02",
                "@zonaid","",

                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("EZRA", result.toString());
                            swipeRefreshLayout.setRefreshing(false);
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);
                                    ListItemRequestOut listProjectEnt = new ListItemRequestOut(
                                            item.getString("ID"), item.getString("Kode_MO"),
                                            item.getString("Kode_Proyek"), item.getString("Kode_Item"),
                                            item.getString("Work_Code"),item.getString("Kode_Tower"),
                                            item.getString("Kode_Lantai"),item.getString("Kode_Zona"),
                                            item.getString("Volume"),item.getString("Unit"),
                                            item.getString("StatusApproval"),item.getString("StatusAktif"),
                                            item.getString("ApprovalNo"),item.getString("Nama_Pembuat"),
                                            item.getString("Tanggal_Dibuat"),item.getString("Nama_Pengubah"),
                                            item.getString("Tanggal_Diubah"),item.getString("Deskripsi"),
                                            item.getString("Nama_Tower"),item.getString("Nama_Lantai"),
                                            item.getString("Nama_Zona"),  item.getString("StatusRequest"),
                                            item.getString("Comment"),item.getString("NamaPembuatReal")
                                    );

                                    listItems.add(listProjectEnt);
                                }
                                // Log.d("EZRA2", item.toString());
                                adapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }


    private void selectFragment(MenuItem item) {
        Fragment frag = null;
        // init corresponding fragment
        switch (item.getItemId()) {
            case R.id.action_filter:



                break;
            case R.id.action_sort:



                break;
        }
        // update selected item
        mSelectedItem = item.getItemId();

        // uncheck the other items.
        for (int i = 0; i< bottomNavigation.getMenu().size(); i++) {
            MenuItem menuItem = bottomNavigation.getMenu().getItem(i);
            menuItem.setChecked(menuItem.getItemId() == item.getItemId());
        }

//        updateToolbarText(item.getTitle());

        if (frag != null) {
            //FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            //ft.replace(R.id.fragmentContainer, frag, frag.getTag());
            //ft.commit();

        }
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {
            //actionMode = getActivity().startSupportActionMode(actionModeCallback);
            actionMode = ((AppCompatActivity) getApplicationContext()).startSupportActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        adapter.toggleSelection(position);
        int count = adapter.getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

            Intent intent2=new Intent(MainActivityBefore.this,RequestMaterialFilterActivity.class);
            intent2.putExtra("fromrequestmaterial","true");
            startActivityForResult(intent2, request_data_from_2);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void logoutUser() {
        session.RemoveSession();
        Intent intent = new Intent(MainActivityBefore.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MMController.getInstance().setConnectivityListener(this);
    }


    @Override
    public void onRefresh() {
        if(isNetworkAvailable()){
            listItems.clear();
            adapter.notifyDataSetChanged();
            currentPage = 1;
            SetListView("", "1", "10");
        }
        else
        {

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
