package com.totalbp.mmrequest;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.totalbp.mmrequest.activity.ApprovalMaterialActivity;
import com.totalbp.mmrequest.activity.QtyIdleItemsActivity;
import com.totalbp.mmrequest.activity.RequestMaterialFilterActivity;
import com.totalbp.mmrequest.activity.RequestMaterialOutFinal2Activity;
import com.totalbp.mmrequest.adapter.ListDetilAdapter;
import com.totalbp.mmrequest.adapter.ListLokasiAdapter;
import com.totalbp.mmrequest.adapter.ListRequestItemAdapter;
import com.totalbp.mmrequest.adapter.ListRequestItemDetilAdapter;
import com.totalbp.mmrequest.config.SessionManager;
import com.orm.SugarContext;
import com.totalbp.mmrequest.controller.MMController;
import com.totalbp.mmrequest.fragments.ProfilePictureDialogFragment;
import com.totalbp.mmrequest.interfaces.VolleyCallback;
import com.totalbp.mmrequest.model.ListDayEnt;
import com.totalbp.mmrequest.model.ListGroupItemEnt;
import com.totalbp.mmrequest.model.ListGroupItemMoDetilEnt;
import com.totalbp.mmrequest.model.ListImageUserMoEnt;
import com.totalbp.mmrequest.model.ListItemRequestOut;
import com.totalbp.mmrequest.model.UserPrivilegeEnt;
import com.totalbp.mmrequest.receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

import static com.totalbp.mmrequest.config.AppConfig.urlProfileFromTBP;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, ListRequestItemDetilAdapter.MessageAdapterListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Dialog dialog;
    private BottomNavigationView bottomNavigation;
    private View navHeader;
    private int mSelectedItem;
    private long lastBackPressTime = 0;
    private Toast toast;
    private TextView selectProject, projectSelected;
    private TextView tvTowerFloorZona, tvNamaPekerjaan, tvUserNameSideBar, tvEmailSideBar;
    private ImageView imgProfile;
    ListRequestItemDetilAdapter.MessageAdapterListener test;
    //private ImageView btnTransfer, btnApproval, btnReport, btnRequest;
//    private LinearLayout llRequest,llApproval, llTransfer,llReport;

    private static final int request_data_from_project_items  = 99;
    private SessionManager session;
    TextView projectName;
    public String idspk, kodespk, tower, floor, zona, namapekerjaan, namavendor, percentage;
    public  Bundle extras;

    private FloatingActionButton fab,fab1,fab2;
    private TextView tvLabelNewMo, tvLabelFiler;

    private Animation fab_open,fab_close,rotate_forward,rotate_backward;

    private static final int request_data_from  = 20;
    private static final int request_data_from_2  = 21;
    private static final int request_data_frommain  = 23;
    private Boolean isFabOpen = false;

    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;

    public List<ListDayEnt> listItems = new ArrayList<>();

    private ActionModeCallback actionModeCallback;
    private ActionMode actionMode;
    public Integer currentPage = 1;
    public Integer pageSize = 10;
    public Integer searchActive = 0;
    String today;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
    private static final SimpleDateFormat dateFormatParam = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

    private static final SimpleDateFormat dateFormatForInput = new SimpleDateFormat("yyyy-MMM-dd", Locale.ENGLISH);
    JSONObject item;
    private MMController controller;

    private static final int request_data_to_confirmation  = 22;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE  = 99;

    private SectionedRecyclerViewAdapter sectionAdapter;
    Calendar calendar = new GregorianCalendar();
    Calendar calendarTwo = new GregorianCalendar();
    int sectionOneScrool = 1;

    public ArrayList<ListGroupItemEnt> list = new ArrayList<>();
    public ArrayList<ListGroupItemMoDetilEnt> list2 = new ArrayList<>();
    public ArrayList<ListImageUserMoEnt> list3 = new ArrayList<>();
    List<String> listString = new ArrayList<>();
//    public  adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SugarContext.init(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        controller = new MMController();
        session = new SessionManager(getApplicationContext());
        test = this;

        selectProject = (TextView) findViewById(R.id.select_project);
        projectSelected = (TextView) findViewById(R.id.project_selected);
        dialog = new Dialog(MainActivity.this);

        Intent intent = getIntent();
        extras = intent.getExtras();
        if(extras!=null){
            if(extras.getString("nik")!=null)
            {
                //Jika url tidak dapat dari launcher
                if(extras.getString("url") == null){
                    session.setUrlConfig("http://testcis.totalbp.com/");
                }
                else
                    {
                    session.setUrlConfig(extras.getString("url"));
                }

                session.setKodeProyek(extras.getString("projectid"));
                session.setNamaProyek(extras.getString("projectname"));

                session.createLoginSession(true,extras.getString("nik"),extras.getString("email"), "role", extras.getString("token"), extras.getString("nama"));
                Log.d("login_session_mm",extras.getString("nik")+extras.getString("email")+ "role"+ extras.getString("token")+ extras.getString("nama"));

//                session.setCanView(extras.getString("toapprove"));
//                session.setCanEdit(extras.getString("todelete"));
//                session.setCanInsert(extras.getString("toedit"));
//                session.setCanDelete(extras.getString("toinsert"));
//                session.setCanApprove(extras.getString("toview"));
//                session.setUrlConfig(extras.getString("url"));

                CheckCanUserPreviledge();

                if(!session.getCanView().toString().equals("1"))
                {
                    Toast.makeText(getApplicationContext(),"You dont have Privilege to View!", Toast.LENGTH_SHORT).show();
                    this.finish();
                }
            }
//            Toast.makeText(getApplicationContext(),extras.getString("nik"),Toast.LENGTH_SHORT).show();
        }
        else{
            Intent inent = new Intent("com.totalbp.cis.main_action");
            PackageManager pm = getPackageManager();
            List<ResolveInfo> resolveInfos = pm.queryIntentActivities(inent, PackageManager.GET_RESOLVED_FILTER);
            if(resolveInfos.isEmpty()) {
                Toast.makeText(getApplicationContext(),"Application Not Installed", Toast.LENGTH_SHORT).show();
                Log.i("NoneResolved", "No Activities");
                new AlertDialog.Builder(this)
                        .setTitle("Warning")
                        .setMessage("Please using CIS launcher to use the apps")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                finish();
                            }}).show();
            } else {
                Log.i("Resolved!", "There are activities" + resolveInfos);
                startActivity(inent);
                this.finish();
            }
        }

        getSupportActionBar().setTitle("List Item Material");

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navHeader = navigationView.getHeaderView(0);

        tvUserNameSideBar = (TextView) navHeader.findViewById(R.id.tv_UsernameSideBar);
        tvEmailSideBar = (TextView) navHeader.findViewById(R.id.tv_EmailSideBar);
        imgProfile = (ImageView) navHeader.findViewById(R.id.imageProfile);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        if (!session.getKodeProyek().equals("")) {
            projectSelected.setText(session.getNamaProyek());
        }

        tvUserNameSideBar.setText(session.getUserName());
        tvEmailSideBar.setText(session.getUserEmail());

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        sectionAdapter = new SectionedRecyclerViewAdapter();
        //Start Recycler View
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_views);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(sectionAdapter);

        fab = (FloatingActionButton)findViewById(R.id.fab);
        fab1 = (FloatingActionButton)findViewById(R.id.fab1);
        fab2 = (FloatingActionButton)findViewById(R.id.fab2);

        tvLabelNewMo = (TextView)findViewById(R.id.tvLabelNewMo);
        tvLabelFiler = (TextView) findViewById(R.id.tvLabelFiler);

        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_backward);
//        actionModeCallback = new ActionModeCallback();
//        swipeRefreshLayout.post(
//                new Runnable() {
//                    @Override
//                    public void run() {
//                        if(isNetworkAvailable()){

//                            SetListView("", currentPage.toString(), pageSize.toString());
//                        }
//                        else
//                        {
//                        }
//                    }
//                }
//        );


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateFAB();
            }
        });
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,RequestMaterialOutFinal2Activity.class);
                intent.putExtra("fromrequestmaterial","true");
                startActivityForResult(intent, request_data_from);
            }
        });
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,QtyIdleItemsActivity.class);
                intent.putExtra("fromrequestmaterial","true");
                startActivityForResult(intent, request_data_frommain);
            }
        });


        Glide.with(getApplicationContext()).load(session.getUrlConfig()+urlProfileFromTBP+session.getUserDetails().get("nik")+".jpg")
                .asBitmap()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new BitmapImageViewTarget(imgProfile) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        imgProfile.setImageDrawable(circularBitmapDrawable);
                    }
                });


        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", session.getUrlConfig()+urlProfileFromTBP+session.getUserDetails().get("nik")+".jpg");
                bundle.putInt("position", 0);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ProfilePictureDialogFragment newFragment = ProfilePictureDialogFragment.newInstance();
                newFragment.setArguments(bundle);
                newFragment.show(ft, "slideshow");
            }
        });

//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                //JIKA BUKAN HASIL SEARCH MAKA ADD SCROOL AKTIF
//                if(!searchActive.equals("1")){
//                    int visibleItemCount = mLayoutManager.getChildCount();
//                    int totalItemCount = mLayoutManager.getItemCount();
//                    int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();
//                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
//                            && firstVisibleItemPosition >= 0
//                            && totalItemCount >= PAGE_SIZE) {
//
//                        currentPage = currentPage + 1;
//                        SetListView("", currentPage.toString(), pageSize.toString());
//                        Log.d("onScroll", currentPage.toString());
//
//                    }
//                }
//                else
//                {
//
//                }
//            }
//        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        }

//        try {
//            Badges.setBadge(getApplicationContext(), 5);
//        } catch (BadgesNotSupportedException badgesNotSupportedException) {
//            Log.d("LogBadges", badgesNotSupportedException.getMessage());
//        }

        loadJSONFromAsset();

    }

    public String loadJSONFromAsset() {
        swipeRefreshLayout.setRefreshing(true);
        listItems.clear();
        for (int i = 0; i < 5; i++) {
            int id = i+1;
            String day = dateFormat.format(calendar.getTime());
            String dayparam = dateFormatParam.format(calendar.getTime());

            ListDayEnt listHeadInspection = new ListDayEnt(
                    String.valueOf(id), day, dayparam, 1
            );

            listItems.add(listHeadInspection);
            sectionAdapter.addSection(new NewsSection(NewsSection.DAY, day, dayparam, String.valueOf(id), "", "", "", "", "", ""));
            Log.d("LogMoDate", "Id :"+String.valueOf(id)+", day :"+day);
            swipeRefreshLayout.setRefreshing(false);

            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        return null;
    }

    @Override
    public void onIconClicked(int position) {

    }

    @Override
    public void onIconImportantClicked(int position) {

    }

    @Override
    public void onMessageRowClicked(int position) {

//        Log.d("LogggJum", String.valueOf(list2.size()));
//        Log.d("LogggPos", String.valueOf(position));
        ListGroupItemMoDetilEnt message = list2.get(position);
        list2.set(position, message);

                Calendar calendar = new GregorianCalendar();
                String today = dateFormatForInput.format(calendar.getTime());

                SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
                Date convertedDate = new Date();
                try {
                    convertedDate = dateFormat2.parse(message.getTanggal_Dibuat().toString());
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                SimpleDateFormat dateFormat3 = new SimpleDateFormat("yyyy-MMM-dd", Locale.ENGLISH);
                String date2 = dateFormat3.format(convertedDate);

                Intent intent = new Intent(getApplicationContext(), ApprovalMaterialActivity.class);
                intent.putExtra("IdMo", message.getId().toString());
                intent.putExtra("KodeMo", message.getKode_MO().toString());
                intent.putExtra("IdMaterial", message.getKode_Item().toString());
                intent.putExtra("ApprovalNumber", message.getApprovalNo().toString());
                intent.putExtra("StatusRequest", message.getStatusRequest().toString());

                intent.putExtra("KodeTower", message.getKode_Tower().toString());
                intent.putExtra("KodeLantai", message.getKode_Lantai().toString());
                intent.putExtra("KodeArea", message.getKode_Zona().toString());

                intent.putExtra("Comment", message.getComment().toString());
                intent.putExtra("Nama_Pembuat", message.getNama_Pembuat().toString());
                intent.putExtra("Nama_Pengubah", message.getNama_Pembuat().toString());
                intent.putExtra("Pembuat", message.getNama_Pembuat().toString());
                intent.putExtra("Pengubah", message.getNama_Pembuat().toString());
                intent.putExtra("Tanggal_Dibuat", date2);
                intent.putExtra("Tanggal_Diubah", today);

                startActivityForResult(intent, request_data_to_confirmation);
    }

    @Override
    public void onRowLongClicked(int position) {

    }

    private class NewsSection extends StatelessSection
    {

        final static int DAY = 0;

        final int topic;

        String title;
        String titleParam;
        String tittleid;

        ArrayList<ListGroupItemEnt> list = new ArrayList<>();
//        ArrayList<ListGroupItemMoDetilEnt> list2 = new ArrayList<>();
//        ArrayList<ListImageUserMoEnt> list3 = new ArrayList<>();



        int imgPlaceholderResId;
        boolean expanded = true;

        NewsSection(int topic, String string, String tanggalParam, String idday, String type, String towerid, String floorid, String areaid, String materialid, String status) {
            super(new SectionParameters.Builder(R.layout.activity_sectioned_row)
                    .headerResourceId(R.layout.activity_sectioned_header)
                    .footerResourceId(R.layout.activity_sectioned_footer)
                    .build());

            this.topic = topic;
            switch (topic) {
                case DAY:
                    this.title = string;
                    this.titleParam = tanggalParam;
                    this.tittleid = idday;
                    Log.d("LogIn", type);
                    if(type.equals("search"))
                    {
                        this.list = SetDataSearch(tanggalParam, String.valueOf("1"), String.valueOf("5"), idday, towerid, floorid, areaid, materialid, status);
                    }
                    else
                        {
                            this.list = SetData(tanggalParam, String.valueOf("1"), String.valueOf("5"), idday);

                    }

                    this.imgPlaceholderResId = R.drawable.ic_spn;
                    break;
            }
        }

        private List<String> getNews(int arrayResource) {
            return new ArrayList<>(Arrays.asList(getResources().getStringArray(arrayResource)));
        }

        public ArrayList SetData(String tanggalParam, String pagesize, String totalrow, String idday){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getApplicationContext());
            Log.d("LogIn", "SetData");
            controller.InqGeneral2(getApplicationContext(),"GetListItemMaterialOut",
                        "@KodeProject","'"+session.getKodeProyek()+"'",
                        "@currentpage",pagesize,
                        "@pagesize",totalrow,
                        "@sortby","",
                        "@wherecond"," AND T_MO.Kode_Proyek ="+"'"+session.getKodeProyek()+"' "+
                                "AND CONVERT(VARCHAR,T_MO.Tanggal_Dibuat,105) = '"+tanggalParam+"' ",
                        "@nik",String.valueOf(session.isNikUserLoggedIn()),
                        "@formid","MAMAN.02",
                        "@zonaid","",

                        new VolleyCallback() {
                            @Override
                            public void onSave(String output) {

                            }

                            @Override
                            public void onSuccess(JSONArray result) {
                                try {
                                    Log.d("LogMoItem", result.toString());
                                    swipeRefreshLayout.setRefreshing(false);
                                    if (result.length() > 0) {
                                        for (int i = 0; i < result.length(); i++) {
                                            int j = 1+i;
                                            JSONObject item = result.getJSONObject(i);

                                            ListGroupItemEnt itemEnts = new ListGroupItemEnt(
                                                    item.getString("Kode_Proyek"),item.getString("Kode_Item"),
                                                    item.getString("StatusAktif"),item.getString("Nama_Pembuat"),
                                                    item.getString("Tanggal_Dibuat"),item.getString("Deskripsi"),
                                                    item.getString("NamaPembuatReal"),String.valueOf(j), idday
                                            );
                                            list.add(itemEnts);

                                            SetDataMoNumber(item.getString("Kode_Proyek").toString(), item.getString("Tanggal_Dibuat").toString(), item.getString("Nama_Pembuat").toString(), item.getString("Kode_Item").toString(), String.valueOf(j), idday);
                                        }
                                        sectionAdapter.notifyDataSetChanged();
                                    }
                                }catch (JSONException e){
                                    Toast.makeText(getApplicationContext(), "CATCH "+e.toString(), Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            return list;

        }

        public ArrayList SetDataSearch(String tanggalParam, String pagesize, String totalrow, String idday, String towerid, String floorid, String areaid, String materialid, String status){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getApplicationContext());

            Log.d("LogIn", "SetDataSearch");
            if(towerid.equals("-"))
            {
                towerid = "";
            }
            else
            {
                towerid = " AND T_MO.Kode_Tower = '"+towerid+"' ";
            }

            if(floorid.equals("--"))
            {
                floorid = "";
            } else
            {
                floorid = " AND T_MO.Kode_Lantai = '"+floorid+"' ";
            }

            if(areaid.equals("-"))
            {
                areaid = "";
            } else
            {
                areaid = " AND M_Area.Deskripsi = '"+areaid+"' ";
            }

            if(materialid.equals("-"))
            {
                materialid = "";
            } else
            {
                materialid = " AND T_MO.Kode_Item = '"+materialid+"' ";
            }

            if(!status.equals(""))
            {
                //String InitialStatus = null;
                if(status.equals("NOT POSTED"))
                {
                    status = " AND T_MO.StatusRequest = 1 AND T_MO.ApprovalNo = '' ";
                }
                else if(status.equals("WAITING FOR APPROVAL"))
                {
                    status = " AND T_MO.StatusRequest = 1 AND T_MO.ApprovalNo != '' ";
                }
                else if(status.equals("APPROVED BY SM"))
                {
                    status = " AND T_MO.StatusRequest = 2 ";
                }
                else if(status.equals("REJECTED BY SM"))
                {
                    status = " AND T_MO.StatusRequest = 3 ";
                }
                else if(status.equals("CONFIRMED BY STORE KEEPER"))
                {
                    status = " AND T_MO.StatusRequest = 4 ";
                }
                else if(status.equals("REJECTED BY STORE KEEPER"))
                {
                    status = " AND T_MO.StatusRequest = 5 ";
                }
                else if(status.equals("CONFIRMED BY QSPV"))
                {
                    status = " AND T_MO.StatusRequest = 6 ";
                }
                else if(status.equals("REJECTED BY QSPV"))
                {
                    status = " AND T_MO.StatusRequest = 7 ";
                }

            }

            String finalTowerid = towerid;
            String finalFloorid = floorid;
            String finalAreaid = areaid;
            String finalStatus = status;
            controller.InqGeneral2(getApplicationContext(),"GetListItemMaterialOut",
                    "@KodeProject","'"+session.getKodeProyek()+"'",
                    "@currentpage",pagesize,
                    "@pagesize",totalrow,
                    "@sortby","",
                    "@wherecond"," AND T_MO.Kode_Proyek ="+"'"+session.getKodeProyek()+"' "+
                            "AND CONVERT(VARCHAR,T_MO.Tanggal_Dibuat,105) = '"+tanggalParam+"' "+materialid,
                    "@nik",String.valueOf(session.isNikUserLoggedIn()),
                    "@formid","MAMAN.02",
                    "@zonaid","",

                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                Log.d("LogMoItem", result.toString());
                                swipeRefreshLayout.setRefreshing(false);
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        int j = 1+i;
                                        JSONObject item = result.getJSONObject(i);

                                        ListGroupItemEnt itemEnts = new ListGroupItemEnt(
                                                item.getString("Kode_Proyek"),item.getString("Kode_Item"),
                                                item.getString("StatusAktif"),item.getString("Nama_Pembuat"),
                                                item.getString("Tanggal_Dibuat"),item.getString("Deskripsi"),
                                                item.getString("NamaPembuatReal"),String.valueOf(j), idday
                                        );
                                        list.add(itemEnts);

                                        SetDataMoNumberSearch(item.getString("Kode_Proyek").toString(), item.getString("Tanggal_Dibuat").toString(), item.getString("Nama_Pembuat").toString(), item.getString("Kode_Item").toString(), String.valueOf(j), idday, finalTowerid, finalFloorid, finalAreaid, finalStatus);
                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                }
                            }catch (JSONException e){
                                Toast.makeText(getApplicationContext(), "CATCH "+e.toString(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            return list;

        }

        public ArrayList SetDataMoNumber(String kodeproyek, String tgldibuat, String pembuat, String kodeitem, String idunique, String idday){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getApplicationContext());
            Log.d("LogIn", "EnterNormalFunction");
            controller.InqGeneral2(getApplicationContext(),"GetListItemMaterialOutDetil",
                        "@KodeProject","'"+kodeproyek+"'",
                        "@currentpage","1",
                        "@pagesize","50",
                        "@sortby","",
                        "@wherecond"," AND T_MO.Kode_Proyek ="+"'"+kodeproyek+"' "+
                                "AND CONVERT(VARCHAR,T_MO.Tanggal_Dibuat,105) = '"+tgldibuat+"' "+
                                "AND T_MO.Nama_Pembuat = '"+pembuat+"' "+
                                "AND T_MO.Kode_Item = '"+kodeitem+"' ",
                        "@nik",String.valueOf(session.isNikUserLoggedIn()),
                        "@formid","MAMAN.02",
                        "@zonaid","",

                        new VolleyCallback() {
                            @Override
                            public void onSave(String output) {

                            }

                            @Override
                            public void onSuccess(JSONArray result) {
                                try {
                                    swipeRefreshLayout.setRefreshing(false);
                                    Log.d("LogMoItemDetil", result.toString());
                                    if (result.length() > 0) {
                                        for (int i = 0; i < result.length(); i++) {
                                            JSONObject item = result.getJSONObject(i);

                                            ListGroupItemMoDetilEnt itemEnts = new ListGroupItemMoDetilEnt(
                                                    item.getString("ID"),item.getString("Kode_MO"),
                                                    item.getString("Kode_Proyek"),item.getString("Kode_Item"),
                                                    item.getString("Kode_Tower"),item.getString("Kode_Lantai"),
                                                    item.getString("Kode_Zona"),item.getString("Volume"),
                                                    item.getString("Unit"),item.getString("StatusApproval"),
                                                    item.getString("StatusAktif"),item.getString("ApprovalNo"),
                                                    item.getString("Nama_Pembuat"),item.getString("Tanggal_Dibuat"),
                                                    item.getString("Deskripsi"),item.getString("Nama_Tower"),
                                                    item.getString("Nama_Lantai"),item.getString("Nama_Zona"),
                                                    item.getString("RefHeaderSPR"),item.getString("RefDetailSPR"),
                                                    item.getString("StatusRequest"),item.getString("NamaPembuatReal"),
                                                    idunique, item.getString("Comment"), idday

                                            );

                                            list2.add(itemEnts);
                                        }
//                                        sectionAdapter.notifyDataSetChanged();
                                       gtvByteImageFromNik(idunique,pembuat, idday);
                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                }catch (JSONException e){
                                    Toast.makeText(getApplicationContext(), "CATCH "+e.toString(), Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            return list2;
        }

        public ArrayList SetDataMoNumberSearch(String kodeproyek, String tgldibuat, String pembuat, String kodeitem, String idunique, String idday, String towerid, String floorid, String areaid, String status){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getApplicationContext());
            Log.d("LogIn", "EnterSearchFunction");
            controller.InqGeneral2(getApplicationContext(),"GetListItemMaterialOutDetil",
                    "@KodeProject","'"+kodeproyek+"'",
                    "@currentpage","1",
                    "@pagesize","50",
                    "@sortby","",
                    "@wherecond"," AND T_MO.Kode_Proyek ="+"'"+kodeproyek+"' "+
                            "AND CONVERT(VARCHAR,T_MO.Tanggal_Dibuat,105) = '"+tgldibuat+"' "+
                            "AND T_MO.Nama_Pembuat = '"+pembuat+"' "+
                            "AND T_MO.Kode_Item = '"+kodeitem+"' "+towerid+floorid+areaid+status,
                    "@nik",String.valueOf(session.isNikUserLoggedIn()),
                    "@formid","MAMAN.02",
                    "@zonaid","",

                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result) {
                            try {
                                swipeRefreshLayout.setRefreshing(false);
                                Log.d("LogMoItemDetil", result.toString());
                                if (result.length() > 0) {
                                    for (int i = 0; i < result.length(); i++) {
                                        JSONObject item = result.getJSONObject(i);

                                        ListGroupItemMoDetilEnt itemEnts = new ListGroupItemMoDetilEnt(
                                                item.getString("ID"),item.getString("Kode_MO"),
                                                item.getString("Kode_Proyek"),item.getString("Kode_Item"),
                                                item.getString("Kode_Tower"),item.getString("Kode_Lantai"),
                                                item.getString("Kode_Zona"),item.getString("Volume"),
                                                item.getString("Unit"),item.getString("StatusApproval"),
                                                item.getString("StatusAktif"),item.getString("ApprovalNo"),
                                                item.getString("Nama_Pembuat"),item.getString("Tanggal_Dibuat"),
                                                item.getString("Deskripsi"),item.getString("Nama_Tower"),
                                                item.getString("Nama_Lantai"),item.getString("Nama_Zona"),
                                                item.getString("RefHeaderSPR"),item.getString("RefDetailSPR"),
                                                item.getString("StatusRequest"),item.getString("NamaPembuatReal"),
                                                idunique, item.getString("Comment"), idday

                                        );

                                        list2.add(itemEnts);
                                    }
//                                        sectionAdapter.notifyDataSetChanged();
                                       gtvByteImageFromNik(idunique,pembuat, idday);
                                }
                                sectionAdapter.notifyDataSetChanged();
                            }catch (JSONException e){
                                Toast.makeText(getApplicationContext(), "CATCH "+e.toString(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            return list2;
        }


        public ArrayList gtvByteImageFromNik(String uniqueid, String nik, String idday){
            swipeRefreshLayout.setRefreshing(true);
            session = new SessionManager(getApplicationContext());
            controller.InqGeneralFoto(getApplicationContext(),"GetFotoKaryawanMobile",
                    "@NIK",nik,
                    "@NikPembuat",nik,
                    new VolleyCallback() {
                        @Override
                        public void onSave(String output) {

                        }

                        @Override
                        public void onSuccess(JSONArray result)
                        {
                            swipeRefreshLayout.setRefreshing(false);
                            if (result.length() > 0) {
                                try {


                                    for (int i = 0; i < result.length(); i++)
                                    {
                                        JSONObject item = result.getJSONObject(i);

                                        String foto = item.getString("Foto");
                                        byte[] decodedString = Base64.decode(foto, Base64.DEFAULT);
                                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                                        //memakai stream agar bisa diload oleh glide
                                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                        decodedByte.compress(Bitmap.CompressFormat.JPEG, 15, stream);

                                        //masukan id unik, nik dan image hasil decode
                                        ListImageUserMoEnt itemEnts = new ListImageUserMoEnt(
                                                uniqueid,nik,
                                                stream.toByteArray(), idday
                                        );
                                        list3.add(itemEnts);

//                                        item = result.getJSONObject(0);
//
//                                        Log.d("MMLogFoto", foto);
//                                        if (type.equals("requester")) {
//                                            ivRequesterPhoto.setImageBitmap(decodedByte);
//                                            dContentImageQspv.setImageBitmap(decodedByte);
//                                        } else {
//                                            ivSmPhoto.setImageBitmap(decodedByte);
//                                            dContentImageManager.setImageBitmap(decodedByte);
//                                        }

                                    }
                                    sectionAdapter.notifyDataSetChanged();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
            return list3;
        }

        public int getContentItemsTotal2() {
//            return expanded? list.size() : 0;
            return list2.size();
        }

        @Override
        public int getContentItemsTotal() {
//            return expanded? list.size() : 0;
            return list.size();
        }

        @Override
        public RecyclerView.ViewHolder getItemViewHolder(View view) {
            return new ItemViewHolder(view);
        }

        @Override
        public void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
            final ItemViewHolder itemHolder = (ItemViewHolder) holder;

            itemHolder.tvMaterialName.setText(list.get(position).getDeskripsi());

            ListRequestItemDetilAdapter adapter = new ListRequestItemDetilAdapter(getApplicationContext(), list2, position, list, test);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            itemHolder.idRLMo.setLayoutManager(mLayoutManager);
            itemHolder.idRLMo.setAdapter(adapter);

//            itemHolder.tvDate.setText(list.get(position).getDate());
////            itemHolder.tvInspector.setText(list.get(position).getInspector());
//            itemHolder.tvStartStatus.setText(list.get(position).getStatusStart());
//            Log.d("LogInspections222",list.get(position).getTglInspeksiParam().toString());
//            StringBuilder builder = new StringBuilder();
//            for (ListGroupItemMoDetilEnt details : list2)
//            {
//
////                Log.d("LogInspections3",details.getMdInsp().toString()+details.getTglInspeksi().toString()+"="+list.get(position).getId().toString()+list.get(position).getTglInspeksiParam().toString());
//
//                if(details.getIdDay().toString().equals(list.get(position).getIdDay().toString()) && details.getIdUnique().toString().equals(list.get(position).getIdUnique().toString() ))
//                {
////                    Log.d("LogInspections33",details.getTglInspeksi().toString()+"="+list.get(position).getTglInspeksiParam().toString());
//                    builder.append(details.getKode_MO() + "\n");
//                    Log.d("LogInspection",details.getKode_MO().toString());
//                    itemHolder.textView3.setText(builder.toString());
//
//                    Log.d("LogInspection2",builder.toString());
//                }
//
//
//            }

//            int x = 0;
//            for (ListGroupItemMoDetilEnt details : list2)
//            {
//
////                Log.d("LogLooop", String.valueOf(x));
////                x++;
//
//                if(details.getIdUnique().toString().equals(list.get(position).getIdUnique().toString()) && details.getIdDay().toString().equals(list.get(position).getIdDay().toString()) && details.getKode_Item().toString().equals(list.get(position).getKode_Item().toString()) && details.getNama_Pembuat().toString().equals(list.get(position).getNama_Pembuat().toString() ))
//                {
//                    if(!listString.contains(details.getKode_MO()))
//                    {
//                        listString.add(details.getKode_MO());
//
//                        LinearLayout linearLayout = new LinearLayout(getApplicationContext());
//                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                        linearLayout.setLayoutParams(params);
//                        linearLayout.setClickable(true);
//                        linearLayout.setOrientation(LinearLayout.VERTICAL);
//                        final int sdk = android.os.Build.VERSION.SDK_INT;
//                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//                            linearLayout.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.list_selector));
//                        } else {
//                            linearLayout.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.list_selector));
//                        }
//
////                    TextView tvAppStatus = new TextView(getApplicationContext());
////                    LinearLayout.LayoutParams tvAppStatusparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
////                    tvAppStatusparams.gravity = Gravity.RIGHT;
////                    tvAppStatus.setLayoutParams(tvAppStatusparams);
////                    //tvAppStatus.setText(details.getKode_MO().toString());
////                    tvAppStatus.setTextColor(Color.parseColor("#85b100"));
////                    tvAppStatus.setTypeface(tvAppStatus.getTypeface(), Typeface.BOLD);
////                    tvAppStatus.setTextSize(getResources().getDimension(R.dimen.textsize));
////
////                    if(details.getStatusRequest().equals("1") && details.getApprovalNo().equals(""))
////                    {
////                        tvAppStatus.setBackgroundResource(R.drawable.btn_yellowtrans);
////                        tvAppStatus.setText("NOT POSTED");
////                    }
////                    else if(details.getStatusRequest().equals("1") && !details.getApprovalNo().equals(""))
////                    {
////                        tvAppStatus.setBackgroundResource(R.drawable.btn_yellowtrans);
////                        tvAppStatus.setText("WAITING FOR APPROVAL");
////                    }
////                    else if(details.getStatusRequest().equals("2"))
////                    {
////                        tvAppStatus.setBackgroundResource(R.drawable.btn_greentrans);
////                        tvAppStatus.setText("APPROVED BY SM");
////                    }
////                    else if(details.getStatusRequest().equals("3"))
////                    {
////                        tvAppStatus.setBackgroundResource(R.drawable.btn_redtrans);
////                        tvAppStatus.setText("REJECTED BY SM");
////                    }
////                    else if(details.getStatusRequest().equals("4"))
////                    {
////                        tvAppStatus.setBackgroundResource(R.drawable.btn_bluetrans);
////                        tvAppStatus.setText("CONFIRMED BY STORE KEEPER");
////                    }
////                    else if(details.getStatusRequest().equals("5"))
////                    {
////                        tvAppStatus.setBackgroundResource(R.drawable.btn_redtrans);
////                        tvAppStatus.setText("REJECTED BY STORE KEEPER");
////                    }
////                    else if(details.getStatusRequest().equals("6"))
////                    {
////                        tvAppStatus.setBackgroundResource(R.drawable.btn_greentrans);
////                        tvAppStatus.setText("CONFIRMED BY QSPV");
////                    }
////                    else if(details.getStatusRequest().equals("7"))
////                    {
////                        tvAppStatus.setBackgroundResource(R.drawable.btn_redtrans);
////                        tvAppStatus.setText("REJECTED BY QSPV");
////                    }
//
//
//                        TextView tvMoNumber = new TextView(getApplicationContext());
//                        LinearLayout.LayoutParams etparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                        tvMoNumber.setLayoutParams(etparams);
//                        tvMoNumber.setText(details.getKode_MO().toString());
//                        tvMoNumber.setTextColor(Color.parseColor("#85b100"));
//                        tvMoNumber.setTypeface(tvMoNumber.getTypeface(), Typeface.BOLD);
//                        tvMoNumber.setTextSize(getResources().getDimension(R.dimen.textsize));
//
//                        TextView tvLokasi = new TextView(getApplicationContext());
//                        LinearLayout.LayoutParams etlokasiparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                        tvLokasi.setLayoutParams(etlokasiparams);
//                        tvLokasi.setText(details.getNama_Tower() + " : " + details.getNama_Lantai() + " : " + details.getNama_Zona());
//                        tvLokasi.setTextColor(Color.parseColor("#9f9f9f"));
//                        tvLokasi.setTextSize(getResources().getDimension(R.dimen.textsize));
//
//                        TextView tvMoId = new TextView(getApplicationContext());
//                        LinearLayout.LayoutParams tvmoidparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                        tvMoId.setLayoutParams(tvmoidparams);
//                        tvMoId.setText(details.getId());
//                        tvMoId.setVisibility(View.GONE);
//
//                        View vw = new View(getApplicationContext());
//                        LinearLayout.LayoutParams vwparams = new LinearLayout.LayoutParams(AppBarLayout.LayoutParams.MATCH_PARENT, 2);
//                        vwparams.setMargins(0, 0, 15, 0);
//                        vw.setLayoutParams(vwparams);
//                        vw.setBackgroundColor(Color.parseColor("#95c0c0c0"));
//
////                    linearLayout.addView(tvAppStatus);
//                        linearLayout.addView(tvMoNumber);
//                        linearLayout.addView(tvLokasi);
//                        linearLayout.addView(tvMoId);
//                        linearLayout.addView(vw);
//
//                        itemHolder.idRLMo.addView(linearLayout);
//                        Log.d("LogDoubleIdUnique list", list.get(position).getIdUnique().toString() + ", IdUnique list2 " + details.getIdUnique().toString() + ", KodeItem list" + list.get(position).getKode_Item().toString()+ ", KodeItem list2" + details.getKode_Item().toString()+ ", Item List" + list.get(position).getDeskripsi().toString()+ ", Item List2" + details.getDeskripsi().toString()+ ", Pembuat List" + list.get(position).getNamaPembuatReal().toString()+ ", Pembuat List2" + details.getNamaPembuatReal().toString()+ ", KodeMO List2" + details.getKode_MO().toString());
//
//
//                        linearLayout.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
////                            Toast toast = Toast.makeText(getApplicationContext(), "Value : "+details.getKode_MO().toString(), Toast.LENGTH_LONG);
////                            toast.setGravity(Gravity.CENTER, 0, 0);
////                            toast.show();
//

//                            }
//                        });
//                    }
//
//                }
//            }
//
            for (ListImageUserMoEnt details : list3) {
                if (details.getId().toString().equals(list.get(position).getIdUnique().toString()) && details.getIdDay().toString().equals(list.get(position).getIdDay().toString()) && details.getNik().toString().equals(list.get(position).getNama_Pembuat().toString())) {
                    //itemHolder.imgItem.setImageBitmap(details.getImage());

                    Glide.with(getApplicationContext()).load(details.getImage())
                            .thumbnail(0.5f)
                            .centerCrop()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(itemHolder.imgItem);

                }
            }


        }

        @Override
        public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
            return new HeaderViewHolder(view);
        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;

            headerHolder.tvTitle.setText(title);
            headerHolder.tvDayParam.setText(titleParam);

//            headerHolder.rootView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    expanded = !expanded;
//                    headerHolder.imgArrow.setImageResource(
//                            expanded ? R.drawable.ic_keyboard_arrow_up_black_18dp : R.drawable.ic_keyboard_arrow_down_black_18dp
//                    );
//                    sectionAdapter.notifyDataSetChanged();
//                }
//            });

        }

        @Override
        public RecyclerView.ViewHolder getFooterViewHolder(View view) {
            return new FooterViewHolder(view);
        }

        @Override
        public void onBindFooterViewHolder(RecyclerView.ViewHolder holder) {
            FooterViewHolder footerHolder = (FooterViewHolder) holder;

            footerHolder.rootViewFooter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // Looping saat click footer mengupdate listitem untuk menentukan page di section yang diclick
                    for (int loop = 0 ; loop < listItems.size(); loop++)
                    {
                        if (listItems.get(loop).getDateParam().toString().equals(titleParam))
                        {
                            int count = 1+listItems.get(loop).getTotalClick();
                            ListDayEnt listHeadInspection = new ListDayEnt(
                                    listItems.get(loop).getId(), listItems.get(loop).getDate(),
                                    listItems.get(loop).getDateParam(), count
                            );

                            listItems.set(loop, listHeadInspection);
                            list = SetData(titleParam, String.valueOf(count), String.valueOf("5"),tittleid);
                        }
                    }

                }
            });
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvTitle, tvDayParam;
        private final LinearLayout rootView;

        HeaderViewHolder(View view) {
            super(view);
            rootView = (LinearLayout) view.findViewById(R.id.root_header);
            tvTitle = (TextView) view.findViewById(R.id.tvDay);
            tvDayParam = (TextView) view.findViewById(R.id.tvDayParam);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout rootViewFooter;


        FooterViewHolder(View view) {
            super(view);

            rootViewFooter = (RelativeLayout) view.findViewById(R.id.rootViewFooter);
        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout rootView;
        private final RecyclerView idRLMo;
        private final TextView tvMaterialName;
        private final TextView tvMoNumber;
        private final TextView tvLocation;
        private final ImageView imgItem;

        ItemViewHolder(View view) {
            super(view);

            rootView = (RelativeLayout) view.findViewById(R.id.rootView);
            idRLMo = (RecyclerView) view.findViewById(R.id.idRLMo);
            tvMaterialName = (TextView) view.findViewById(R.id.tvMaterialName);
            tvMoNumber = (TextView) view.findViewById(R.id.tvMoNumber);
            tvLocation = (TextView) view.findViewById(R.id.tvLocation);
            imgItem = (ImageView) view.findViewById(R.id.imgItem);
        }
    }



    public void CheckCanUserPreviledge(){

        Gson gsonHeader = new Gson();
        UserPrivilegeEnt userPrivilegeEnt = new UserPrivilegeEnt();
        userPrivilegeEnt.setUserID(session.isNikUserLoggedIn());
        userPrivilegeEnt.setKodeProyek(session.getKodeProyek());
        userPrivilegeEnt.setApp_ID("MAMAN.01");
        userPrivilegeEnt.setForm_ID("MAMAN.02");

        String jsonString = gsonHeader.toJson(userPrivilegeEnt);
        Log.d("LogParamInput",jsonString.toString());

        controller.getUserPrivilege(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                //MProgressDialog.dismissProgressDialog();
                try {
                    Log.d("LogPreviledge", result.toString());
                    //swipeRefreshLayout.setRefreshing(false);
                    if (result.length() > 0) {

                        for (int i = 0; i < result.length(); i++) {

                            item = result.getJSONObject(i);

                            session.setCanView(item.getString("To_View"));
                            session.setCanEdit(item.getString("To_Edit"));
                            session.setCanInsert(item.getString("To_Insert"));
                            session.setCanDelete(item.getString("To_Delete"));
                            session.setCanApprove(item.getString("To_Approve"));

                            Log.d("LOG.USERPRIVILEGE","To Approve : "+item.getString("To_Approve")+ " To Delete :" +item.getString("To_Delete")+ " To Edit :"+ item.getString("To_Edit")+ " To Insert :" + item.getString("To_Insert")+ " To View : "+ item.getString("To_View"));

                        }


                        //Toast.makeText(getApplicationContext(), "Can view HSE : " + session.getviewhse() + " , Can view Quality : " + session.getviewquality() + " , Can Approve : " + session.getcanapprovencf(), Toast.LENGTH_LONG).show();
                        //adapterNotification.notifyDataSetChanged();
                        //swipeRefreshLayout.setRefreshing(false);
                    }
                }catch (JSONException e){
                    Toast.makeText(getApplicationContext(), "ERROR CheckCanViewQuality", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onSave(String output) {

            }
        });
    }

    public void animateFAB(){

        if(isFabOpen){

            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            tvLabelNewMo.startAnimation(fab_close);
            tvLabelFiler.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            isFabOpen = false;
            Log.d("Raj", "close");

        } else {

            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            tvLabelNewMo.startAnimation(fab_open);
            tvLabelFiler.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            isFabOpen = true;
            Log.d("Raj","open");

        }
    }


    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;

            listItems.clear();
//            adapter.notifyDataSetChanged();
            currentPage = 1;
//            SetListView("", "1", "10");

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;


        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.fab), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);

        CoordinatorLayout.LayoutParams params =(CoordinatorLayout.LayoutParams)sbView.getLayoutParams();
        params.gravity = Gravity.TOP;
        sbView.setLayoutParams(params);
        snackbar.show();

    }

//
//    @Override
//    public void onNetworkConnectionChanged(boolean isConnected) {
//        showSnack(isConnected);
//    }

    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            //mode.getMenuInflater().inflate(R.menu.detilpo_item, menu);

            // disable swipe refresh if action mode is enabled
            swipeRefreshLayout.setEnabled(false);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_filter:
                    // delete all the selected messages
                    //deleteMessages();
                    mode.finish();
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            //adapter.clearSelections();
            swipeRefreshLayout.setEnabled(true);
            actionMode = null;
            recyclerView.post(new Runnable() {
                @Override
                public void run() {
//                    adapter.resetAnimationIndex();
                    // mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);


        if( requestCode == request_data_from ) {
            if (data != null){
                listItems.clear();
//                SetListView("", "1", "10");
            }
        }
        else if( requestCode == request_data_from_2 ) {
            if (data != null){

                Log.d("Here","Logg");
                sectionAdapter.removeAllSections();
                listItems.clear();
                list.clear();
                list2.clear();
                list3.clear();
                searchActive = 1;
                sectionAdapter.notifyDataSetChanged();

                if(!data.getExtras().getString("dateRange").equals("")) {

                    String towerid = data.getExtras().getString("towerid");
                    String floorid = data.getExtras().getString("floorid");
                    String areaid = data.getExtras().getString("areaid");
                    String materialid = data.getExtras().getString("materialid");
                    String status = data.getExtras().getString("status");


                    List<String> myList = new ArrayList<String>(Arrays.asList(data.getExtras().getString("dateRange").split(",")));
                    for (String date : myList) {
                        String finalStr = date.replace("[", "");
                        finalStr = finalStr.replace("]", "");
                        finalStr = finalStr.replace(" ", "");

                        Log.d("LogFilterUpcomingFrg", finalStr);
                        int loop = 0;
                        sectionAdapter.addSection(new NewsSection(NewsSection.DAY, finalStr, finalStr, String.valueOf(loop), "search", towerid, floorid, areaid, materialid, status));
                        loop++;
                    }
                }
                sectionAdapter.notifyDataSetChanged();
            }
        }
        else if( requestCode == request_data_to_confirmation ) {
            if (data != null){

                listItems.clear();
//                SetListView("", "1", "10");
            }
        }

    }

    public boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            // Network is present and connected
            isAvailable = true;
            //btnConnect.setVisibility(View.GONE);
            //h2.removeCallbacks(run);
        }
        else
        {
            isAvailable = false;
            ///h2.postDelayed(run, 0);
            //btnConnect.setVisibility(View.VISIBLE);
        }
        return isAvailable;
    }



//    public void SetListView(String wherecond, String currentpage, String pagesize){
//        swipeRefreshLayout.setRefreshing(true);
//        session = new SessionManager(getApplicationContext());
//        //listItems.clear();
//        String whereCond = "";
//        if (!wherecond.equals("")){
//            whereCond = wherecond;
//        }
//        controller.InqGeneral2(getApplicationContext(),"SPM_GetListRequestMaterialOut_Param",
//                "@KodeProject","'"+session.getKodeProyek()+"'",
//                "@currentpage",currentpage,
//                "@pagesize",pagesize,
//                "@sortby","",
//                "@wherecond"," AND T_MO.Kode_Proyek ="+"'"+session.getKodeProyek()+"'",
//                "@nik",String.valueOf(session.isNikUserLoggedIn()),
//                "@formid","MAMAN.02",
//                "@zonaid","",
//
//                new VolleyCallback() {
//                    @Override
//                    public void onSave(String output) {
//
//                    }
//
//                    @Override
//                    public void onSuccess(JSONArray result) {
//                        try {
//                            Log.d("EZRA", result.toString());
//                            swipeRefreshLayout.setRefreshing(false);
//                            if (result.length() > 0) {
//                                for (int i = 0; i < result.length(); i++) {
//                                    item = result.getJSONObject(i);
//                                    ListItemRequestOut listProjectEnt = new ListItemRequestOut(
//                                            item.getString("ID"), item.getString("Kode_MO"),
//                                            item.getString("Kode_Proyek"), item.getString("Kode_Item"),
//                                            item.getString("Work_Code"),item.getString("Kode_Tower"),
//                                            item.getString("Kode_Lantai"),item.getString("Kode_Zona"),
//                                            item.getString("Volume"),item.getString("Unit"),
//                                            item.getString("StatusApproval"),item.getString("StatusAktif"),
//                                            item.getString("ApprovalNo"),item.getString("Nama_Pembuat"),
//                                            item.getString("Tanggal_Dibuat"),item.getString("Nama_Pengubah"),
//                                            item.getString("Tanggal_Diubah"),item.getString("Deskripsi"),
//                                            item.getString("Nama_Tower"),item.getString("Nama_Lantai"),
//                                            item.getString("Nama_Zona"),  item.getString("StatusRequest"),
//                                            item.getString("Comment"),item.getString("NamaPembuatReal")
//                                    );
//
//                                    listItems.add(listProjectEnt);
//                                }
//                                adapter.notifyDataSetChanged();
//                            }
//                        }catch (JSONException e){
//                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
//                        }
//                    }
//                });
//    }

//    public void SetListViewSearch(String wherecond, String currentpage, String pagesize, String tower, String floor, String area, String material, String status, String datefrom, String dateto){
//        swipeRefreshLayout.setRefreshing(true);
//        session = new SessionManager(getApplicationContext());
//        //listItems.clear();
//        String whereCond = "";
//        if (!wherecond.equals("")){
//            whereCond = wherecond;
//        }
//
//        if(tower.equals("-"))
//        {
//            tower = "";
//        }
//        else
//        {
//            tower = " AND T_MO.Kode_Tower = '"+tower+"' ";
//        }
//
//        if(floor.equals("--"))
//        {
//            floor = "";
//        } else
//        {
//            floor = " AND T_MO.Kode_Lantai = '"+floor+"' ";
//        }
//
//        if(area.equals("-"))
//        {
//            area = "";
//        } else
//        {
//            area = " AND M_Area.Deskripsi = '"+area+"' ";
//        }
//
//        if(material.equals("-"))
//        {
//            material = "";
//        } else
//        {
//            material = " AND T_MO.Kode_Item = '"+material+"' ";
//        }
//
//        if(!status.equals(""))
//        {
//            //String InitialStatus = null;
//            if(status.equals("NOT POSTED"))
//            {
//                status = " AND T_MO.StatusRequest = 1 AND T_MO.ApprovalNo = '' ";
//            }
//            else if(status.equals("WAITING FOR APPROVAL"))
//            {
//                status = " AND T_MO.StatusRequest = 1 AND T_MO.ApprovalNo != '' ";
//            }
//            else if(status.equals("APPROVED BY SM"))
//            {
//                status = " AND T_MO.StatusRequest = 2 ";
//            }
//            else if(status.equals("REJECTED BY SM"))
//            {
//                status = " AND T_MO.StatusRequest = 3 ";
//            }
//            else if(status.equals("CONFIRMED BY STORE KEEPER"))
//            {
//                status = " AND T_MO.StatusRequest = 4 ";
//            }
//            else if(status.equals("REJECTED BY STORE KEEPER"))
//            {
//                status = " AND T_MO.StatusRequest = 5 ";
//            }
//            else if(status.equals("CONFIRMED BY QSPV"))
//            {
//                status = " AND T_MO.StatusRequest = 6 ";
//            }
//            else if(status.equals("REJECTED BY QSPV"))
//            {
//                status = " AND T_MO.StatusRequest = 7 ";
//            }
//
//        }
//
//
//        controller.InqGeneral2(getApplicationContext(),"SPM_GetListRequestMaterialOut_Param",
//                "@KodeProject","'"+session.getKodeProyek()+"'",
//                "@currentpage",currentpage,
//                "@pagesize",pagesize,
//                "@sortby","",
//                "@wherecond"," AND T_MO.Kode_Proyek = '"+session.getKodeProyek()+"' "+tower+floor+area+material+status,
//                "@nik",String.valueOf(session.isNikUserLoggedIn()),
//                "@formid","MAMAN.02",
//                "@zonaid","",
//
//                new VolleyCallback() {
//                    @Override
//                    public void onSave(String output) {
//
//                    }
//
//                    @Override
//                    public void onSuccess(JSONArray result) {
//                        try {
//                            Log.d("EZRA", result.toString());
//                            swipeRefreshLayout.setRefreshing(false);
//                            if (result.length() > 0) {
//                                for (int i = 0; i < result.length(); i++) {
//                                    item = result.getJSONObject(i);
//                                    ListItemRequestOut listProjectEnt = new ListItemRequestOut(
//                                            item.getString("ID"), item.getString("Kode_MO"),
//                                            item.getString("Kode_Proyek"), item.getString("Kode_Item"),
//                                            item.getString("Work_Code"),item.getString("Kode_Tower"),
//                                            item.getString("Kode_Lantai"),item.getString("Kode_Zona"),
//                                            item.getString("Volume"),item.getString("Unit"),
//                                            item.getString("StatusApproval"),item.getString("StatusAktif"),
//                                            item.getString("ApprovalNo"),item.getString("Nama_Pembuat"),
//                                            item.getString("Tanggal_Dibuat"),item.getString("Nama_Pengubah"),
//                                            item.getString("Tanggal_Diubah"),item.getString("Deskripsi"),
//                                            item.getString("Nama_Tower"),item.getString("Nama_Lantai"),
//                                            item.getString("Nama_Zona"),  item.getString("StatusRequest"),
//                                            item.getString("Comment"),item.getString("NamaPembuatReal")
//                                    );
//
//                                    listItems.add(listProjectEnt);
//                                }
//                                // Log.d("EZRA2", item.toString());
//                                adapter.notifyDataSetChanged();
//                            }
//                        }catch (JSONException e){
//                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
//                        }
//                    }
//                });
//    }

    private void selectFragment(MenuItem item) {
        Fragment frag = null;
        // init corresponding fragment
        switch (item.getItemId()) {
            case R.id.action_filter:



                break;
            case R.id.action_sort:



                break;
        }
        // update selected item
        mSelectedItem = item.getItemId();

        // uncheck the other items.
        for (int i = 0; i< bottomNavigation.getMenu().size(); i++) {
            MenuItem menuItem = bottomNavigation.getMenu().getItem(i);
            menuItem.setChecked(menuItem.getItemId() == item.getItemId());
        }

//        updateToolbarText(item.getTitle());

        if (frag != null) {
            //FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            //ft.replace(R.id.fragmentContainer, frag, frag.getTag());
            //ft.commit();

        }
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {
            //actionMode = getActivity().startSupportActionMode(actionModeCallback);
            actionMode = ((AppCompatActivity) getApplicationContext()).startSupportActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
//        adapter.toggleSelection(position);
//        int count = adapter.getSelectedItemCount();

//        if (count == 0) {
//            actionMode.finish();
//        } else {
//            actionMode.setTitle(String.valueOf(count));
//            actionMode.invalidate();
//        }
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

            Intent intent2=new Intent(MainActivity.this,RequestMaterialFilterActivity.class);
            intent2.putExtra("fromrequestmaterial","true");
            startActivityForResult(intent2, request_data_from_2);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void startCommentActivity(Intent i, int param){
        startActivityForResult(i, param);
    }

    private void logoutUser() {
        session.RemoveSession();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
//        MMController.getInstance().setConnectivityListener(this);
    }


    @Override
    public void onRefresh() {
        if(isNetworkAvailable()){

            sectionAdapter.removeAllSections();
            listItems.clear();
            list.clear();
            list2.clear();
            list3.clear();
            listString.clear();
            sectionAdapter.notifyDataSetChanged();
            swipeRefreshLayout.setRefreshing(false);

            calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            loadJSONFromAsset();

        }
        else
        {

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
