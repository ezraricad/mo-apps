package com.totalbp.mmrequest.model;

/**
 * Created by Ezra
 */

public class ListGroupItemEnt {

    public ListGroupItemEnt() {
    }

    public String getKode_Proyek() {
        return Kode_Proyek;
    }

    public void setKode_Proyek(String kode_Proyek) {
        Kode_Proyek = kode_Proyek;
    }

    public String getKode_Item() {
        return Kode_Item;
    }

    public void setKode_Item(String kode_Item) {
        Kode_Item = kode_Item;
    }

    public String getStatusAktif() {
        return StatusAktif;
    }

    public void setStatusAktif(String statusAktif) {
        StatusAktif = statusAktif;
    }

    public String getNama_Pembuat() {
        return Nama_Pembuat;
    }

    public void setNama_Pembuat(String nama_Pembuat) {
        Nama_Pembuat = nama_Pembuat;
    }

    public String getTanggal_Dibuat() {
        return Tanggal_Dibuat;
    }

    public void setTanggal_Dibuat(String tanggal_Dibuat) {
        Tanggal_Dibuat = tanggal_Dibuat;
    }

    public String getDeskripsi() {
        return Deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        Deskripsi = deskripsi;
    }

    public String getNamaPembuatReal() {
        return NamaPembuatReal;
    }

    public void setNamaPembuatReal(String namaPembuatReal) {
        NamaPembuatReal = namaPembuatReal;
    }

    public ListGroupItemEnt(String kode_Proyek, String kode_Item, String statusAktif, String nama_Pembuat, String tanggal_Dibuat, String deskripsi, String namaPembuatReal, String IdUnique, String idday) {

        Kode_Proyek = kode_Proyek;
        Kode_Item = kode_Item;
        StatusAktif = statusAktif;
        Nama_Pembuat = nama_Pembuat;
        Tanggal_Dibuat = tanggal_Dibuat;
        Deskripsi = deskripsi;
        NamaPembuatReal = namaPembuatReal;
        this.IdUnique = IdUnique;
        IdDay = idday;
    }

    public String Kode_Proyek;
    public String Kode_Item;
    public String StatusAktif;
    public String Nama_Pembuat;
    public String Tanggal_Dibuat;
    public String Deskripsi;
    public String NamaPembuatReal;
    public String IdUnique;
    public String IdDay;

    public String getIdDay() {
        return IdDay;
    }

    public void setIdDay(String idDay) {
        IdDay = idDay;
    }

    public String getIdUnique() {
        return IdUnique;
    }

    public void setIdUnique(String idUnique) {
        IdUnique = idUnique;
    }
}

