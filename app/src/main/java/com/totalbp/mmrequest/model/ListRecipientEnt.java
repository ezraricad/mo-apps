package com.totalbp.mmrequest.model;

/**
 * Created by Ezra
 */

public class ListRecipientEnt {

    public ListRecipientEnt() {
    }

    public String Nik;

    public ListRecipientEnt(String nik, String userName) {
        this.Nik = nik;
        this.UserName = userName;
    }

    public String UserName;

    public String getNik() {
        return Nik;
    }

    public void setNik(String nik) {
        Nik = nik;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }
}

