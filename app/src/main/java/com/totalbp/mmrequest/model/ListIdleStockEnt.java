package com.totalbp.mmrequest.model;

/**
 * Created by Ezra
 */

public class ListIdleStockEnt {

    public ListIdleStockEnt() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getIdleStock() {
        return IdleStock;
    }

    public void setIdleStock(String idleStock) {
        IdleStock = idleStock;
    }

    public ListIdleStockEnt(String id, String idleStock) {
        Id = id;

        IdleStock = idleStock;
    }

    public String Id;
    public String IdleStock;
}

