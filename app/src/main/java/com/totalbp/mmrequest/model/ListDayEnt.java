package com.totalbp.mmrequest.model;

/**
 * Created by Ezra
 */

public class ListDayEnt {

    public ListDayEnt() {
    }

    public String Id;
    public String Date;
    public String DateParam;

    public int getTotalClick() {
        return TotalClick;
    }

    public void setTotalClick(int totalClick) {
        TotalClick = totalClick;
    }

    public int TotalClick;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getDateParam() {
        return DateParam;
    }

    public void setDateParam(String dateParam) {
        DateParam = dateParam;
    }

    public ListDayEnt(String id, String date, String dateparam, int TotalClick) {

        Id = id;
        Date = date;
        DateParam = dateparam;
        this.TotalClick = TotalClick;
    }
}

