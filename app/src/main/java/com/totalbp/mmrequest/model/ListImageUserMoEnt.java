package com.totalbp.mmrequest.model;

import android.graphics.Bitmap;

/**
 * Created by Ezra
 */

public class ListImageUserMoEnt {

    public ListImageUserMoEnt() {
    }

    public String Id;
    public String Nik;
    public byte[] Image;
    public String IdDay;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getNik() {
        return Nik;
    }

    public void setNik(String nik) {
        Nik = nik;
    }

    public byte[] getImage() {
        return Image;
    }

    public void setImage(byte[] image) {
        Image = image;
    }

    public String getIdDay() {
        return IdDay;
    }

    public void setIdDay(String idDay) {
        IdDay = idDay;
    }

    public ListImageUserMoEnt(String id, String nik, byte[] image, String idday) {

        Id = id;
        Nik = nik;
        Image = image;
        IdDay = idday;
    }
}

