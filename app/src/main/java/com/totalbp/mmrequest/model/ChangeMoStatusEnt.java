package com.totalbp.mmrequest.model;

/**
 * Created by Ezra
 */

public class ChangeMoStatusEnt {


    public  String ID;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getKode_MO() {
        return Kode_MO;
    }

    public void setKode_MO(String kode_MO) {
        Kode_MO = kode_MO;
    }

    public String getNama_Pembuat() {
        return Nama_Pembuat;
    }

    public void setNama_Pembuat(String nama_Pembuat) {
        Nama_Pembuat = nama_Pembuat;
    }

    public String getNama_Pengubah() {
        return Nama_Pengubah;
    }

    public void setNama_Pengubah(String nama_Pengubah) {
        Nama_Pengubah = nama_Pengubah;
    }

    public String getPembuat() {
        return Pembuat;
    }

    public void setPembuat(String pembuat) {
        Pembuat = pembuat;
    }

    public String getPengubah() {
        return Pengubah;
    }

    public void setPengubah(String pengubah) {
        Pengubah = pengubah;
    }

    public String getStatusAktif() {
        return StatusAktif;
    }

    public void setStatusAktif(String statusAktif) {
        StatusAktif = statusAktif;
    }

    public String getStatusRequest() {
        return StatusRequest;
    }

    public void setStatusRequest(String statusRequest) {
        StatusRequest = statusRequest;
    }

    public String getTanggal_Dibuat() {
        return Tanggal_Dibuat;
    }

    public void setTanggal_Dibuat(String tanggal_Dibuat) {
        Tanggal_Dibuat = tanggal_Dibuat;
    }

    public String getTanggal_Diubah() {
        return Tanggal_Diubah;
    }

    public void setTanggal_Diubah(String tanggal_Diubah) {
        Tanggal_Diubah = tanggal_Diubah;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public String getTokenID() {
        return TokenID;
    }

    public void setTokenID(String tokenID) {
        TokenID = tokenID;
    }

    public  String Kode_MO;
    public  String Nama_Pembuat;
    public  String Nama_Pengubah;
    public  String Pembuat;
    public  String Pengubah;
    public  String StatusAktif;
    public  String StatusRequest;
    public  String Tanggal_Dibuat;
    public  String Tanggal_Diubah;
    public  String Comment;
    public  String TokenID;

    public String getPicPenerimaPhoto() {
        return PicPenerimaPhoto;
    }

    public void setPicPenerimaPhoto(String picPenerimaPhoto) {
        PicPenerimaPhoto = picPenerimaPhoto;
    }

    public  String PicPenerimaPhoto;

    public String getApprovalNo() {
        return ApprovalNo;
    }

    public void setApprovalNo(String approvalNo) {
        ApprovalNo = approvalNo;
    }

    public String ApprovalNo;

    public ChangeMoStatusEnt() {
    }


}

