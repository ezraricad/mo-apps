package com.totalbp.mmrequest.model;

/**
 * Created by Ezra
 */

public class ListReadyStockEnt {

    public ListReadyStockEnt() {
    }

    public ListReadyStockEnt(String id, String readyStock, String Unit) {
        Id = id;
        ReadyStock = readyStock;
        this.Unit = Unit;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getReadyStock() {
        return ReadyStock;
    }

    public void setReadyStock(String readyStock) {
        ReadyStock = readyStock;
    }

    public String Id;
    public String ReadyStock;
    public String Unit;
}

