package com.totalbp.mmrequest.model;

/**
 * Created by Ezra
 */

public class ListLokasiEnt {

    public ListLokasiEnt() {
    }

    public String Id;
    public String KodeTower;
    public String Nama_Zona;
    public String KodeLokasi;
    public String Nama_Lokasi;
    public String KodeArea;
    public String Deskripsi;
    public String KodePekerjaan;
    public String NamaPekerjaan;
    public String thsprid;

    public String getVolRequest() {
        return VolRequest;
    }

    public void setVolRequest(String volRequest) {
        VolRequest = volRequest;
    }

    public String getVolIdle() {
        return VolIdle;
    }

    public void setVolIdle(String volIdle) {
        VolIdle = volIdle;
    }

    public String VolRequest;
    public String VolIdle;

    public String getKodeItem() {
        return KodeItem;
    }

    public void setKodeItem(String kodeItem) {
        KodeItem = kodeItem;
    }

    public String KodeItem;
    public String TypeItem;

    public String getStatusPost() {
        return StatusPost;
    }

    public void setStatusPost(String statusPost) {
        StatusPost = statusPost;
    }

    public String StatusPost;

    public String getTypeItem() {
        return TypeItem;
    }

    public void setTypeItem(String typeItem) {
        TypeItem = typeItem;
    }

    public ListLokasiEnt(String id, String kodeTower, String nama_Zona, String kodeLokasi, String nama_Lokasi, String kodeArea, String deskripsi, String kodePekerjaan, String namaPekerjaan, String thsprid, String KodeItem, String VolRequest, String VolIdle, String TypeItem, String StatusPost) {
        Id = id;
        KodeTower = kodeTower;
        Nama_Zona = nama_Zona;
        KodeLokasi = kodeLokasi;
        Nama_Lokasi = nama_Lokasi;
        KodeArea = kodeArea;
        Deskripsi = deskripsi;
        KodePekerjaan = kodePekerjaan;
        NamaPekerjaan = namaPekerjaan;
        this.thsprid = thsprid;
        this.KodeItem = KodeItem;
        this.VolRequest = VolRequest;
        this.VolIdle = VolIdle;
        this.TypeItem = TypeItem;
        this.StatusPost = StatusPost;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getKodeTower() {
        return KodeTower;
    }

    public void setKodeTower(String kodeTower) {
        KodeTower = kodeTower;
    }

    public String getNama_Zona() {
        return Nama_Zona;
    }

    public void setNama_Zona(String nama_Zona) {
        Nama_Zona = nama_Zona;
    }

    public String getKodeLokasi() {
        return KodeLokasi;
    }

    public void setKodeLokasi(String kodeLokasi) {
        KodeLokasi = kodeLokasi;
    }

    public String getNama_Lokasi() {
        return Nama_Lokasi;
    }

    public void setNama_Lokasi(String nama_Lokasi) {
        Nama_Lokasi = nama_Lokasi;
    }

    public String getKodeArea() {
        return KodeArea;
    }

    public void setKodeArea(String kodeArea) {
        KodeArea = kodeArea;
    }

    public String getDeskripsi() {
        return Deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        Deskripsi = deskripsi;
    }

    public String getKodePekerjaan() {
        return KodePekerjaan;
    }

    public void setKodePekerjaan(String kodePekerjaan) {
        KodePekerjaan = kodePekerjaan;
    }

    public String getNamaPekerjaan() {
        return NamaPekerjaan;
    }

    public void setNamaPekerjaan(String namaPekerjaan) {
        NamaPekerjaan = namaPekerjaan;
    }

    public String getThsprid() {
        return thsprid;
    }

    public void setThsprid(String thsprid) {
        this.thsprid = thsprid;
    }
}

