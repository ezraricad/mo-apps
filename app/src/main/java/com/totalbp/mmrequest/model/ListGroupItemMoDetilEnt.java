package com.totalbp.mmrequest.model;

/**
 * Created by Ezra
 */

public class ListGroupItemMoDetilEnt {

    public ListGroupItemMoDetilEnt() {
    }

    public String ID;
    public String Kode_MO;
    public String Kode_Proyek;
    public String Kode_Item;
    public String Kode_Tower;
    public String Kode_Lantai;
    public String Kode_Zona;
    public String Volume;
    public String Unit;
    public String StatusApproval;
    public String StatusAktif;
    public String ApprovalNo;
    public String Nama_Pembuat;
    public String Tanggal_Dibuat;
    public String Deskripsi;
    public String Nama_Tower;
    public String Nama_Lantai;
    public String Nama_Zona;
    public String RefHeaderSPR;
    public String RefDetailSPR;
    public String StatusRequest;
    public String NamaPembuatReal;
    public String IdUnique;
    public String Comment;
    public String IdDay;

    public String getIdDay() {
        return IdDay;
    }

    public void setIdDay(String idDay) {
        IdDay = idDay;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public String getIdUnique() {
        return IdUnique;
    }

    public void setIdUnique(String idUnique) {
        IdUnique = idUnique;
    }

    public String getId() {
        return ID;
    }

    public void setId(String id) {
        ID = id;
    }

    public String getKode_MO() {
        return Kode_MO;
    }

    public void setKode_MO(String kode_MO) {
        Kode_MO = kode_MO;
    }

    public String getKode_Proyek() {
        return Kode_Proyek;
    }

    public void setKode_Proyek(String kode_Proyek) {
        Kode_Proyek = kode_Proyek;
    }

    public String getKode_Item() {
        return Kode_Item;
    }

    public void setKode_Item(String kode_Item) {
        Kode_Item = kode_Item;
    }

    public String getKode_Tower() {
        return Kode_Tower;
    }

    public void setKode_Tower(String kode_Tower) {
        Kode_Tower = kode_Tower;
    }

    public String getKode_Lantai() {
        return Kode_Lantai;
    }

    public void setKode_Lantai(String kode_Lantai) {
        Kode_Lantai = kode_Lantai;
    }

    public String getKode_Zona() {
        return Kode_Zona;
    }

    public void setKode_Zona(String kode_Zona) {
        Kode_Zona = kode_Zona;
    }

    public String getVolume() {
        return Volume;
    }

    public void setVolume(String volume) {
        Volume = volume;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public String getStatusApproval() {
        return StatusApproval;
    }

    public void setStatusApproval(String statusApproval) {
        StatusApproval = statusApproval;
    }

    public String getStatusAktif() {
        return StatusAktif;
    }

    public void setStatusAktif(String statusAktif) {
        StatusAktif = statusAktif;
    }

    public String getApprovalNo() {
        return ApprovalNo;
    }

    public void setApprovalNo(String approvalNo) {
        ApprovalNo = approvalNo;
    }

    public String getNama_Pembuat() {
        return Nama_Pembuat;
    }

    public void setNama_Pembuat(String nama_Pembuat) {
        Nama_Pembuat = nama_Pembuat;
    }

    public String getTanggal_Dibuat() {
        return Tanggal_Dibuat;
    }

    public void setTanggal_Dibuat(String tanggal_Dibuat) {
        Tanggal_Dibuat = tanggal_Dibuat;
    }

    public String getDeskripsi() {
        return Deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        Deskripsi = deskripsi;
    }

    public String getNama_Tower() {
        return Nama_Tower;
    }

    public void setNama_Tower(String nama_Tower) {
        Nama_Tower = nama_Tower;
    }

    public String getNama_Lantai() {
        return Nama_Lantai;
    }

    public void setNama_Lantai(String nama_Lantai) {
        Nama_Lantai = nama_Lantai;
    }

    public String getNama_Zona() {
        return Nama_Zona;
    }

    public void setNama_Zona(String nama_Zona) {
        Nama_Zona = nama_Zona;
    }

    public String getRefHeaderSPR() {
        return RefHeaderSPR;
    }

    public void setRefHeaderSPR(String refHeaderSPR) {
        RefHeaderSPR = refHeaderSPR;
    }

    public String getRefDetailSPR() {
        return RefDetailSPR;
    }

    public void setRefDetailSPR(String refDetailSPR) {
        RefDetailSPR = refDetailSPR;
    }

    public String getStatusRequest() {
        return StatusRequest;
    }

    public void setStatusRequest(String statusRequest) {
        StatusRequest = statusRequest;
    }

    public String getNamaPembuatReal() {
        return NamaPembuatReal;
    }

    public void setNamaPembuatReal(String namaPembuatReal) {
        NamaPembuatReal = namaPembuatReal;
    }

    public ListGroupItemMoDetilEnt(String id, String kode_MO, String kode_Proyek, String kode_Item, String kode_Tower, String kode_Lantai, String kode_Zona, String volume, String unit, String statusApproval, String statusAktif, String approvalNo, String nama_Pembuat, String tanggal_Dibuat, String deskripsi, String nama_Tower, String nama_Lantai, String nama_Zona, String refHeaderSPR, String refDetailSPR, String statusRequest, String namaPembuatReal, String IdUnique, String Comment, String idday) {

        ID = id;
        Kode_MO = kode_MO;
        Kode_Proyek = kode_Proyek;
        Kode_Item = kode_Item;
        Kode_Tower = kode_Tower;
        Kode_Lantai = kode_Lantai;
        Kode_Zona = kode_Zona;
        Volume = volume;
        Unit = unit;
        StatusApproval = statusApproval;
        StatusAktif = statusAktif;
        ApprovalNo = approvalNo;
        Nama_Pembuat = nama_Pembuat;
        Tanggal_Dibuat = tanggal_Dibuat;
        Deskripsi = deskripsi;
        Nama_Tower = nama_Tower;
        Nama_Lantai = nama_Lantai;
        Nama_Zona = nama_Zona;
        RefHeaderSPR = refHeaderSPR;
        RefDetailSPR = refDetailSPR;
        StatusRequest = statusRequest;
        NamaPembuatReal = namaPembuatReal;
        this.IdUnique = IdUnique;
        this.Comment = Comment;
        IdDay = idday;
    }
}

