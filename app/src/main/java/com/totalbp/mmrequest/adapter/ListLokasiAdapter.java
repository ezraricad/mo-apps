package com.totalbp.mmrequest.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.totalbp.mmrequest.R;
import com.totalbp.mmrequest.config.SessionManager;
import com.totalbp.mmrequest.controller.MMController;
import com.totalbp.mmrequest.interfaces.VolleyCallback;
import com.totalbp.mmrequest.model.ListIdleStockEnt;
import com.totalbp.mmrequest.model.ListItemRequestOut;
import com.totalbp.mmrequest.model.ListLokasiEnt;
import com.totalbp.mmrequest.model.ListReadyStockEnt;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;

/**
 * Created by Ezra.R on 10/10/2017.
 */

public class ListLokasiAdapter extends RecyclerView.Adapter<ListLokasiAdapter.MyViewHolder>{
    private Context mContext;
    private List<ListLokasiEnt> itemRequest;
    private List<ListReadyStockEnt> itemrequestReadyStock;
    private List<ListIdleStockEnt> itemsIdleStock;
    private MessageAdapterListener listener;
    private SparseBooleanArray selectedItems;
    private static int currentSelectedIndex = -1;

    // array used to perform multiple animation at once
    private SparseBooleanArray animationItemsIndex;
    private boolean reverseAllAnimations = false;
    private SessionManager session;
    MMController controller;
    private SearchableSpinner spinnerMaterial1;

    BottomSheetBehavior sheetBehavior;
    @BindView(R.id.btn_bottom_sheet)
    Button btnBottomSheet;

    @BindView(R.id.etTotalReqVol)
    EditText etTotalReqVol;

    @BindView(R.id.etUnitReqItem)
    EditText etUnitReqItem;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        public TextView tvTower, tvFloor, tvArea, tvUnitReq, tvUnit, tvCreatedDate;
        public LinearLayout messageContainer;
        public EditText etIStock, etRStock, etRequestVolume;
        public CheckBox cbItemRequest;
        public ImageButton btnCheck;

        public MyViewHolder(View view) {
            super(view);

            tvTower = (TextView) view.findViewById(R.id.tvTower);
            tvFloor = (TextView) view.findViewById(R.id.tvFloor);
            tvArea = (TextView) view.findViewById(R.id.tvArea);
            tvUnitReq = (TextView) view.findViewById(R.id.tvUnitReq);
            tvUnit = (TextView) view.findViewById(R.id.tvUnit);
            etRequestVolume = (EditText) view.findViewById(R.id.etRequestVolume);
            etRStock = (EditText) view.findViewById(R.id.etRStock);
            etIStock = (EditText) view.findViewById(R.id.etIStock);
            cbItemRequest = (CheckBox) view.findViewById(R.id.cbItemRequest);
            btnCheck = (ImageButton) view.findViewById(R.id.btnCheck);

            messageContainer = (LinearLayout) view.findViewById(R.id.messageContainer);

            view.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {
            listener.onRowLongClicked(getAdapterPosition());
            view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
            return true;
        }
    }


    public ListLokasiAdapter(Context mContext, List<ListLokasiEnt> itemrequest, List<ListReadyStockEnt> itemrequestReadyStock, List<ListIdleStockEnt> itemrequestIdleStock, SessionManager session, MMController controller,BottomSheetBehavior sheetBehavior, Button btnBottomSheet, EditText etTotalReqVol, EditText etUnitReqItem, MessageAdapterListener listener) {
        this.mContext = mContext;
        this.itemRequest = itemrequest;
        this.itemrequestReadyStock = itemrequestReadyStock;
        this.itemsIdleStock = itemrequestIdleStock;
        this.listener = listener;
        selectedItems = new SparseBooleanArray();
        animationItemsIndex = new SparseBooleanArray();
        this.session = session;
        this.controller = controller;
        this.sheetBehavior = sheetBehavior;
        this.btnBottomSheet = btnBottomSheet;
        this.etTotalReqVol = etTotalReqVol;
        this.etUnitReqItem = etUnitReqItem;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_lokasirow, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ListLokasiEnt rowItem = itemRequest.get(position);
//        ListReadyStockEnt rowItem2 = itemrequestReadyStock.get(position);

        if(!rowItem.getNama_Zona().equals("null")) {
            holder.tvTower.setText(rowItem.getNama_Zona());
            holder.tvFloor.setText(rowItem.getNama_Lokasi());
            holder.tvArea.setText(rowItem.getDeskripsi());

            for (ListReadyStockEnt details : itemrequestReadyStock) {
                if (details.getId().toString().equals(rowItem.getId().toString())) {
                    holder.etRStock.setText(details.getReadyStock());
                    holder.tvUnit.setText(details.getUnit());
                    holder.tvUnitReq.setText(details.getUnit());
                }
            }

            for (ListIdleStockEnt details2 : itemsIdleStock) {
                if (details2.getId().toString().equals(rowItem.getId().toString())) {
                    holder.etIStock.setText(details2.getIdleStock());
                }
            }

            holder.etRequestVolume.setText("");
//            holder.tvUnit.setText(rowItem.getDeskripsi());
        }

//        holder.tvCreatedDate.setText(rowItem.getTanggal_Dibuat());

        // apply click events


        applyClickEvents(holder, position, rowItem.getTypeItem(), rowItem.getKodeItem(), rowItem.getKodeLokasi(), rowItem.getKodeArea(), holder.etRStock.getText().toString());
    }

    private void applyClickEvents(MyViewHolder holder, final int position, String TypeItem, String material, String kodelantai, String kodearea, String volready) {


        holder.cbItemRequest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                JSONObject item;
                String latestvol;
                                        @Override
                                        public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {

                                            Log.d("LogReadyStockFin", String.valueOf(isChecked));
                                            if (isChecked) {

                                                //SAVE THE DATA
                                                if (!holder.etRequestVolume.getText().toString().equals("")) {
                                                    String where = "";

                                                    Log.d("LogReadyStock1", volready);
                                                    //Jagaan saat parameter kode area kosong berarti wherecond diganti untuk menampilkan item migrasi
                                                    if (TypeItem.equals("Migrasi-SPR") || TypeItem.equals("Migrasi-Kontrak") || TypeItem.equals("Migrasi-OtherKontrak") || TypeItem.equals("Migrasi-AddKontrak") || TypeItem.equals("Migrasi-NPO") || TypeItem.equals("Prelim")) {
                                                        where = " WHERE Kode_Proyek = '" + session.getKodeProyek() + "' AND KatalogID = '" + material + "' ";
                                                    } else {
                                                        where = " WHERE Kode_Proyek = '" + session.getKodeProyek() + "' AND KatalogID = '" + material + "' AND Kode_Lokasi = '" + kodelantai + "' AND Kode_Area = '" + kodearea + "' ";
                                                    }

                                                    session = new SessionManager(mContext);
                                                    controller.InqGeneral3(mContext, "SPM_GetQtyReadyStockFromViewParam",
                                                            "@KodeProject", session.getKodeProyek(),
                                                            "@currentpage", "1",
                                                            "@pagesize", "10",
                                                            "@sortby", "",
                                                            "@wherecond", where,
//                "@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND KatalogID = '"+material+"' AND Kode_Lokasi = '"+floortype+floor+"' AND Kode_Area = '"+area+"' ",
                                                            //"@nik",String.valueOf(session.isLoggedIn()),
                                                            //"@formid","MAMAN.02",
                                                            //"@zonaid","",
                                                            new VolleyCallback() {
                                                                @Override
                                                                public void onSave(String output) {

                                                                }

                                                                @Override
                                                                public void onSuccess(JSONArray result) {
                                                                    try {
                                                                        Log.d("EZRAgtvReadyStockQty", result.toString());
                                                                        if (result.length() > 0) {
                                                                            for (int i = 0; i < result.length(); i++) {
                                                                                item = result.getJSONObject(i);
                                                                                latestvol = item.getString("LatestVol2");
                                                                            }

                                                                            Log.d("LogReadyStock2", volready);
                                                                            float volumerequest = Float.parseFloat(holder.etRequestVolume.getText().toString());
                                                                            float volumereadystock = Float.parseFloat(holder.etRStock.getText().toString());
                                                                            float volumeidlestock = Float.parseFloat(holder.etIStock.getText().toString());

                                                                            float readystocknow = Float.parseFloat(latestvol);
                                                                            Log.d("LogVolumRequest", String.valueOf(readystocknow));
                                                                            //                              if (volumerequest <= volumereadystock) {
                                                                            if (volumerequest <= readystocknow) {
                                                                                String input = String.format(Locale.ENGLISH, "%.4f", volumerequest);
                                                                                Log.d("LogVolumeInput", String.valueOf(input));

                                                                                holder.cbItemRequest.setChecked(true);
                                                                                holder.etRequestVolume.setEnabled(false);
                                                                                listener.onVolRequestFiled(position, input, "0");
                                                                                setTotalRequestItem();
//                                            saveRequestMO(input, "0");
//                                                            saveRequestMO(etVolumeRequest.getText().toString(), "0");
                                                                            } else {

                                                                                if (volumerequest <= (readystocknow + volumeidlestock)) {
                                                                                    float qtyfinal1 = volumerequest - readystocknow;

                                                                                    String inputreadystock = String.format(Locale.ENGLISH, "%.4f", volumereadystock);

                                                                                    holder.cbItemRequest.setChecked(true);
                                                                                    holder.etRequestVolume.setEnabled(false);
                                                                                    listener.onVolRequestFiled(position, inputreadystock, String.valueOf(qtyfinal1));
                                                                                    setTotalRequestItem();
//                                                saveRequestMO(inputreadystock, String.valueOf(qtyfinal1));
//                                                                saveRequestMO(tvReadyStock.getText().toString(), String.valueOf(qtyfinal1));
                                                                                } else {
                                                                                    Toast toast = Toast.makeText(mContext, "Volume Request must less or equal than Ready Stock (" + readystocknow + "), or (Ready Stock + Idle Stock)!", Toast.LENGTH_LONG);
                                                                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                                                                    toast.show();

                                                                                    holder.etRequestVolume.setText("");
                                                                                    holder.cbItemRequest.setChecked(false);
                                                                                }
                                                                            }
                                                                        } else {
                                                                            latestvol = "0";
                                                                            Toast.makeText(mContext, "Check Qty Ready Stcok Latest Fail!", Toast.LENGTH_LONG).show();
                                                                        }
//                                    gtvIdleStockQty(material, tdsprid, index);
//                                    adapter.notifyDataSetChanged();
                                                                    } catch (JSONException e) {
                                                                        Toast.makeText(mContext, "CATCH gtvReadyStockQty", Toast.LENGTH_LONG).show();
                                                                    }
                                                                }
                                                            });
                                                } else {
                                                    Toast toast = Toast.makeText(mContext, "Request Volume required", Toast.LENGTH_LONG);
                                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                                    toast.show();

                                                    holder.etRequestVolume.requestFocus();
                                                    holder.cbItemRequest.setChecked(false);
                                                }
                                            }
                                            else
                                            {
                                                listener.onVolRequestCanceled(position, holder.etRequestVolume.getText().toString());
                                                holder.cbItemRequest.setChecked(false);
                                                holder.etRequestVolume.setEnabled(true);
                                                holder.etRequestVolume.setText("");
                                                holder.etRequestVolume.requestFocus();
                                                setTotalRequestItem();
                                            }
                                        }
                                    }
        );

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {

            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        holder.etRequestVolume.clearFocus();
                        setTotalRequestItem();
                        btnBottomSheet.setText("Hide Total");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        holder.etRequestVolume.clearFocus();
                        setTotalRequestItem();
                        btnBottomSheet.setText("Show Total");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            private void setTotalRequestItem() {

                Log.d("LogTotal", "Masuk");

                float total = 0;
                for(int x = 0; x < itemRequest.size(); x++)
                {
                    if(itemRequest.get(x).getStatusPost().toString().equals("1")) {
                        total = total + Float.parseFloat(itemRequest.get(x).getVolRequest().toString());

                        Log.d("LogTotal2", itemRequest.get(x).getVolRequest().toString());
                    }
                }

                etTotalReqVol.setText(String.valueOf(total));

                if(itemrequestReadyStock.size() > 0)
                {
                    etUnitReqItem.setText(itemrequestReadyStock.get(0).getUnit().toString());
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

//        holder.messageContainer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                listener.onMessageRowClicked(position);
//            }
//        });

        holder.btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!holder.etRequestVolume.getText().toString().equals("")) {
                    holder.cbItemRequest.setChecked(true);
                }
                else
                {
                    Toast toast = Toast.makeText(mContext, "Request Volume required", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    holder.etRequestVolume.requestFocus();
                }
            }
        });

    }

    public void setTotalRequestItem() {

        Log.d("LogTotal", "Masuk");

        float total = 0;
        for(int x = 0; x < itemRequest.size(); x++)
        {
            if(itemRequest.get(x).getStatusPost().toString().equals("1")) {
                total = total + Float.parseFloat(itemRequest.get(x).getVolRequest().toString());

                Log.d("LogTotal2", itemRequest.get(x).getVolRequest().toString());
            }
        }

        etTotalReqVol.setText(String.valueOf(total));

        if(itemrequestReadyStock.size() > 0)
        {
            etUnitReqItem.setText(itemrequestReadyStock.get(0).getUnit().toString());
        }
    }

    private void applyFile(MyViewHolder holder, ListItemRequestOut message) {
        /*
        if (!message.getFileUploadUrl().equals("")) {
            holder.ivFile.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_insert_drive_file_black_24dp));
            holder.ivFile.setColorFilter(ContextCompat.getColor(mContext, R.color.yellow));
        } else {
            holder.ivFile.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_attach_file_black_24dp));
            //holder.ivFile.setColorFilter(ContextCompat.getColor(mContext, R.color.yellow));
        }
        */
    }

    private void applyImportant(MyViewHolder holder, ListItemRequestOut message) {
        /*
        if (message.isImm()) {
            holder.ivStar.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_star_black_24dp));
            holder.ivStar.setColorFilter(ContextCompat.getColor(mContext, R.color.yellow));
        } else {
            holder.ivStar.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_star_border_black_24dp));
            //holder.ivStar.setColorFilter(ContextCompat.getColor(mContext, R.color.yellow));
        }
        */
    }

    private void applyIconAnimation(MyViewHolder holder, int position) {
        /*
        if (selectedItems.get(position, false)) {
            holder.iconFront.setVisibility(View.GONE);
            resetIconYAxis(holder.iconBack);
            holder.iconBack.setVisibility(View.VISIBLE);
            holder.iconBack.setAlpha(1);
            if (currentSelectedIndex == position) {
                FlipAnimator.flipView(mContext, holder.iconBack, holder.iconFront, true);
                resetCurrentIndex();
            }
        } else {
            holder.iconBack.setVisibility(View.GONE);
            resetIconYAxis(holder.iconFront);
            holder.iconFront.setVisibility(View.VISIBLE);
            holder.iconFront.setAlpha(1);
            if ((reverseAllAnimations && animationItemsIndex.get(position, false)) || currentSelectedIndex == position) {
                FlipAnimator.flipView(mContext, holder.iconBack, holder.iconFront, false);
                resetCurrentIndex();
            }
        }
        */
    }


    // As the views will be reused, sometimes the icon appears as
    // flipped because older view is reused. Reset the Y-axis to 0
    private void resetIconYAxis(View view) {
        if (view.getRotationY() != 0) {
            view.setRotationY(0);
        }
    }

    public void resetAnimationIndex() {
        reverseAllAnimations = false;
        animationItemsIndex.clear();
    }

    //@Override
    //public long getItemId(int position) {
//        return messages.get(position).getId();
//    }



    @Override
    public int getItemCount() {
        return itemRequest.size();
    }

    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
            animationItemsIndex.delete(pos);
        } else {
            selectedItems.put(pos, true);
            animationItemsIndex.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        reverseAllAnimations = true;
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {
        itemRequest.remove(position);
        resetCurrentIndex();
    }

    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }

    public interface MessageAdapterListener {
        void onIconClicked(int position);

        void onIconImportantClicked(int position);

        void onMessageRowClicked(int position);

        void onRowLongClicked(int position);

        void onVolRequestFiled(int position, String param, String param2);

        void onVolRequestCanceled(int position, String param);


    }

}
